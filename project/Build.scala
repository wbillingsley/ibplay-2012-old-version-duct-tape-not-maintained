import sbt._
import Keys._
import play.Project._

object ApplicationBuild extends Build {

    val appName         = "ibplay"
    val appVersion      = "1.0"

    val appDependencies = Seq(
      "com.theintelligentbook" %% "ibmodel" % "0.2-SNAPSHOT",
      "com.theintelligentbook" %% "ibmongo" % "0.2-SNAPSHOT"

      // Add your project dependencies here,
    )

    val main = play.Project(appName, appVersion, appDependencies).settings(
      // Add your own project settings here
        
      templatesImport += "com.wbillingsley.handy._",
      
      templatesImport += "com.theintelligentbook.ibmodel.mongo._",
      
      requireJs ++= Seq(
          "editMarkupWikiPage.js", "editWikiPage.js",
          "main.js", 
          "viewChatCharts.js", "viewChatStream.js", "viewContent.js", "viewEventCharts.js",
          "viewMarkupWikiPage.js", "viewPoll.js", "viewQnaList.js", "viewQnaQuestion.js",
          "viewTextPoll.js", "viewWikiPage.js"
      )
    )

}
