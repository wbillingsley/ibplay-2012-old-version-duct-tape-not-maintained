## To edit this new page, click edit in the content bar

This page uses `Markdown` formatting.  But we've also added a few extra pieces of markup:

### Reveal.js presentations

You can write a presentation as a wiki page. They look pretty cool. 

To write a presentation, surround the whole presentation with `<reveal<` and `>reveal>`, and surround each slide with `<<` and `>>` or `[[` or `]]`, as in the example below.

    <reveal<    
    
    <<
    ## First slide heading
    
    The slide can have **markup** in the usual way
    >>
        
    <<
    [[
    ## Slides under each other
      
    Reveal presentations can go vertical as well as horizontal. 
    To do that, use this nested markup
    ]]
   
    [[
    ## But Note…
    The slide dividers (<reveal<, <<, >>, [[, ]]) have to be on
    their own lines, not intermingled into the text     
    ]]
    >>
    
    >reveal>
