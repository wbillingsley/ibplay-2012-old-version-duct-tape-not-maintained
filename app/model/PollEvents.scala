package model

import EventRoom._
import com.theintelligentbook.ibmodel.mongo._
import com.wbillingsley.handy._
import Ref._
import play.api.libs.json._
import scala.collection.mutable

object PollEvents {

  /**
   * Gets or initialises the event room's poll state (the set of responses so far)
   */
  def getPollState(room: EventRoom, pollId:String) = {
    room.states.get(PollStream(pollId)) match {
      case Some(p: PRState) => p
      case _ => {
        val p = PRState(pollId)
        room.states = room.states.updated(PollStream(pollId), p)
        p
      }
    }
  }

  /**
   * Tabulates the poll responses into a PollResults
   */
  def calcPollData(poll: Poll, responses: mutable.Map[String, PollResponse]): PollResults = {
    PollResults(poll.id.toString, Map(
      (for ((option, i) <- poll.options.zipWithIndex) yield {
        option -> responses.count { case (id, pr) => pr.answer contains i }
      }): _*))
  }

  def broadcastState(room: EventRoom, poll:Ref[Poll]) {
    /* 
     * TODO: At scale this could become a bottleneck as (with a blocking DB) it is fetching the
     * poll each time on the EventRoom's thread. 
     */
    for (p <- poll; pollId = p.id.toString; state = getPollState(room, pollId)) {
      room.broadcast(PollStream(pollId), calcPollData(p, state.pollResponses))
    }
  }

  case class PollStream(pollId: String) extends ListenTo {
    override def onSubscribe(listenerName:String, room:EventRoom) = broadcastState(room, RefById(classOf[Poll], pollId))
  }

  case class PushPollToChat(poll: Poll) extends IBEvent {
    override def toJson = {
      val bid = poll.book.getId.map(_.toString)
      val cid = poll.ce.getId.map(_.toString)
      val pid = poll.id.toString
      Json.obj(
        "pid" -> JsString(pid),
        "type" -> JsString("poll"),
        "contentId" -> JsString(bid.getOrElse("")),
        "question" -> JsString(poll.question.getOrElse("(no question)")),
        "options" -> Json.toJson(poll.options),
        "mode" -> JsString(poll.mode.toString))
    }

    /**
     * The event room should broadcast this to everyone who's listening to the book's chat stream
     */
    override def action(room: EventRoom) {
      for (b <- poll.book) {
        room.broadcast(ChatEvents.ChatStream(b.id.toString), this)
      }
    }
  }

  /** A new poll response (vote) has been made */
  case class Vote(pr: PollResponse) extends IBEvent {

    /**
     * On receving this event, the room should re-tabulate the poll results and send them out to everyone
     * who's listening to the poll
     */
    override def action(room: EventRoom) {
      for (pollId <- pr.poll.getId; state = getPollState(room, pollId.toString)) {
        state.pollResponses(pr.id.toString) = pr        
        broadcastState(room, pr.poll)
      }
    }

  }

  /** The state of a poll is the votes so far. */
  class PRState(val pollId: String, val pollResponses: mutable.Map[String, PollResponse]) extends State

  object PRState {
    def apply(pollId: String) = {
      val seq = PollResponseDAO.getPollResponses(RefById(classOf[Poll], pollId)).map(pr => (pr.id.toString -> pr))
      new PRState(pollId, mutable.Map(seq: _*))
    }
  }

  /** An update to the poll results. Broadcast on each new vote. */
  case class PollResults(pollId:String, counts: Map[String, Int]) extends IBEvent {
    override def toJson = Json.obj(
      "type" -> "pollResults",
      "id" -> pollId,
      "results" -> Json.toJson(
    		  counts.toSeq.map { case (opt, count) => Json.obj("option" -> opt, "votes" -> count) }
      )
    )
  }

}