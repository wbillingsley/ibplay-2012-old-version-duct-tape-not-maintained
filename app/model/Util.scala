package model

import com.wbillingsley.handy._
import Ref._
import com.theintelligentbook.ibmodel.ContentModel
import com.theintelligentbook.ibmodel.mongo.{Presentation, ContentEntry, Book, Reader}
import play.api.libs.json.{JsString, Json, JsValue}
import scala.Some
import scala.Some
import com.wbillingsley.handy.RefFailed
import com.wbillingsley.handy.RefItself
import play.api.libs.json.JsString
import scala.Some
import play.api.mvc.Results.{Ok, NotFound, Forbidden, Async, InternalServerError}
import com.wbillingsley.handy._
import play.api.mvc.{Result, SimpleResult}
import play.api.libs.json.JsString
import scala.Some
import play.api.Logger
import scala.concurrent.promise
import play.api.mvc.Request
import play.api.mvc.AcceptExtractors

object Util extends AcceptExtractors {
  
  import scala.language.implicitConversions

  /**
   * Converts a Ref[play.api.templates.Html] to a Result
   */
  implicit def refHtmlToResult(r:Ref[play.api.templates.Html]):play.api.mvc.AsyncResult = {
    Async {
      val p = promise[Result]
      r onComplete(
        onSuccess = p success Ok(_),
        onNone = p success NotFound(views.html.xErrorNotFound("Not found")),
        onFail = _ match {
          case Refused(msg) => p success Forbidden(views.html.xErrorForbidden(msg))
          case exc:Throwable => p success InternalServerError(views.html.xErrorInternalError(exc.getMessage))
        }
      )
      p.future
    }
  }
  
  /**
   * Converts a Ref[play.api.mvc.Result] to a Result
   */
  implicit def refResultToResult(r:Ref[Result])(implicit request:Request[_]) = {
    Async {
      val p = promise[Result]
      r onComplete(
        onSuccess = p success _,
        onNone = p success {
          request match {
            case Accepts.Html() => NotFound(views.html.xErrorNotFound("Not found"))
            case Accepts.Json() => NotFound(Json.obj("error" -> "not found"))
            case _ => NotFound
          }
        },
        onFail = _ match {
          case Refused(msg) => p success {
            request match {
              case Accepts.Html() => Forbidden(views.html.xErrorForbidden(msg))
              case Accepts.Json() => Forbidden(Json.obj("error" -> msg))
              case _ => Forbidden(msg)
            }            
          }
          case exc:Throwable => p success {
            request match {
              case Accepts.Html() => InternalServerError(views.html.xErrorInternalError(exc.getMessage))
              case Accepts.Json() => InternalServerError(Json.obj("error" -> exc.getMessage))
              case _ => InternalServerError(exc.getMessage)
            }                        
          }
        }
      )
      p.future
    }
  }
  
  implicit class refEnumerate[T](val refMany:RefMany[T]) extends AnyVal {
    import play.api.libs.iteratee.{Enumerator, Concurrent}
    
    def enumerator:Enumerator[T] = {
      refMany match {
        // case RefRCCursor(cursor) = cursor.enumerator
        
        case _ => play.api.libs.iteratee.Concurrent.unicast[T] { channel =>	    	    
	    	    for (r <- refMany) {
	              channel.push(r)
	            }
	    	    channel.end
	    	  }
        
      }
    }    
  }

  /**
   * Gets a trimmed Option[String] value from a map of parameters
   * @param map
   * @param key
   * @return
   */
  def fetchStr(map:Map[String,Seq[String]], key:String) = {
    val v = map.getOrElse(key, Seq.empty[String]).headOption
    v.flatMap{str =>
      val trim = str.trim()
      if (trim.isEmpty()) { None } else { Some(trim) }
    }
  }

  /**
   * Gets a Set value from a map of parameters
   * @param map
   * @param key
   * @return
   */
  def fetchSet(map:Map[String,Seq[String]], key:String) = {
    map.getOrElse(key, Seq.empty[String]).toSet
  }

  /**
   * Gets a Boolean value from a map of parameters
   * @param map
   * @param key
   * @return
   */
  def fetchBool(map:Map[String,Seq[String]], key:String) = {
    map.getOrElse(key, Seq.empty[String]).headOption.map { v =>
      parseBool(v)
    }
  }
  
  def parseBool(v:String):Boolean = {
      val str = v.toLowerCase
      str == "on" || str == "true" || str == "1"    
  }

  /**
   * Gets an Option[Int] value from a map of parameters
   * @param map
   * @param key
   * @return
   */
  def fetchInt(map:Map[String,Seq[String]], key:String) = {
    map.getOrElse(key, Seq.empty[String]).headOption.map(_.toInt)
  }

  def fetchSetInts(map:Map[String,Seq[String]], key:String) = {
    map.getOrElse(key, Seq.empty[String]).map(_.toInt).toSet
  }

  
  /**
   * Produces a standard format response for the client
   *
   * if something is found:
   * { recommended: { ... JSON rendering of a content entry, including { permissions: { ...} } ... }
   *
   * also if there is a presentation:
   * { presentation: { ... JSON rendering of a presentation ... }
   *
   * if nothing is found:
   * { nothing: { permissions: { ... JSON rendering of permissions (mayAdd: true/false) ... } } }
   *
   *
   * @param tok
   * @param book
   * @param rec
   * @param pres
   * @param seeAlso
   * @return
   */  
  def ceAndPresResponse(tok:Approval[Reader], book:Ref[Book], rec:Ref[ContentEntry], pres:Ref[Presentation] = RefNone, seeAlso:RefMany[ContentEntry] = RefNone) = {
    
    /*
     * The entry and presentation to show might change, for instance if the entry is also a reference to a
     * presentation
     */
    val (rec2, pres2) = ContentModel.presAndCE(rec, pres)
    
    // Collect the JSON on the entry, or if none then on the book
    var refJson = (for (cej <- IBJson.entryToJsonWithPerms(rec2, tok)) yield {
      Json.obj("recommended" -> cej)
    }) orIfNone (for (bj <- IBJson.bookPerms(book, tok)) yield {
      Json.obj("nothing" -> bj)
    })
    
    // Add the presentation data if there is any
    refJson = (for (j <- refJson; p <- IBJson.presToJson(pres2, tok)) yield {
      j ++ Json.obj("presentation" -> p)
    }) orIfNone refJson

    // TODO: Add "see also"
    
    // Return an OK response with the JSON
    for (j <- refJson) yield Ok(j)
  }  

}
