package model

/**
 * Created with IntelliJ IDEA.
 * User: wbillingsley
 * Date: 11/06/12
 * Time: 10:24 PM
 * To change this template use File | Settings | File Templates.
 */

import play.api.libs.json._
import com.wbillingsley.handy._
import Ref._
import org.bson.types.ObjectId
import com.theintelligentbook.ibmodel.mongo._
import com.theintelligentbook.ibmodel.{SecurityModel, CEKinds}
import controllers.routes
import com.wbillingsley.handy.RefItself
import com.wbillingsley.handy.RefFailed
import org.omg.CosNaming.NamingContextPackage.NotFound
import com.wbillingsley.handy.RefItself

object IBJson {

  /**
   * Calculates the URL to view a content entry (unframed)
   */
  def viewUrl(ce:ContentEntry):Option[String] = {
    CEKinds.valueOf(ce.kind) match {
      case CEKinds.URL => {
        Ref.fromOptionId(classOf[Website], ce.refVal).toOption.flatMap(_.url)
      }
      case CEKinds.YouTube => Ref.fromOptionId(classOf[YouTube], ce.refVal).toOption.flatMap { y =>
        y.youTubeId.map("http://youtube.com/embed/" + _ + "?rel=0")
      }
      case CEKinds.GoogleSlides => Ref.fromOptionId(classOf[GoogleSlides], ce.refVal).toOption.flatMap { y =>
        y.presId.map("https://docs.google.com/presentation/d/" + _ + "/embed?start=false&loop=false&delayms=3000")
      }
      case CEKinds.WikiPage => ce.refVal.map { v => routes.WikiController.read(v).url }
      case CEKinds.MarkupWikiPage => ce.refVal.map { v => routes.WikiController.viewMarkupWikiPage(v).url }
      case CEKinds.Poll => ce.refVal.map { v => routes.PollController.view(v).url }
      case CEKinds.TextPoll => ce.refVal.map { v => routes.PollController.viewTextPoll(v).url }
      case CEKinds.MediaFile => ce.refVal.map { v => routes.MediaFileController.getImage(v).url }
      case _ => None
    }
  }

  /**
   * Calculates the URL to edit a content entry (unframed).
   * This only applies to entries that can be edited.
   */
  def editUrl(ce:ContentEntry):Option[String] = {
    CEKinds.valueOf(ce.kind) match {
      case CEKinds.URL => ce.refVal.map { v => routes.ExternalsController.editWebsite(v).url }
      case CEKinds.YouTube => ce.refVal.map { v => routes.ExternalsController.editYouTube(v).url }
      case CEKinds.GoogleSlides => ce.refVal.map { v => routes.ExternalsController.editGoogleSlides(v).url }
      case CEKinds.WikiPage => ce.refVal.map { v => routes.WikiController.edit(v).url }
      case CEKinds.MarkupWikiPage => ce.refVal.map { v => routes.WikiController.viewEditMarkupWikiPage(v).url }
      case CEKinds.Poll => ce.refVal.map { v => routes.PollController.edit(v).url }
      case CEKinds.TextPoll => ce.refVal.map { v => routes.PollController.editTextPoll(v).url }
      case _ => None
    }
  }

  def j(r:Reader):JsValue = {
    val m = Map(
      "id" -> Json.toJson(r.id.toString),
      "siteRoles" -> Json.toJson(r.siteRoles.map(_.toString))
    )
    Json.toJson(m)
  }
  
  /**
   * Converts an EntryComment to JSON format
   */
  def entryCommentToJson(ec:Ref[EntryComment]) = {    
    for (
      c <- ec;
      from <- readerToShortJson(c.reader)
    ) yield {
      Json.obj(
        "id" -> c.id.toString,
        "comment" -> c.comment,
        "date" -> c.time,
        "from" -> from
      )
    }
  }
  
  def manyEntryCommentToJson(cs:RefMany[EntryComment]) = {
    val cJson = cs.flatMap { c => entryCommentToJson(c.itself) }
    cJson.toRefOne
  }
  

  def entryToJson(entry: Ref[ContentEntry]) = {
    for (ce <- entry) yield Json.obj(
      "id" -> Json.toJson(ce.id.toString),
      "protect" -> Json.toJson(ce.protect),
      "showFirst" -> Json.toJson(ce.showFirst),
      "inTrash" -> Json.toJson(ce.inTrash),
      "score" -> ce.score,
      "commentCount" -> ce.commentCount,
      "name" -> Json.toJson(ce.name),
      "note" -> Json.toJson(ce.shortDescription),
      "kind" -> Json.toJson(ce.kind),
      "url" -> Json.toJson(viewUrl(ce)),
      "editUrl" -> Json.toJson(editUrl(ce)),
      "adjectives" -> Json.toJson(ce.adjectives),
      "nouns" -> Json.toJson(ce.nouns),
      "topics" -> Json.toJson(ce.topics),
      "highlightTopic" -> Json.toJson(ce.highlightTopic),
      "site" -> Json.toJson(ce.site)        
    )
  }
  
  def entryToJsonWithPerms(entry: Ref[ContentEntry], tok:Approval[Reader]) = {
    for (
        ce <- entry;
        jce <- entryToJson(ce.itself);
        jp <- entryPerms(ce.itself, tok) 
    ) yield {
      jce ++ jp
    }
  }

  /**
   * A map to JSON containing boolean values for commonly required permissions
   * @param ce
   * @param tok
   * @return
   */
  def entryPerms(ce:Ref[ContentEntry], tok:Approval[Reader]) = {    
    for (
      entry <- ce;
      read <- optionally(tok ask SecurityModel.ReadEntry(entry.itself));
      protect <- optionally(tok ask SecurityModel.ProtectEntry(entry.itself));
      edit <- optionally(tok ask SecurityModel.EditContent(entry.itself));
      vote <- optionally(tok ask SecurityModel.VoteOnEntry(entry.itself));
      bp <- bookPerms(entry.book, tok) 
    ) yield {
      Json.obj("permissions" ->
	      (Json.obj(
	          "mayRead" -> !read.isEmpty,
	          "mayProtect" -> !protect.isEmpty,
	          "mayEdit" -> !edit.isEmpty,
	          "mayVote" -> !vote.isEmpty
	      ) ++ bp)
	  )
    }
  }

  def bookPerms(b:Ref[Book], tok:Approval[Reader]) = {    
    for (
        book <- b;
        chat <- optionally(tok ask SecurityModel.Chat(book.itself));
        add <- optionally(tok ask SecurityModel.AddContent(book.itself))
    ) yield {
      Json.obj("mayChat" -> !chat.isEmpty, "mayAdd" -> !add.isEmpty)
    }
  }

  def chatCommentToJson(refChatComment: Ref[ChatComment]):Ref[JsValue] = {
    import Ref._
    
    for (
        cc <- refChatComment;
        author <- optionally(cc.createdBy)
    ) yield {
      var j = Json.obj(
	      "id" -> Json.toJson(cc.id.toString),
	      "type" -> JsString("chat"),
	      "anonymous" -> JsBoolean(cc.anonymous),
	      "content" -> Json.toJson(cc.comment),
	      "date" -> Json.toJson(cc.created.getTime)	      
      )
      author.flatMap(_.nickname).foreach(j += "name" -> JsString(_))
      j
    } 
  }
  
  def presToJson(pres:Ref[Presentation], tok:Approval[Reader]) = {
    for (
        p <- pres;
        cej <- IBJson.entryToJsonWithPerms(p.ce, tok)        
    ) yield {
      val entries = p.entries.flatMap(x => entryToJsonWithPerms(x.itself, tok))
      cej ++ Json.obj(
          "pid" -> Json.toJson(p.id.toString),
          "protect" -> Json.toJson(p.protect),
          "entries" -> entries.fetch.toSeq
      )      
    }       
  } 
  
  /**
   * Takes a reader and returns the short snippet of information that might
   * accompany a blog post or a comment.
   */
  def readerToShortJson(reader:Ref[Reader]) = {
    for (ropt <- optionally(reader)) yield {
      ropt match {
        case Some(r) => Json.obj(
	        "id" -> r.id.toString,
	        "nickname" -> r.nickname        
	      )
        case None => Json.obj("nickname" -> "Anonymous")
      }
      
    }
  }
     
  def embeddedCommentToJson(comm:Ref[EmbeddedComment]) = {
    for (
      c <- comm;
      r <- readerToShortJson(c.addedBy)
    ) yield {
      Json.obj(
        "score" -> c.score,
        "text" -> c.text,
        "date" -> c.date,
        "addedBy" -> r
      )
    }
  }
  
  def manyEmbeddedCommentToJson(cs:RefMany[EmbeddedComment]) = {
    val cJson = cs.flatMap { c => embeddedCommentToJson(c.itself) }
    cJson.toRefOne
  }
  
  def manyQnaQuestionToJson(qs:RefMany[QnAQuestion], tok:Approval[Reader]) = {
    val qJson = qs.flatMap { q => qnaQuestionToShortJson(q.itself, tok) }
    qJson.toRefOne   
  }
  
  /** 
   * An abbreviated version of a Q&A Question for showing in the list
   */
  def qnaQuestionToShortJson(ques:Ref[QnAQuestion], tok:Approval[Reader]) = {
    for (
      q <- ques;
      r <- readerToShortJson(q.addedBy)
    ) yield {      
      Json.obj(
          "id" -> q.id.toString,
          "title" -> q.title,
          "body" -> q.body,
          "score" -> q.score,
          "answerCount" -> q.answerCount,
          "views" -> q.views,
          "addedBy" -> r,
          "created" -> q.created,
          "updated" -> q.updated
      )      
    }    
  }
  
  def qnaAnswerToJson(ans:Ref[QnAAnswer]) = {
    for (
      a <- ans;
      r <- readerToShortJson(a.addedBy);
      comm <- manyEmbeddedCommentToJson(a.comments.toRefMany)
    ) yield {
      Json.obj(
        "id" -> a.id.toString,
        "score" -> a.score,
        "body" -> a.text,
        "addedBy" -> r,
        "created" -> a.created,
        "updated" -> a.updated,
        "comments" -> comm.toSeq
      )
    }
  }
  
  def manyQnaAnswerToJson(qs:RefMany[QnAAnswer]) = {
    val qJson = qs.flatMap { q => qnaAnswerToJson(q.itself) }
    qJson.toRefOne   
  }  
  
  def qnaQuestionToFullJson(ques:Ref[QnAQuestion], tok:Approval[Reader]) = {
    for (
      q <- ques;
      r <- readerToShortJson(q.addedBy);
      ans <- manyQnaAnswerToJson(q.answers.toRefMany);
      comm <- manyEmbeddedCommentToJson(q.comments.toRefMany)
    ) yield {      
      Json.obj(
          "id" -> q.id.toString,
          "title" -> q.title,
          "body" -> q.body,
          "score" -> q.score,
          "addedBy" -> r,
          "created" -> q.created,
          "updated" -> q.updated,
          "comments" -> comm.toSeq,
          "answers" -> ans.toSeq
      )
      
    }
    
  }
  
  def pollToJson(poll:Ref[Poll], tok:Approval[Reader]) = {
    for (
      p <- poll;
      mayVote <- optionally(tok ask SecurityModel.RespondToPoll(p.itself))
    ) yield {      
      Json.obj(
          "id" -> p.id.toString,
          "question" -> p.question,
          "options" -> {
            for ((opt, idx) <- p.options.zipWithIndex) yield { Json.obj("text" -> opt, "value" -> idx) }
          },
          "mode" -> p.mode.toString,
          "mayVote" -> !mayVote.isEmpty
      )     
    }    
  }  

  def textPollToJson(poll:Ref[TextPoll], tok:Approval[Reader]) = {
    for (
      p <- poll;
      mayVote <- optionally(tok ask SecurityModel.RespondToTextPoll(p.itself))
    ) yield {      
      Json.obj(
          "id" -> p.id.toString,
          "question" -> p.question,
          "mayVote" -> !mayVote.isEmpty
      )     
    }    
  }  

  
  
  def jId(ref:Ref[HasObjectId]) = {
    Json.toJson(ref.getId.map(_.toString))
  }


}
