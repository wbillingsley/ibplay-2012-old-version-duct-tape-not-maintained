package model

import EventRoom._
import com.theintelligentbook.ibmodel.mongo._
import com.wbillingsley.handy._
import Ref._
import play.api.libs.json._
import scala.collection.mutable

object TextPollEvents {

  /**
   * Gets or initialises the event room's poll state (the set of responses so far)
   */
  def getPollState(room: EventRoom, pollId:String) = {
    room.states.get(TPollStream(pollId)) match {
      case Some(p: PRState) => p.itself
      case _ => {
        for (p <- RefById(classOf[TextPoll], pollId)) yield {
          val ps = new PRState(pollId, mutable.Map.empty ++ p.wordCounts)
          room.states = room.states.updated(TPollStream(pollId), ps)
          ps
        }        
      }
    }
  }


  def broadcastState(room: EventRoom, poll:Ref[TextPoll]) {
    /* 
     * TODO: At scale this could become a bottleneck as (with a blocking DB) it is fetching the
     * poll each time on the EventRoom's thread. 
     */
    for (p <- poll; pollId = p.id.toString; state <- getPollState(room, pollId)) {
      room.broadcast(TPollStream(pollId), PollResults(pollId, state.wordCounts))
    }
  }

  case class TPollStream(pollId: String) extends ListenTo {
    override def onSubscribe(listenerName:String, room:EventRoom) = broadcastState(room, RefById(classOf[TextPoll], pollId))
  }

  case class PushToChat(poll: TextPoll) extends IBEvent {
    override def toJson = {
      val bid = poll.book.getId.map(_.toString)
      val cid = poll.ce.getId.map(_.toString)
      val pid = poll.id.toString
      Json.obj(
        "pid" -> JsString(pid),
        "type" -> JsString("textPoll"),
        "contentId" -> JsString(bid.getOrElse("")),
        "question" -> JsString(poll.question.getOrElse("(no question)"))
      )
    }

    /**
     * The event room should broadcast this to everyone who's listening to the book's chat stream
     */
    override def action(room: EventRoom) {
      for (b <- poll.book) {
        room.broadcast(ChatEvents.ChatStream(b.id.toString), this)
      }
    }
  }

  /** A new poll response (vote) has been made */
  case class Vote(poll:Ref[TextPoll], words:Set[String]) extends IBEvent {

    /**
     * On receiving this event, the room should re-tabulate the poll results and send them out to everyone
     * who's listening to the poll
     */
    override def action(room: EventRoom) {
      for (pollId <- poll.getId; state <- getPollState(room, pollId.toString)) {
        for (word <- words) {
          state.wordCounts.put(word, state.wordCounts.getOrElse(word, 0) + 1)
        }        
        broadcastState(room, poll)
      }
    }

  }

  /** The state of a poll is the votes so far. */
  class PRState(val pollId: String, val wordCounts: mutable.Map[String,Int]) extends State

  /** An update to the poll results. Broadcast on each new vote. */
  case class PollResults(pollId:String, counts: mutable.Map[String, Int]) extends IBEvent {
    override def toJson = Json.obj(        
      "type" -> "textPollResults",
      "id" -> pollId,
      "results" -> Json.toJson(
    		  counts.toSeq.map { case (opt, count) => Json.obj("word" -> opt, "count" -> count) }
      )
    )
  }  
  
}