package model

import EventRoom._
import com.theintelligentbook.ibmodel.mongo._
import play.api.libs.json._

object ChatEvents {
  
  case class ChatStream(bookId:String) extends ListenTo

  case class ChatMessage(anonymous: Boolean, book: Book, reader: Option[Reader], msg: Option[String], topics: Seq[String]) extends IBEvent {
    
    override def toJson = {
      var message: Seq[(String, JsValue)] = Seq(
        "type" -> JsString("chat"),
        "anonymous" -> JsBoolean(anonymous),
        "content" -> Json.toJson(msg),
        "date" -> Json.toJson(System.currentTimeMillis()))
      if (!anonymous) {
        reader.flatMap(_.nickname).foreach { n => message :+= ("name" -> JsString(n)) }
      }
      JsObject(message)
    }
    
    /**
     * The event room should just broadcast this to everyone who's listening
     */
    override def action(room:EventRoom) = {
      room.broadcast(ChatStream(book.id.toString), this)
    }
  }

}