package model

import akka.actor._
import scala.concurrent.duration._

import play.api._
import play.api.libs.json._
import play.api.libs.iteratee._
import play.api.libs.concurrent._
import play.api.libs.concurrent.Execution.Implicits._
import play.api.libs.iteratee.Concurrent.Channel

import akka.util.Timeout
import akka.pattern.ask

import play.api.Play.current

import com.wbillingsley.handy._
import com.theintelligentbook.ibmodel.mongo._
import org.bson.types.ObjectId

import scala.collection.mutable

object EventRoom {
  
  /** A key saying what the listener is listening to */
  abstract class ListenTo {
    def onSubscribe(listenerName:String, room:EventRoom) = { /* do nothing */ }
  }
  
  /** A bit of state that events can update */
  abstract class State

  /** Type of event sent in the room. */
  abstract class IBEvent {
    def toJson: JsValue = Json.obj("unexpected" -> "This event would not normally be broadcast")
    
    /** The action the EventRoom should perform when it receives this event. */
    def action(room:EventRoom) { 
      throw new UnsupportedOperationException("This event has no defined action") 
    }
  }
  
  /** Joining the event room */
  case class Join(listenerName: String, reader:Option[Reader], session:String, bookId:String, listenTo:ListenTo*) extends IBEvent 
  
  case class Subscribe(listenerName: String, listenTo: ListenTo) extends IBEvent
  
  case class Unsubscribe(listenerName: String, listenTo: ListenTo) extends IBEvent
  
  /** Leaving the event room */
  case class Quit(listenerName: String) extends IBEvent
  
  /** Successfully connected, returning an enumerator of the broadcast events. */
  case class Connected(listenerName:String, enumerator:Enumerator[JsValue]) extends IBEvent
  
  /** Can't connect */
  case class CannotConnect(msg:String) extends IBEvent {
    override def toJson = Json.obj("error" -> msg)
  }

  /** Who else is in the same event room. */
  case class MemberList(members: Iterable[Option[Reader]]) extends IBEvent {
    def getName(r:Option[Reader]) = r.flatMap(_.nickname).getOrElse("Anonymous")
    
	override def toJson = Json.obj(
	      "type" -> JsString("members"),      
	      "members" -> Json.toJson(members.map(r => getName(r))),
	      "date" -> Json.toJson(System.currentTimeMillis())
	    )
  }
  
  
  implicit val timeout = Timeout(1.second)

  lazy val default = {
    val roomActor = Akka.system.actorOf(Props[EventRoom])
    roomActor
  }

  def join(listenerName:String, reader:Option[Reader], session:String, bookId:String, listenTo:ListenTo*) = {
    (default ? Join(listenerName, reader, session, bookId, listenTo:_*)).map {

      case Connected(listenerName, enumerator) => enumerator

      case CannotConnect(error) => 
        // Send an error and close the socket
        Enumerator[JsValue](CannotConnect(error).toJson).andThen(Enumerator.enumInput(Input.EOF))
    }
  }

  def notifyEventRoom(e:IBEvent) = default ! e

}

class EventRoom extends Actor {
  
  import EventRoom._
  
  /**
   * The members of the chat room and their streams
   */
  var members = Map.empty[String, Channel[JsValue]]

  /**
   * A map of listenTo -> listenerName
   */
  var subscribers = Map.empty[ListenTo, Set[String]]
  
  /**
   * A map of listenerName -> listenTo
   */
  var subscriptions = Map.empty[String, Set[ListenTo]]
  
  /**
   * The join request of the members of the event room
   */
  var memberJoins = Map.empty[String, Join] 
  
  /**
   * The various states that can be listened to in the event room
   */
  var states = Map.empty[ListenTo, State] 
 
  /**
   * Subscribe a listener to something that can be listened to
   */
  private def subscribe(listenerName:String, listenTo:ListenTo) {
	  subscribers = subscribers.updated(listenTo, subscribers.getOrElse(listenTo, Set.empty[String]) + listenerName)
      subscriptions = subscriptions.updated(listenerName, subscriptions.getOrElse(listenerName, Set.empty[ListenTo]) + listenTo)
      listenTo.onSubscribe(listenerName, this)
      broadcast(listenTo, MemberList(membersByLT(listenTo)))    
  }
  
  /**
   * Unsubscribe a listener to something that can be listened to
   */
  private def unsubscribe(listenerName:String, listenTo:ListenTo) {
      subscribers = subscribers.updated(listenTo, subscribers.getOrElse(listenTo, Set.empty[String]) - listenerName)
      subscriptions = subscriptions.updated(listenerName, subscriptions.getOrElse(listenerName, Set.empty[ListenTo]) - listenTo)
      broadcast(listenTo, MemberList(membersByLT(listenTo)))    
  }
  
  def receive = {

    case ej:Join => {
      
      val enumerator = Concurrent.unicast[JsValue](
        onStart = { channel =>
          if (members.contains(ej.listenerName)) {
            channel push CannotConnect("This username is already used").toJson
          } else {
            members = members + (ej.listenerName -> channel)
            memberJoins = memberJoins + (ej.listenerName -> ej)            

            // Push out the name of the listener
            channel push Json.obj("type" -> "connected", "listenerName" -> ej.listenerName)
            
            for (lt <- ej.listenTo) {
              subscribe(ej.listenerName, lt)
            }
          }
        },
        onComplete = { EventRoom.default.!(Quit(ej.listenerName)) }        
      )
      sender ! Connected(ej.listenerName, enumerator)
      
    }
    
    // Subscribe to a ListenTo
    case Subscribe(listenerName, listenTo) => subscribe(listenerName, listenTo)
    
    // Unsubscribe from a ListenTo
    case Unsubscribe(listenerName, listenTo) => unsubscribe(listenerName, listenTo)
    
    // Quit the room 
    case EventRoom.Quit(listenerName) => {   
      println("Quitting: " + listenerName)
      
      members = members - listenerName
      memberJoins = memberJoins - listenerName
      
      // Unsubscribe this listener from all streams
      for (subscription <- subscriptions.getOrElse(listenerName, Set.empty[ListenTo])) {
        subscribers = subscribers.updated(subscription, subscribers.getOrElse(subscription, Set.empty[String]) - listenerName)
        broadcast(subscription, MemberList(membersByLT(subscription)))        
      }
      subscriptions = subscriptions - listenerName
    }
    
    // For other events, do what the event says
    case event:IBEvent => {
      event.action(this)  
    }        

  }
  
  /**
   * Sends an event out to everyone who is listening to the same thing
   */
  def broadcast(key:ListenTo, event:IBEvent) {
    val json = event.toJson
    println("Sending " + json)
    
    for (
        listenerSet <- subscribers.get(key); 
        listenerName <- listenerSet; 
        listener <- members.get(listenerName)
    ) {
      println("Sending to " + listenerName)
      listener.push(json)
    }    
  }
  
  
  def membersByLT(lt:ListenTo):Iterable[Option[Reader]] = {
    val ltListeners = subscribers.get(lt).getOrElse(Set.empty[String])    
    val joins = for (listenerName <- ltListeners; join <- memberJoins.get(listenerName)) yield join
    
    // Collect a list of all sessions and their associated readers
    var sessionReader = mutable.Map.empty[String, Option[Reader]]
    for (j <- joins) {
      if (!sessionReader.contains(j.session) || sessionReader(j.session).isEmpty)
    	 sessionReader(j.session) = j.reader
    }
    
    // Return the sequence of readers (which may include many RefNone entries)
    sessionReader.values
  }  
  

}


