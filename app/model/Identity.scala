package model

case class Identity(
  kind:String,
  provider:String,
  id:String
) {
  var owner: AnyRef = null
}
