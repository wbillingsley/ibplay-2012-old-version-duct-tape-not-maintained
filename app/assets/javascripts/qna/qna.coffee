
Handlebars.registerHelper("qlink", (text, qId) -> 
  new Handlebars.SafeString(
    "<a href='/qna/" + qId + "'>" + text + "</a>"
  )
)

Handlebars.registerHelper("time", (t) -> 
  new Handlebars.SafeString(
    jQuery.timeago(new Date(t))
  )
)

Handlebars.registerHelper("markdown", (t) -> 
  new Handlebars.SafeString(
    IBQna.converter.makeHtml(t)
  )
)

window.IBQna = 

  qUrl: () -> "" + @qid + "/json"

  # CSS Selector indicating which element to fill out
  qSelector: "#question"

  init: (id) ->
    console.log("initialising with id" + id)
    @qid = id 
    if not @qTemplate?
      @qTemplate = Handlebars.compile($("#question-template").html())
      @qCommentTemplate = Handlebars.compile($("#q-comment-template").html())
      @ansCommentTemplate = Handlebars.compile($("#ans-comment-template").html())
    
    @converter = Markdown.getSanitizingConverter()

  data: []

  updateQuestion: () ->  
    jQuery.ajax({
      url: @qUrl()
      success: (data, status, req) ->
        IBQna.data = data
        $(IBQna.qSelector).html(IBQna.qTemplate(IBQna.data))
    })
    
  replaceWithQCommentForm: (el) ->    
    $(el).replaceWith(IBQna.qCommentTemplate({qid: IBData.qid}))
    
  replaceWithAnsCommentForm: (el) ->
    ansid = $(el).attr("data-answer-id")    
    $(el).replaceWith(IBQna.ansCommentTemplate({qid: IBData.qid, ansid: ansid}))
    
    
            
# This returns IBQna as a require.js module
#
define([], () -> window.IBQna)