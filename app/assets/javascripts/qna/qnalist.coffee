
Handlebars.registerHelper("qlink", (text, qId) -> 
  new Handlebars.SafeString(
    "<a href='/qna/" + qId + "'>" + text + "</a>"
  )
)

Handlebars.registerHelper("time", (t) -> 
  new Handlebars.SafeString(
    jQuery.timeago(new Date(t))
  )
)

window.IBQnaList = 

  qListUrl: "qna.json"

  # CSS Selector indicating which element to fill out
  qListSelector: "#question-list"

  init: () -> 
    if not @qListTemplate?
      @qListTemplate = Handlebars.compile($("#question-list-template").html())

  data: []

  updateQuestionsList: () ->
    @init()  
    console.log("updating list of Qs!")
    jQuery.ajax({
      url: @qListUrl
      success: (data, status, req) ->
        IBQnaList.data = data
        $(IBQnaList.qListSelector).html(IBQnaList.qListTemplate(IBQnaList.data))
    })
    
  
    
# This returns IBQnaList as a require.js module
#
define([], () -> window.IBQnaList)
    