/*
 * Brushed Time Series Chart
 *
 * Based on a Martin Bostock example where he proposed
 * this style as a convention.
 *
 * Modified to support brushes.
 */
function brushTimeSeriesChart() {
  var margin = {top: 20, right: 20, bottom: 20, left: 80},
      width = 760,
      height = 120,
      xValue = function(d) { return d[0]; },
      yValue = function(d) { return d[1]; },
      xScale = d3.time.scale(),
      yScale = d3.scale.linear(),
      xAxis = d3.svg.axis().scale(xScale).orient("bottom").tickSize(6, 0),
      yAxisLeft = d3.svg.axis().scale(yScale).ticks(6).orient("left"),
      area = d3.svg.area().x(X).y1(Y),
      line = d3.svg.line().x(X).y(Y),
      brushable = true,
      brushCallback = function(domain) {
      
      }
    
  function chart(selection) {
    selection.each(function(data) {

      // Convert data to standard representation greedily;
      // this is needed for nondeterministic accessors.
      data = data.map(function(d, i) {
        return [xValue.call(data, d, i), yValue.call(data, d, i)];
      });

      // Update the x-scale.
      xScale
          .domain(d3.extent(data, function(d) { return d[0]; }))
          .range([0, width - margin.left - margin.right]);

      // Update the y-scale.
      yScale
          .domain([0, d3.max(data, function(d) { return d[1]; })])
          .range([height - margin.top - margin.bottom, 0]);

      // Select the svg element, if it exists.
      var svg = d3.select(this).selectAll("svg").data([data]);

      // Otherwise, create the skeletal chart.
      var gEnter = svg.enter().append("svg").append("g");
      gEnter.append("path").attr("class", "area");
      gEnter.append("path").attr("class", "line");
      gEnter.append("g").attr("class", "x axis");
      gEnter.append("g").attr("class", "y axis");
      
      
      var brush = d3.svg.brush()
    		.x(xScale)
    		.on("brush", brush)
    		
      function brush() {
  			brushCallback(brush.empty() ? xScale.domain() : brush.extent())
	  }
    		          

      // Update the outer dimensions.
      svg .attr("width", width)
          .attr("height", height);

      // Update the inner dimensions.
      var g = svg.select("g")
          .attr("transform", "translate(" + margin.left + "," + margin.top + ")");

      // Update the area path.
      g.select(".area")
          .attr("d", area.y0(yScale.range()[0]));

      // Update the line path.
      g.select(".line")
          .attr("d", line);

      // Update the x-axis.
      g.select(".x.axis")
          .attr("transform", "translate(0," + yScale.range()[0] + ")")
          .call(xAxis);
          
      // Update the y-axis
      g.select(".y.axis")
          .attr("transform", "translate(-10,0)")
		  .call(yAxisLeft);
          
      if (brushable) {
      	gEnter.append("g")
      			.attr("class", "x brush")
      			.call(brush)
    			.selectAll("rect")
			      .attr("y", 0)
      			.attr("height", height);
      }
          
    });
  }

  // The x-accessor for the path generator; xScale xValue.
  function X(d) {
    return xScale(d[0]);
  }

  // The x-accessor for the path generator; yScale yValue.
  function Y(d) {
    return yScale(d[1]);
  }

  chart.margin = function(_) {
    if (!arguments.length) return margin;
    margin = _;
    return chart;
  };

  chart.width = function(_) {
    if (!arguments.length) return width;
    width = _;
    return chart;
  };

  chart.height = function(_) {
    if (!arguments.length) return height;
    height = _;
    return chart;
  };

  chart.x = function(_) {
    if (!arguments.length) return xValue;
    xValue = _;
    return chart;
  };

  chart.y = function(_) {
    if (!arguments.length) return yValue;
    yValue = _;
    return chart;
  };
  
  chart.brushCallback = function(_) {
    if (!arguments.length) return brushCallback;
    brushCallback = _;
    return chart;
  };
	
  return chart;
}


/*
 * A brush stick time series chart
 */ 
function brushTimeSeriesStickChart() {
  var margin = {top: 20, right: 20, bottom: 20, left: 80},
      width = 760,
      height = 120,
      xValue = function(d) { return d[0]; },
      yValue = function(d) { return d[1]; },
      xScale = d3.time.scale(),
      yScale = d3.scale.linear(),
      xAxis = d3.svg.axis().scale(xScale).orient("bottom").tickSize(6, 0),
      yAxisLeft = d3.svg.axis().scale(yScale).ticks(6).orient("left"),
      area = d3.svg.area().x(X).y1(Y),
      line = d3.svg.line().x(X).y(Y),
      brushable = true,
      brushCallback = function(domain) {
      
      }
    
  function chart(selection) {
    selection.each(function(data) {

      // Convert data to standard representation greedily;
      // this is needed for nondeterministic accessors.
      data = data.map(function(d, i) {
        return [xValue.call(data, d, i), yValue.call(data, d, i)];
      });

	  var extent = d3.extent(data, function(d) { return d[0]; })

      // Update the x-scale.
      xScale
          .domain(extent)
          .range([0, width - margin.left - margin.right]);

      // Update the y-scale.
      yScale
          .domain([0, d3.max(data, function(d) { return d[1]; })])
          .range([height - margin.top - margin.bottom, 0]);

      // Select the svg element, if it exists.
      var svg = d3.select(this).selectAll("svg").data([data]);

      // Otherwise, create the skeletal chart.
      var gEnter = svg.enter().append("svg").append("g");
      gEnter.append("g").attr("class", "x axis");
      gEnter.append("g").attr("class", "y axis");
      
      
      var brush = d3.svg.brush()
    		.x(xScale)
    		.on("brush", brush)
    		
      function brush() {
  			brushCallback(brush.empty() ? xScale.domain() : brush.extent())
	  }
    		          

      // Update the outer dimensions.
      svg.attr("width", width)
          .attr("height", height);

      // Update the inner dimensions.
      var g = svg.select("g")
          .attr("transform", "translate(" + margin.left + "," + margin.top + ")");
		
	  var lines = g.selectAll("rect").data(data)
	  
	  lines.exit().remove
	  lines.enter().append("line")
			
	  lines.attr("x1", function (d, i) { return xScale(d[0]) })
	  	   .attr("x2", function (d, i) { return xScale(d[0]) })
		   .attr("y1", function (d, i) { return yScale(d[1]) })
		   .attr("y2", height - margin.top - margin.bottom)

      // Update the area path.
      g.select(".area")
          .attr("d", area.y0(yScale.range()[0]));

      // Update the line path.
      g.select(".line")
          .attr("d", line);

      // Update the x-axis.
      g.select(".x.axis")
          .attr("transform", "translate(0," +  yScale.range()[0] + ")")
          .call(xAxis);
          
      // Update the y-axis
      g.select(".y.axis")
          .attr("transform", "translate(-10,0)")
		  .call(yAxisLeft);
          
      if (brushable) {
      	gEnter.append("g")
      			.attr("class", "x brush")
      			.call(brush)
    			.selectAll("rect")
			      .attr("y", 0)
      			.attr("height", height);
      }
          
    });
  }

  // The x-accessor for the path generator; xScale xValue.
  function X(d) {
    return xScale(d[0]);
  }

  // The x-accessor for the path generator; yScale yValue.
  function Y(d) {
    return yScale(d[1]);
  }

  chart.margin = function(_) {
    if (!arguments.length) return margin;
    margin = _;
    return chart;
  };

  chart.width = function(_) {
    if (!arguments.length) return width;
    width = _;
    return chart;
  };

  chart.height = function(_) {
    if (!arguments.length) return height;
    height = _;
    return chart;
  };

  chart.x = function(_) {
    if (!arguments.length) return xValue;
    xValue = _;
    return chart;
  };

  chart.y = function(_) {
    if (!arguments.length) return yValue;
    yValue = _;
    return chart;
  };
  
  chart.brushCallback = function(_) {
    if (!arguments.length) return brushCallback;
    brushCallback = _;
    return chart;
  };
	
  return chart;
}
 

/*
 * A Donut chart
 */
function donutChart() {
	var width = 960,
			height = 500,
			radius = Math.min(width, height) / 2,
			value = function(d) { return d[1] },
			label = function(d) { return "" + d[0] + " " + d[1] }
	
	var color = d3.scale.ordinal()
			.range(["#98abc5", "#8a89a6", "#7b6888", "#6b486b", "#a05d56", "#d0743c", "#ff8c00"]);
	
	var arc = d3.svg.arc()
			.outerRadius(radius - 10)
			.innerRadius(radius - 70);
	
  function chart(selection) {
    selection.each(function(data) {
	
			var pie = d3.layout.pie()
				.sort(null)
				.value(value);
				
      // Select the svg element, if it exists.
      var svg = d3.select(this).selectAll("svg").data([data]);

      // Otherwise, create the skeletal chart.
      var gEnter = svg.enter().append("svg")
                     .attr("width", width)
                     .attr("height", height)
                  .append("g")
                    .attr("transform", "translate(" + width / 2 + "," + height / 2 + ")");
                    
      var g = svg.select("g").selectAll(".arc")
                   .data(pie(data))

      var ge = g.enter().append("g")
                   .attr("class", "arc");

  		ge.append("path")
          .attr("d", arc)
          .style("fill", function(d, i) { return color(i); });

  		ge.append("text")
          .attr("transform", function(d) { return "translate(" + arc.centroid(d) + ")"; })
          .attr("dy", ".35em")
          .style("text-anchor", "middle")
            
          
      g.exit().remove()
      g.select(".arc path").attr("d", arc)    
      g.select(".arc text").attr("transform", function(d) { return "translate(" + arc.centroid(d) + ")"; }).text(function(d) { return label(d.data) });   
      
				
		});
	}
		
  chart.margin = function(_) {
    if (!arguments.length) return margin;
    margin = _;
    return chart;
  };

  chart.width = function(_) {
    if (!arguments.length) return width;
    width = _;
    return chart;
  };

  chart.height = function(_) {
    if (!arguments.length) return height;
    height = _;
    return chart;
  };

  chart.label = function(_) {
    if (!arguments.length) return label;
    label = _;
    return chart;
  };
		
  chart.value = function(_) {
    if (!arguments.length) return value;
    value = _;
    return chart;
  };

	return chart;


}


window.IBCharts = {
  brushTimeSeriesStickChart: brushTimeSeriesStickChart,
  
  brushTimeSeriesChart: brushTimeSeriesChart,
		
  donutChart: donutChart
}


define([],function() { return window.IBCharts }) 

