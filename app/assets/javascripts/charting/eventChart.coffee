# The aggregated events data we've loaded
#
# TODO: Re-merge this with bookChartEvents.coffee (it's essentially the same with a different URL)
#

aggEvents = []

chartData = []

onSuccess = (data, status, jqxhr) ->
	if data.aggEvents?
		aggEvents = data.aggEvents
	replaceChart()
	EC.initChart()


genTopicData = () -> genKeyData("topics")

genTotalData = () ->	
	v = for agg in aggEvents
			{x: new Date(agg.time["$date"]), y: agg.total} 
	console.log(v)
	chartData = [
		{
			key: "total",
			color: "#ff7f0e",
			values: v
		}
	]
	OurChart.data = v
	
	

genFunction = genTotalData


replaceChart = () ->
	genFunction()
	OurChart.makeChart()
	
	

window.EC = {

  # Fetches the data from the server and charts it
  fetchAndChart: (url) -> 
    $.ajax({
      type: "GET"
      url: url
      dataType: "json"
      success: (data) -> 
        if data.aggEvents?
          aggEvents = data.aggEvents
        replaceChart()
        EC.initChart()
    })

  aggEvents: () -> aggEvents
	
	# Converts the date to a data object
	datify: () ->
		@events = ([new Date(o.time["$date"]), o] for o in aggEvents)
		@events 
		
	# Pads missing hours with empty array entries
	pad: () ->
		@padded = if @events.length > 0
			start = @events[0][0]
			end = @events[@events.length - 1][0]
			
			arr = []
			t = new Date(start)
			cursor = 0
			while (t <= end)
				if (@events[cursor][0].getTime() == t.getTime())
					arr.push(@events[cursor])
					cursor++
				else
					arr.push([new Date(t), {}])
				t.setHours(t.getHours() + 1)			
			arr
		else
			[]
			
		@padded
		
	# Produces a d3-suitable 2D array, selecting a particular key from each object in padded
	select: (arr, f) -> 
		([t[0], f(t[1])] for t in arr)
	
	# Padded is an array [time, obj].  Each obj contains a number of maps.
	# This takes a 'mapSelectFunc' to choose one of those maps. The method then accumulates 
	# the totals for all the keys of that map it sees across the array.
	#
	# arr is passed as a parameter as it we may want to filter @padded
	#
	accum: (arr, mapSelectFunc) -> 
		acc = {}
		for t in arr
			m = mapSelectFunc(t[1])
			for key of m
				acc[key] = (acc[key] ? 0) + m[key]
		([key, acc[key]] for key of acc)
		
	accumKinds: () ->
		@accum(@filtered(), (o) -> o.kinds)

	accumNouns: () ->
		@accum(@filtered(), (o) -> o.nouns)

	accumAdjs: () ->
		@accum(@filtered(), (o) -> o.adjs)

	accumSites: () ->
		@accum(@filtered(), (o) -> o.sites)
				
	accumTopics: () ->
		@accum(@filtered(), (o) -> o.topics)
		
	accumSessions: () ->
		@accum(@filtered(), (o) -> o.sessions)
		
	accumReaders: () ->
		@accum(@filtered(), (o) ->
			m = {}
			for key of o.readers
				m[key] = o.readers[key]
			m["anonymous"] = o.anonymous
			m
		)
	
	
	selectAccumKinds: () -> 
		@accumulator = @accumKinds
		@updateCharts()
		
	selectAccumSites: () -> 
		@accumulator = @accumSites
		@updateCharts()
	
	selectAccumAdjs: () -> 
		@accumulator = @accumAdjs
		@updateCharts()
	
	selectAccumNouns: () -> 
		@accumulator = @accumNouns
		@updateCharts()
	
	selectAccumTopics: () -> 
		@accumulator = @accumTopics
		@updateCharts()

	selectAccumReaders: () -> 
		@accumulator = @accumReaders
		@updateCharts()

	selectAccumSessions: () -> 
		@accumulator = @accumSessions
		@updateCharts()

	
	# The selected interval	
	selectedInterval: null

	# updates the selected interval
	selectInterval: (d) -> 
		EC.selectedInterval = d
		EC.updateCharts() 

	
	# Filters the "padded" array to only those in the selected interval
	filtered: () -> 
		if (EC.selectedInterval?) 
			@padded.filter((v) -> v[0] >= EC.selectedInterval[0] && v[0] <= EC.selectedInterval[1])
		else
			@padded
			
		
	# Initialises the charts		
	initChart: () ->
		@datify()
		
		# HACK: We've shortcutted the padding.
		@padded = @events
		
		@accumulator = @accumNouns
		
		@tsBrush = new IBCharts.brushTimeSeriesStickChart().height(300).width(800).brushCallback(@selectInterval)
		d3.select("#brush-time-series").datum(@select(@padded, (obj) -> obj.total ? 0)).call(@tsBrush)
		
		@donut = new IBCharts.donutChart()
		d3.select("#pie-chart").datum(@accumulator()).call(@donut) 
	
	# Updates the dependent pie charts
	updateCharts: () ->
		d3.select("#pie-chart").datum(@accumulator()).call(@donut)
	
}


	
	
window.OurChart = 

	data: []
	
	max: 100
	
	startDate: new Date(0)
	
	endDate: new Date()
	
	w: 800
	h: 400
	
	margT: 80
	margB: 80
	margL: 80
	margR: 80

	makeChart: () ->
		@startDate = d3.min(@data, (d) -> d.x)
		@endDate = d3.max(@data, (d) -> d.x)
		@max = d3.max(@data, (d) -> d.y)
	
		x = d3.time.scale().domain([@startDate, @endDate]).range([0, @w])
		@x = x
		# x.tickFormat(d3.time.format("%Y-%m-%d"));
		y = d3.scale.linear().domain([0, @max]).range([@h, 0])
		@y = y
		# Add an SVG element with the desired dimensions and margin.
		$("#chart").children().remove()
		graph = d3.select("#chart").append("svg:svg")
			.attr("width", @w + @margL + @margR)
			.attr("height", @h + @margT + @margB)
			.append("svg:g")
			.attr("transform", "translate(" + @margL + "," + @margT + ")")
			      
		xAxis = d3.svg.axis().scale(x).tickSize(-@h).tickSubdivide(1)
		# Add the x-axis.
		graph.append("svg:g")
		      .attr("class", "x axis")
		      .attr("transform", "translate(0," + @h + ")")
		      .call(xAxis);
		
		
		# create left yAxis
		yAxisLeft = d3.svg.axis().scale(y).ticks(6).orient("left");
		# Add the y-axis to the left
		graph.append("svg:g")
		      .attr("class", "y axis")
		      .attr("transform", "translate(-10,0)")
		      .call(yAxisLeft);
		
		barW = (@w * 60 * 60 * 1000) / (@endDate.getTime() - @startDate.getTime())
		
		h = @h
		showSelected = @showSelected
		graph.selectAll("rect").data(@data)
			.enter().append("rect")
			.attr("x", (d, i) -> x(d.x) - barW / 2)
			.attr("y", (d, i) -> y(d.y))
			.attr("width", barW)
			.attr("height", (d) -> h - y(d.y))
			.on("mouseover", (d, i) -> 
				OurChart.showSelected(i)
			)


	showPie2: (selector, hash, title) ->
		nv.addGraph(() ->
			data = for key of hash
					{ 
						label: key
						value: hash[key]
					}
					
					
			nvData = [{
				key: title
				values: data		
			}]
					
			console.log("foo")
			console.log(nvData)
		
			chart = nv.models.pieChart()
				.x((d) -> d.label)
				.y((d) -> d.value)
				.showLabels(true)
				
			d3.select(selector).datum(nvData).call(chart)
			chart
		)

	showPie: (selector, hash, title) ->
		el = $(selector)
		el.children().remove()
		for key of hash
			html = "<p>" + key + ": " + hash[key] + "<p>"
			el.append(html)
			
	showSelected: (i) ->	
		evt = aggEvents[i]
		
		date = new Date(evt.time["$date"])
		total = evt.total
		
		$("#detail-time").text(date.toLocaleString())
		$("#detail-total").text(total)
		
		@showPie("#detail-nouns", evt.nouns, "nouns")
		@showPie("#detail-topics", evt.topics, "topics")
		@showPie("#detail-kinds", evt.kinds, "kinds")
		@showPie("#detail-sites", evt.sites, "sites")
	

		



randomColor = () ->
	c = () -> Math.floor(Math.random()*256).toString(16)
	"#" + c() + c() + c()

genKeyData = (key) ->
	keyset = {}
	for agg in aggEvents
		for subkey, value of agg[key]
			keyset[subkey] = value
	chartData = for subkey, value of keyset 
		{ 
			key: subkey, 
			color: randomColor(), 
			values: {x: new Date(agg.time["$date"]), y: agg[key][subkey]} for agg in aggEvents 
		} 
			
  
# This returns IBMarkdown as a require.js module
#
define(["charting/charts"], (IBCharts) -> window.EC)
	