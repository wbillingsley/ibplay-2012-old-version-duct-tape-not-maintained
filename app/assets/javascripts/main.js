/*
 * Require.js set of modules for various info style pages.
 * (At the moment we're just including everything!)
 */
require([
   "helper/eventRoom",
   "helper/chatStream",
   "helper/wikiSubstitutions",
   "helper/pollResults",
   "helper/textPollResults",
   "helper/entryComments",
   "helper/addContent",
   "markdown/ibMarkdown"
], function(l) {
	console.log("library has loaded")
})