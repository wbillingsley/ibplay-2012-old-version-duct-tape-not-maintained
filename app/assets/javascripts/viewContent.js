/*
 * Require.js set of modules for the content viewer.
 * (At the moment we're just including everything!)
 */
require([
   "helper/lookupContent",
   "helper/eventRoom",
   "helper/chatStream",
   "helper/wikiSubstitutions",
   "helper/pollResults",
   "helper/textPollResults",
   "helper/entryComments",
   "helper/addContent",   
   "helper/prevNext"
], function(l) {
	console.log("library has loaded")
	resizeContentFrames()
})