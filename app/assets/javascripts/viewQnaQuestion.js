require([
   "markdown/ibMarkdown",
   "qna/qna"
], function(IBMarkdown, IBQna) {
	console.log("library has loaded")
	
	IBQna.init(IBData.qid)
	IBQna.updateQuestion()	
	
	var converter1 = IBMarkdown.getSanitizingConverter();	
	var editor1 = new Markdown.Editor(converter1);
	editor1.run()	  
})