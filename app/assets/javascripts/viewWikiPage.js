require([
   "helper/eventRoom",
   "helper/chatStream",
   "helper/wikiSubstitutions",
   "helper/pollResults",
   "helper/textPollResults"
], function(IBEventRoom, IBChatStream, IBWikiSubstitutions, IBPollResults, IBTextPollResults) {
	console.log("library has loaded")
	
	var pageResizer = function() {
		var h = $(window).height()
		$(".page").css("min-height", 
		    function (index, value) {
				return "" + h - ($(this).outerHeight(true) - $(this).height()) + "px"
			}
		)
	}
	$(window).resize(pageResizer)
	pageResizer()
	
	IBWikiSubstitutions.run()
		
	
})