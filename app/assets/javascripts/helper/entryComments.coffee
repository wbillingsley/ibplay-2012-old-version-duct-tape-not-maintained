#
# Calls the JSON API to retrieve, view, and submit comments 
#
class CommentsViewer
	constructor: () ->

	curPage: 0

	skip: 0
	
	limit: 20
	
	lastPage: 0
	
	entryId: 0
	
	view: (entryId) -> 
		@entryId = entryId
		@skip = 0
		@update()
		
	update: () -> 
		# call the server
		$.ajax({
			type: "GET"
			url: "/entry/" + @entryId + "/comments.json"
			data: {
			 skip: @skip
			}
			success: @success
		})
		
	skipTo: (skip) ->
		@skip = skip
		@update()
		
	
		
	updateView: () ->
		c = d3.select(@selector).select(".comments").selectAll(".comment").data(@data.comments)
		
		# Remove any excess comment blocks
		c.exit().remove()
		
		# Add the structure of any comment blocks we need
		e = c.enter().append("div").classed("comment", true)
		e.append("div").classed("from", true)
		e.append("div").classed("comment-text", true)
		e.append("div").classed("date", true)
		
		# Update the content of all comment blocks
		c.select(".from").text((d) -> d.from.nickname ? "Anonymous")
		c.select(".comment-text").text((d) -> d.comment)
		c.select(".date").text((d) -> new Date(d.date).toLocaleString())
		

		p = ({
				p: i
				active: @curPage == i
				text: "" + (i + 1)
			} for i in [0..@lastPage]
		)
		
		p.push({ 
				p: @curPage + 1
				disabled: @curPage >= @lastPage
				active: false
				text: '>'
		})

		p.unshift({ 
			p: @curPage - 1
			disabled: @curPage == 0
			active: false
			text: '<'
		})
		
		
		console.log(p)
		
		pag = d3.select(@selector).select(".pagination").select("ul").selectAll("li").data(p)
		
		pag.exit().remove()
		pag.enter().append("li").append("a")
		
		viewer = this
		l = @limit
		pag.classed("active", (d) -> d.active ? false)	
		pag.classed("disabled", (d) -> d.disabled ? false)
		pag.select("a").attr("href", (d) -> 
			if (d.disabled or d.active)
				"javascript: {}"
			else		
				"javascript: CommentsViewer.skipTo(" + d.p * l + ")"		
		).text((d) -> d.text)
		
	
	# Submits the content form via AJAX	
	submitComment: () ->		
		data = new FormData($("#submit-comment").get()[0])
		client = new XMLHttpRequest()
		url = "/entry/" + IBViewer.currCe()?.id + "/addComment"
		client.open("POST", url, false)
		client.send(data)
		@skipTo(-1)
	
		
	
	# d3.js selector for the comments container	
	selector: "#entry-comments"
	
	# Callback for success
	success: (data) => 
		@data = data
	
		@lastPage = Math.max(Math.floor((data.count - 1) / @limit), 0) 
		
		@curPage = if (@skip >= 0)
			Math.floor(@skip / @limit)
		else
			@lastPage
	
		console.log(data)
		
		@updateView()
	
	
		
	
window.CommentsViewer = new CommentsViewer()


# This returns CommentsViewer as a require.js module
#
define(() -> window.CommentsViewer)
