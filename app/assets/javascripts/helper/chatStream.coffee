##########################################################
# Chat
##########################################################

chatSelector = ".chat-stream"
memberListSelector = ".member-list"

chatComments = []

memberList = []

# Calculates the submit URL for a given poll id
pollSubmitUrl = (pid) ->
  "/poll/" + pid + "/vote"
  
ajaxSubmitPoll = (pid, form) ->
    data = new FormData(form.get()[0])
    client = new XMLHttpRequest()
    client.open("POST", pollSubmitUrl(pid), false)
    client.send(data)


# Calculates the submit URL for a given poll id
textPollSubmitUrl = (pid) ->
  "/textPoll/" + pid + "/vote"

ajaxSubmitTextPoll = (pid, form) ->
    data = new FormData(form.get()[0])
    client = new XMLHttpRequest()
    client.open("POST", textPollSubmitUrl(pid), false)
    client.send(data)


addChatMsg = (msg) ->
    chatComments.push(msg)
    console.log("Added chat message: " + msg)

prependComments = (msgs) ->
    chatComments = msgs.concat(chatComments)
    updateChatStream()

chatDateString = (d) ->
    date = new Date(d.date)

    today = new Date()
    today.setHours(0)
    today.setMinutes(0)
    today.setSeconds(0)
    tomorrow = new Date(today)
    tomorrow.setHours(24)

    if (today <= date and date < tomorrow)
        date.toLocaleTimeString()
    else
        date.toDateString()

chatNameString = (d) ->
    d.name ? "anon"

linkify = (str) ->
    # Adapted from a post by Ricky Rosario
    if (str)
        str = str.replace(
            /((https?\:\/\/)|(www\.))(\S+)(\w{2,4})(:[0-9]+)?(\/|\/([\w#!:.?+=&%@!\-\/]))?/gi,
            (url) ->
                full_url = url;
                if !full_url.match('^https?:\/\/')
                    full_url = 'http://' + full_url;
                '<a href="' + full_url + '">' + url + '</a>'
        )
    str


# Updates the list of chat messages being displayed
updateChatStream = () ->
  sel = d3.select(chatSelector).selectAll(".comment").data(chatComments)
  sel.exit().remove()
  entering = sel.enter().insert("div", ":first-child").classed("comment", true)
    .classed("question", (d) -> !!d.content?.match("^Q:"))

  entering.each((d, i) ->
    sel = d3.select(this)
    switch d.type
      when "chat"
        sel.append("div").classed("from", true).text((d) -> chatNameString(d))
        content = sel.append("div").classed("text", true)
        # Insert and then linkify the text. Inserting as a text node first should ensure
        # it's not possible to cross-site-script the chat comments. (The html will be of
        # a text node, and any HTML tags inserted by the user will have been escaped)
        content.text((d) -> d.content)
        content.html((d) -> linkify(d3.select(this).html()))
        sel.append("div").classed("date", true).text((d) -> chatDateString(d))
      
      when "poll"
        pollBlock = sel.append("form").classed("poll", true)
        header = pollBlock.append("div").classed("poll-header", true).text("Question:")
        content = pollBlock.append("div").classed("text", true)
        content.text((d) ->  d.question)
        opts = pollBlock.selectAll(".option").data(d.options)
        oe = opts.enter().append("div").classed("option", true)
        oe.append("input")
          .attr("type", (optDat) -> switch d.mode
            when "chooseOne" then "radio"
            when "chooseMany" then "checkbox"
          )
          .attr("name", "answer")
          .attr("value", (optDat, i) -> i)
        oe.append("label").text((optDat) -> optDat) 
        pollBlock.append("button").classed("btn btn-mini btn-primary", true).attr("type", "button")
          .on("click", (d, i) ->
            f = $(this).closest("form")
            $(this).attr("disabled", true)
            ajaxSubmitPoll(d.pid, f)          
            f.find("input").attr("disabled", true)
          ) 
          .text("Vote")
          
      when "textPoll"
        pollBlock = sel.append("form").classed("poll", true)
        header = pollBlock.append("div").classed("poll-header", true).text("Question:")
        content = pollBlock.append("div").classed("text", true)
        content.text((d) ->  d.question)
        opt = pollBlock.append("div").append("textarea")
          .attr("name", "answer")
        pollBlock.append("button").classed("btn btn-mini btn-primary", true).attr("type", "button")
          .on("click", (d, i) ->
            f = $(this).closest("form")
            $(this).attr("disabled", true)
            ajaxSubmitTextPoll(d.pid, f)          
            f.find("input").attr("disabled", true)
          ) 
          .text("Vote")      
  )


# Updates the list of readers present in the chat room
updateChatMemberList = () ->
  sel = d3.select(memberListSelector).selectAll(".member").data(memberList)
  sel.exit().remove()
  sel.enter().insert("li").classed("member", true)
  sel.text((d) -> d) 




# The chat stream for a book
#
window.IBChatStream =

    started: false

    start: () ->      
      if not @started               
        @started = true        
        fetchRecentComments()
        @startListening()

    prependComments: prependComments

    # Pre-fetch the last 50 chat comments
    fetchRecentComments: fetchRecentComments = () ->
      $.ajax({
          url: "recentComments"
          type: "GET"
          success: (data) ->
              if data.comments?
                  IBChatStream.prependComments(data.comments.reverse())
      })

    # Start listening for chat comments
    startListening: () ->
        console.log("Starting listening to the event stream")
        EventRoom.init(IBData.bookId)
        EventRoom.eventViewer.addListener(this)
        EventRoom.eventViewer.connect({ type: "chatStream", id: IBData.bookId })        

    chat: (data) ->
      textbox = $("#chat-content")
      msg = textbox.val()
      textbox.val("")
      anon = $("#chat-anon:checked").val() ? false
      data.anonymous = anon
      data.content = msg
      $.ajax({
          url: "chat"
          type: "POST"
          data: data
      })

    receive: (msg) ->
        switch msg.type
          when "chat" 
              addChatMsg(msg)
              updateChatStream()
          when "members" 
            memberList = msg.members
            updateChatMemberList()
          when "poll"
            addChatMsg(msg)
            updateChatStream()
          when "textPoll"
            addChatMsg(msg)
            updateChatStream()






# This returns IBChatStream as a require.js module
#
define(["./eventRoom"], (EventRoom) -> window.IBChatStream)

