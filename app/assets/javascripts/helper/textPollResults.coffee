testData = [
        {
            word: "alpha"
            count: 37
        },
        {
            word: "bravo"
            count: 17
        },
        {
            word: "charlie"
            count: 12
        }
    ]

totalCount = (data) ->
    tot = 0
    for d in data
        tot = tot + d.count
    tot

maxCount = (data) ->
    max = 0
    for d in data
        if d.count > max
            max = d.count
    max

updateResults = (id, data) ->
    
    tot = totalCount(data)
    max = maxCount(data)
    console.log(data)    

    graph = d3.select("#text-poll-results-" + id).selectAll(".word").data(data, (d) -> d.word)
    graph.exit().remove()
    eopt = graph.enter().append("span").classed("word", true)
      .on("click", (d) -> IBTextPollResults.queryAnswers(id, d.word))

    fontSize = (d) -> (20 + (25 * d.count / max) + (25 * d.count / tot))

    graph.text((d) -> d.word + " ")
      .style("font-size", (d) -> "#{fontSize(d)}px" )
      .style("line-height", (d) -> "#{fontSize(d) * 1.3}px" )

pollTemplate = Handlebars.compile("""
  <div class="poll">
    <div class="question">{{question}}</div>
    <div class="push-to-chat">
      <button class="btn btn-mini" onClick="IBTextPollResults.pushPollToChat('{{id}}');">Push to chat</button>
    </div>
    <div>
      <textarea name="answer" required></textarea>
    </div>
    
  </div>
""")

pollResultsTemplate = Handlebars.compile("""
  <div class="pollResults">
    <div class="question">{{question}}</div>
    <div class="poll-results" id="text-poll-results-{{id}}">
    
    </div>    
  </div>
""")

pollResultsListTemplate = Handlebars.compile("""
    <div class="modal hide fade" id="results-list-{{id}}">
      <div class="answers text-poll-answers"></div>          
    </div>
""")



window.IBTextPollResults =
    updateResults: updateResults

    start: (pollId) ->
        console.log("Starting listening to the event stream")
        EventRoom.init(IBData.bookId)
        EventRoom.eventViewer.addListener(this)
        EventRoom.eventViewer.connect({ type: "textPollResults", id: pollId })        
    
    setData: (id, d) ->
        updateResults(id, data)

    receive: (d) ->
      switch d.type
        when "textPollResults"
          console.log(d)          
          updateResults(d.id, d.results)
        
    pushPollToChat: (pid) ->
    	$.ajax({
    		type: "POST"
    		url: "/textPoll/" + pid + "/pushToChat"    		
    	})

    
    # Looks up the answers containing a word on the server, and
    # display them	      
    queryAnswers: (pid, word) ->
      $.ajax({
        type: "GET"
        url: "/textPoll/" + pid + "/answers?word=" + escape(word)
        dataType: "json"
        success: (data) ->
          IBTextPollResults.popupAnswers(pid, data.answers)
      })
      
    # Pops up a list containing an array of answers
    # { answer: "my answer", date: intTimestamp }
    popupAnswers: (pid, answers) ->
    
      list = d3.select("#results-list-" + pid).select(".answers").selectAll(".answer").data(answers, (d) -> d.answer)
      list.exit().remove()
    	
      divs = list.enter().append("div").classed("answer", true)
      divs.append("div").classed("text", true).text((d) -> d.answer)
      divs.append("div").classed("date", true).text((d) -> new Date(d.date).toString())
      $("#results-list-" + pid).modal({show: true})
    	
    # Renders a poll into a DOM element
    renderPoll: (pid, domEl) ->
      jQuery.ajax({
        type: "GET"
        url: "/textPoll/" + pid
        dataType: "json"
        headers: { Accept: "application/json" }
        success: (data) -> 
          data.chooseOne = data.mode == "chooseOne"
          console.log(data)
          $(domEl).html(pollTemplate(data))
      })
      
    # Renders a pollResults section into a DOM element
    renderPollResults: (pid, domEl) ->
      console.log("calling for poll results")
      jQuery.ajax({
        type: "GET"
        url: "/textPoll/" + pid
        dataType: "json"
        headers: { Accept: "application/json" }
        success: (data) -> 
          console.log(data)
          $(domEl).html(pollResultsTemplate(data))
          IBTextPollResults.start(pid)
          $("body").append(pollResultsListTemplate(data))
      })    	
    	
# On load actions

# Register a replacement for rendering polls
IBWikiSubstitutions?.registerReplacement("textPoll", (domEl) -> 
  IBTextPollResults.renderPoll($(domEl).attr("data-ib-poll"), domEl)
)

# Register a replacement for rendering polls
IBWikiSubstitutions?.registerReplacement("textPollResults", (domEl) -> 
  IBTextPollResults.renderPollResults($(domEl).attr("data-ib-poll"), domEl)
)



# This returns IBTextPollResults as a require.js module
#
define(["./eventRoom", "./wikiSubstitutions"], (EventRoom, IBWikiSubstitutions) -> window.IBTextPollResults)

