#
# Accepts a standard JSON response from the server.
#
# If the Javascript IBViewer object is present, then we are in the lookup view, and 
# can just tell it to set its state accordingly.
#
# If it is not present, then we need to redirect to the appropriate view
#
handleStandardJsonResponse = (data) ->
  if IBViewer?
    IBViewer.handleStandardJsonResponse(data)
  else 
    console.log(data)


window.IBAddForms =
    topics: []
    nouns: []
    adjs: []    

    # Initialises an add form for the current status
    initForAdd: (selector, addToPres, initNouns) ->
        IBAddForms.nouns = initNouns
        viewing = IBViewer?.viewing()
        
        if viewing?.presentation?
            IBAddForms.adjs = viewing.presentation.adjectives ? []
            IBAddForms.topics = viewing.presentation.topics ? []
        else if IBViewer?.currCe()
            c = IBViewer?.currCe()
            IBAddForms.adjs = c.adjectives ? []
            IBAddForms.topics = c.topics ? []
        else
            d = viewing?.lookupData ? {}
            IBAddForms.adjs = if d.adj?
                [ d.adj ]
            else
                []
            IBAddForms.topics = if d.topic?
                [ d.topic ]
            else
                []
        if (viewing?.presentation?)
            $(selector).find(".presentation").val(viewing?.presentation.pid)
            $(selector).find(".add-to-pres").attr("checked", addToPres).attr("disabled", false)
        else
            $(selector).find(".presentation").val("")
            $(selector).find(".add-to-pres").attr("checked", false)
            $(selector).find(".add-to-pres").attr("checked", false).attr("disabled", true)
        IBAddForms.updateFormTags(selector)


    removeAdj: (selector, tag) ->
        IBAddForms.adjs = (adj for adj in IBAddForms.adjs when adj != tag)
        IBAddForms.updateFormTags(selector)

    removeNoun: (selector, tag) ->
        console.log("remove " + selector + " " + tag)
        IBAddForms.nouns = (noun for noun in IBAddForms.nouns when noun != tag)
        IBAddForms.updateFormTags(selector)

    removeTopic: (selector, tag) ->
        IBAddForms.topics = (topic for topic in IBAddForms.topics when topic != tag)
        IBAddForms.updateFormTags(selector)

    addAdj: (selector) ->
        tag = $.trim($(selector).find(".add-tag-inp").val())
        if (tag != "" and $.inArray(tag, IBAddForms.adjs) < 0)
            IBAddForms.adjs.push(tag)
            IBAddForms.updateFormTags(selector)

    addNoun: (selector) ->
        tag = $.trim($(selector).find(".add-tag-inp").val())
        if (tag != "" and $.inArray(tag, IBAddForms.nouns) < 0)
            IBAddForms.nouns.push(tag)
            IBAddForms.updateFormTags(selector)

    addTopic: (selector) ->
        tag = $.trim($(selector).find(".add-tag-inp").val())
        if (tag != "" and $.inArray(tag, IBAddForms.topics) < 0)
            IBAddForms.topics.push(tag)
            IBAddForms.updateFormTags(selector)

    # Show the modal dialog for editing an existing page's tags
    showEditTags: (c, presentation) ->
        selector = "#edit-tags"
        if c
            $("#edit-tags .entry-id").html(c.id)
            IBAddForms.adjs = c.adjectives ? []
            IBAddForms.nouns = c.nouns ? []
            IBAddForms.topics = c.topics ? []
            @editTagsC = c
        else
            # Shouldn't be able to edit tags of nothing!
            console.log("Edit tags called for no entry")
            return
        $(selector).find(".presentation").val(presentation?.pid ? "")
        IBAddForms.updateFormTags(selector)
        $(selector).find(".in-trash").attr("checked", c.inTrash ? false)
        $(selector).find(".show-first").attr("checked", c.showFirst ? false)
        $(selector).find(".protect").attr("checked", c.protect ? false)
        $(selector).find(".ce_name").val(c.name ? "")
        $(selector).find(".ce_note").val(c.note ? "")
        $("#edit-tags-modal").modal("show")

    # Posts a form to the server to add a webpage
    #
    performEditTags: (selector, url) ->
        selector = "#edit-tags"
        url = "../../entry/" + @editTagsC.id + "/tags" # Beware, relative URL to parent's parent
        data = new FormData($(selector).get()[0])
        client = new XMLHttpRequest()
        client.open("POST", url, false)
        client.send(data)
        # TODO handle errors
        resp = JSON.parse(client.responseText)
        # Hide all modals
        $(".modal").modal('hide')
        # Handle the response
        handleStandardJsonResponse(resp)

    # Show the modal dialog for adding a webPage
    showAddWeb: (addToPresentation) ->
        selector = "#add-url-entry"
        IBAddForms.initForAdd(selector, addToPresentation, ["Web Page"])
        $("#add-url-entry-modal").modal("show")

    # Show the modal dialog for adding a rich text wiki page
    showAddWikiPage: (addToPresentation) ->
        selector = "#add-wikipage-entry"
        IBAddForms.initForAdd(selector, addToPresentation, ["Rich Text Page"])
        $("#add-wikipage-entry-modal").modal("show")

    # Show the modal dialog for adding a markup (Markdown, etc) wiki page
    showAddMarkupWikiPage: (addToPresentation) ->
        selector = "#add-markup-wikipage-entry"
        IBAddForms.initForAdd(selector, addToPresentation, ["Wiki Page"])
        $("#add-markup-wikipage-entry-modal").modal("show")

    # Show the modal dialog for adding a wiki page
    showAddPoll: (addToPresentation) ->
        selector = "#add-poll-entry"
        IBAddForms.initForAdd(selector, addToPresentation, ["Poll"])
        $("#add-poll-entry-modal").modal("show")

    # Show the modal dialog for adding a wiki page
    showAddYouTube: (addToPresentation) ->
        selector = "#add-youtube-entry"
        IBAddForms.initForAdd(selector, addToPresentation, ["Video"])
        $("#add-youtube-entry-modal").modal("show")

    # Show the modal dialog for uploading a zip of images
    showUploadZip: (addToPresentation) ->
        selector = "#upload-zip-entry"
        IBAddForms.initForAdd(selector, addToPresentation, ["Slide"])
        $("#upload-zip-entry-modal").modal("show")

    # Posts a form to the server to add a webpage
    #
    performAdd: (selector, url) ->
        data = new FormData($(selector).get()[0])
        client = new XMLHttpRequest()
        client.open("POST", url, false)
        client.setRequestHeader("Accept","application/json")
        client.send(data)
        # TODO handle errors
        resp = JSON.parse(client.responseText)
        # Hide all modals
        $(".modal").modal('hide')
        # Handle the response
        handleStandardJsonResponse(resp)

    updateFormTags: (formSelector) ->
        els = d3.select(formSelector).select(".tags").selectAll(".adj").data(IBAddForms.adjs, (d) -> d)
        els.exit().remove()
        newAdj = els.enter().append("span").classed("adj", true)
        newAdj.append("span")
        newAdj.append("input").attr("type", "hidden")
        newAdj.append("button").attr("type", "button").classed("delete", true).text("\u00D7")
        els.select("span").text((d) -> d)
        els.select("input").attr("name", (d,i) -> "ce_adj").attr("value", (d) -> d)
        els.select("button").on("click", (d) -> IBAddForms.removeAdj(formSelector, d))

        els = d3.select(formSelector).select(".tags").selectAll(".noun").data(IBAddForms.nouns, (d) -> d)
        els.exit().remove()
        newAdj = els.enter().append("span").classed("noun", true)
        newAdj.append("span")
        newAdj.append("input").attr("type", "hidden")
        newAdj.append("button").attr("type", "button").classed("delete", true).text("\u00D7")
        els.select("span").text((d) -> d)
        els.select("input").attr("name", (d,i) -> "ce_noun").attr("value", (d) -> d)
        els.select("button").on("click", (d) -> IBAddForms.removeNoun(formSelector, d))

        els = d3.select(formSelector).select(".tags").selectAll(".topic").data(IBAddForms.topics, (d) -> d)
        els.exit().remove()
        newAdj = els.enter().append("span").classed("topic", true)
        newAdj.append("span")
        newAdj.append("input").attr("type", "hidden")
        newAdj.append("button").attr("type", "button").classed("delete", true).text("\u00D7")
        els.select("span").text((d) -> d)
        els.select("input").attr("name", (d,i) -> "ce_topic").attr("value", (d) -> d)
        els.select("button").on("click", (d) -> IBAddForms.removeTopic(formSelector, d))
        
        
        
# This returns IBAddContent as a require.js module
#
define(() -> window.IBAddContent)
        