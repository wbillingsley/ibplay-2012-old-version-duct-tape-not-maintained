testData = [
        {
            option: "a"
            votes: 37
        },
        {
            option: "b"
            votes: 3
        },
        {
            option: "c"
            votes: 57
        }
    ]

data = testData

totW = 0
recalcW = ->
    totW = totalVotes()

w = (val) ->
    "#{(val * 100) / totW}%"

totalVotes = () ->
    tot = 0
    for d in data
        tot = tot + d.votes
    tot

maxVotes = () ->
    max = 0
    for d in data
        if d.votes > max
            max = d.votes
    max

updateResults = (id) ->
    recalcW()

    graph = d3.select("#poll-results-" + id).selectAll(".option").data(data)
    graph.exit().remove()
    eopt = graph.enter().append("div").classed("option", true)
    eopt.append("div").classed("text", true)
    eopt.append("div").classed("votesnum", true)
    eopt.append("div").classed("votesbar", true).append("div").append("div").classed("progress-bar progress-striped", true).append("div").classed("bar", true)

    max = maxVotes()

    graph.each((d,i) ->
        row = d3.select(this)
        row.select(".text").html((d) -> d.option)
        row.select(".votesnum").html((d) -> d.votes)
        row.select(".votesbar .bar").style("width", (d) -> w(d.votes))
        row.select(".progress-bar").classed("progress-warning", d.votes >= max)
    )

pollTemplate = Handlebars.compile("""
  <div class="poll">
    <div class="question">{{question}}</div>
    <div class="push-to-chat">
      <button class="btn btn-mini" onClick="IBPollResults.pushPollToChat('{{id}}');">Push to chat</button>
    </div>
    {{#if chooseOne}}
      <ul class="options">
        {{#each options}}
          <li class="option"><input type="radio" name="answer" value="{{value}}" /> <label>{{text}}</label></li>
        {{/each}}    
       </ul>    
    {{else}}
      <div class="options">
        {{#each options}}
          <div class="option"><input type="checkbox" name="answer" value="{{value}}" /> <label>{{text}}</label></div>
        {{/each}}    
       </div>        
    {{/if}}
    
  </div>
""")

pollResultsTemplate = Handlebars.compile("""
  <div class="pollResults">
    <div class="question">{{question}}</div>
    <div class="poll-results" id="poll-results-{{id}}">
    
    </div>
  </div>
""")




window.IBPollResults =
    updateResults: updateResults

    start: (pollId) ->
        console.log("Starting listening to the event stream")
        EventRoom.init(IBData.bookId)
        EventRoom.eventViewer.addListener(this)
        EventRoom.eventViewer.connect({ type: "pollResults", id: pollId })        
    
    setData: (d) ->
        data = d
        updateResults()

    receive: (d) ->
      switch d.type
        when "pollResults"
          console.log(d)
          data = d.results
          updateResults(d.id)
        
    pushPollToChat: (pid) ->
    	$.ajax({
    		type: "POST"
    		url: "/poll/" + pid + "/pushToChat"    		
    	})
    	
    # Renders a poll into a DOM element
    renderPoll: (pid, domEl) ->
      jQuery.ajax({
        type: "GET"
        url: "/poll/" + pid
        dataType: "json"
        headers: { Accept: "application/json" }
        success: (data) -> 
          data.chooseOne = data.mode == "chooseOne"
          console.log(data)
          $(domEl).html(pollTemplate(data))
      })
      
    # Renders a pollResults section into a DOM element
    renderPollResults: (pid, domEl) ->
      console.log("calling for poll results")
      jQuery.ajax({
        type: "GET"
        url: "/poll/" + pid
        dataType: "json"
        headers: { Accept: "application/json" }
        success: (data) -> 
          data.chooseOne = data.mode == "chooseOne"
          console.log(data)
          $(domEl).html(pollResultsTemplate(data))
          IBPollResults.start(pid)
      })
      

# On load actions

# Register a replacement for rendering polls
IBWikiSubstitutions?.registerReplacement("poll", (domEl) -> 
  IBPollResults.renderPoll($(domEl).attr("data-ib-poll"), domEl)
)

# Register a replacement for rendering polls
IBWikiSubstitutions?.registerReplacement("pollResults", (domEl) -> 
  IBPollResults.renderPollResults($(domEl).attr("data-ib-poll"), domEl)
)



# This returns IBPollResults as a require.js module
#
define(["./eventRoom", "./wikiSubstitutions"], (EventRoom, IBWikiSubstitutions) -> window.IBPollResults)

