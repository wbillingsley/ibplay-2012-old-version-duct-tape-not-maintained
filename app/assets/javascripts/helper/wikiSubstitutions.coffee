# Event handler that performs a topic lookup when a link is clicked
#
linkLookup = (el) ->
    link = $(this)
    data = {
        topic: link.attr("data-ib-topic")
        noun: link.attr("data-ib-noun")
        adj: link.attr("data-ib-adj")
        site: link.attr("data-ib-site")
        entryId: link.attr("data-ib-entryid")
    }
    parent.IBViewer.lookupCE(data)

processLinks = () ->
    #
    $("a[data-ib-topic]").add("a[data-ib-entryid]").on("click", linkLookup)


# Inserts prettify.js and prettify.css if there are any elements with the .prettyprint class
conditionallyInsertPrettify = () ->
    if($(".prettyprint").length > 0)
        $("head").append("<link href='/assets/lib/prettify/prettify.css' type='text/css' rel='stylesheet' />")
        $("body").append("<script type='text/javascript' src='/assets/lib/prettify/prettify.js'></script>")
        $("body").append("<script>prettyPrint();</script>")


# Inserts mathjax if there are any elements with the .mathjax class
conditionallyInsertMathJax = () ->
    if($(".mathjax").length > 0)
        $("head").append('<script type="text/javascript"
                            src="http://cdn.mathjax.org/mathjax/latest/MathJax.js?config=TeX-AMS-MML_HTMLorMML">
                          </script>')

# Inserts Reveal.js if there are any elements with the .reveal class
conditionallyInsertReveal = () ->    
    if($(".reveal").length > 0)
        console.log("Inserting Reveal.js")
        $("head").append('<link rel="stylesheet" href="/assets/lib/reveal.js/css/reveal.css" />')
        $("head").append('<link rel="stylesheet" href="/assets/lib/reveal.js/css/theme/sky.css" />')
        $("body").append('<script src="/assets/lib/reveal.js/reveal.js"></script><script>Reveal.initialize()</script>')
        console.log("Inserted Reveal.js")
                          
                                   


processTemplates = () ->
    $("[data-ib-template]").each((index, element) ->
        jqEl = $(this)
        template = jqEl.attr("data-ib-template")
        if template == "youtube"
            insertYouTube(jqEl)
    )


# Finds all elements with a data-ib-replace attribute, and calls the registered method
# for that replacement 
#
processRegisteredReplacements = () -> 
  $("[data-ib-replace]").each((index, element) ->
    jqEl = $(this)
    replaceKey = jqEl.attr("data-ib-replace")
    replaceFunc = registeredReplacements[replaceKey]
    if replaceFunc?
      replaceFunc(this)
  )     
      
insertYouTube = (jqEl) ->
    videoId = jqEl.attr("data-ib-value")
    templ = '<iframe src="http://www.youtube.com/embed/THEKEY?rel=0" frameborder="0" allowfullscreen></iframe>'
    jqEl.html(templ.replace("THEKEY", videoId))


# Table of registered replacement functions to call
registeredReplacements = {} 

window.IBWikiSubstitutions =
    pushPrevNextAnchors: () ->
        $(".ib-prevnext[id]").each((idx, el) ->
            parent.IBPrevNext.push("anchor", $(el).attr("id"))
        )
        
    run: () -> 
      processLinks()
      conditionallyInsertPrettify()
      conditionallyInsertMathJax()
      conditionallyInsertReveal()
      processTemplates()
      processRegisteredReplacements()

    # Registers a replacement function that will be called for elements with the attribute
    # data-ib-replace='string' 
    registerReplacement: (string, func) -> registeredReplacements[string] = func
    
    registeredReplacements: () -> registeredReplacements
    
    
# This returns IBWikiSubstitutions as a require.js module
#
define([], () -> window.IBWikiSubstitutions)
    