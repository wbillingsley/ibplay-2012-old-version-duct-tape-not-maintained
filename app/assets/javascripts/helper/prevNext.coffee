# For keyboard Prev Next
#
# Holds a list of actions to perform before advancing the page.


class Action
    constructor: (@type, @reference, @data) ->

    perform: ->
        switch @type
            when "anchor" then IBViewer.currIFrame()[0].contentWindow.location.hash = "#" + @reference

    unperform: ->


window.IBPrevNext =
    anchorStack: []
    pos: 0

    reset: () ->
        @anchorStack = []
        @pos = 0
        try
            IBViewer.currIFrame()[0].contentWindow.IBWikiSubstitutions.pushPrevNextAnchors()
            IBViewer.currIFrame()[0].contentWindow.location.hash = "#"
        catch error
            # do nothing
        finally
            console.log("PrevNext reset anchorstack to: " + @anchorStack)
            console.log("PrevNext position is: " + @pos)


    push: (type, reference, data) ->
        @anchorStack.push(new Action(type, reference, data))

    next: () ->
        if (@pos < @anchorStack.length)
            @anchorStack[@pos].perform()
            @pos = @pos + 1
        else
            IBViewer.nextSlide()

    prev: () ->
        if (@pos > 0)
            @pos = @pos - 1
            if (@pos >= 0)
                @anchorStack[@pos].unperform()
            if (@pos > 0)
                @anchorStack[@pos - 1].perform()
            else
                IBViewer.currIFrame()[0].contentWindow.location.hash = "#"
        else
            IBViewer.prevSlide()



    keyhandler: (evt) ->
        evt = evt || window.event
        if evt.target == $("body")[0]
            switch evt.keyCode
                when 37 then IBPrevNext.prev()
                when 39 then IBPrevNext.next()


$(document).on("keydown", IBPrevNext.keyhandler)



# This returns IBPrevNext as a require.js module
#
define(() -> window.IBPrevNext)
