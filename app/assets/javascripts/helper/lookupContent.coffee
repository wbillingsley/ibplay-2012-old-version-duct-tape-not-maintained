

nounSelector = "#ceInfo .nouns"
adjSelector = "#ceInfo .adjs"
topicSelector = "#ceInfo .topics"

titleSelector = "#ceInfo .title"

scoreSelector = "#ce-score"
commentCountSelector = "#ce-comment-count"

slideSelector = "#main-content"

nextButtonSelector = "#next-button"
prevButtonSelector = "#prev-button"
presStartButtonSelector = "#pres-start-button"

editButtonSelector = "#edit-button"

slideSorterSelector = "#slide-sorter"
makePresentationSelector = "#make-presentation"
otherPresentationsSelector = "#other-presentations"
ssSlideListSelector = "#ss-slide-list"

currentSlideClass = "current-slide"
currentIFrameSelector = ".current-slide iframe"



viewing =

    presentation: null

    contentEntryIndex: null

    contentEntries: null

    seeAlso: null

    presSeeAlso: null

    lookupData: null

    findCE: (ce) ->
        ids = (c.id for c in viewing.contentEntries)
        ids.indexOf(ce.id)

hasNext = () ->
    viewing.contentEntryIndex + 1 < viewing.contentEntries?.length

hasPrev = () ->
    if viewing.presentation?
        viewing.contentEntryIndex >= 0
    else
        viewing.contentEntryIndex > 0

empty = () ->
    viewing.contentEntries.length <= 0 && !(viewing.presentation?)

# If we're looking at the presentation title page
isPresSlide = () ->
    (viewing.presentation? and viewing.contentEntryIndex == -1)


logCurrCe = () ->
    if empty()
        $.ajax({
            url: "/logView"
            type: "POST"
            data: {
                topics: [ viewing.lookupData?.topic ]
            }
        })
    else
        $.ajax({
            url: "/logView"
            type: "POST"
            data: {
                entry: currCe().id
            }
        })


updateView = () ->
    updateTagsFromContentEntry()
    updateSlidesFromContentEntry()
    updateNextPrevStart()
    updateSlideSorter()
    updatePermissions()
    updateEditControls()
    IBPrevNext.reset()
    resizeContentFrames()
    logCurrCe()


# The current content entry
currCe = () ->
    if empty()
        null
    else
        ce = if (isPresSlide())
            viewing.presentation
        else
            viewing.contentEntries[viewing.contentEntryIndex]

# The current content entry's iframe
currIFrame = () ->
    $(currentIFrameSelector)

# The user's permissions on the current content entry
currPermissions = () ->
    ce = currCe()
    if ce?
        ce.permissions
    else
        viewing.emptyPermissions

# Enables or disables elements based on permissions in viewing
updatePermissions = () ->
    perms = currPermissions()
    $(".require-chat").attr("disabled", !perms?.mayChat)
    $(".require-vote").attr("disabled", !perms?.mayVote)
    $(".require-add").attr("disabled", !perms?.mayAdd)
    $(".require-edit").attr("disabled", !perms?.mayEdit)
    $(".require-protect").attr("disabled", !perms?.mayProtect)
    if viewing.presentation?
        $(".require-pres-add").attr("disabled", !viewing.presentation.permissions?.mayAdd)
        $(".require-pres-edit").attr("disabled", !viewing.presentation.permissions?.mayEdit)
    console.log("Updated permissions. ")


# Updates the tags from the content entry's metadata
updateTagsFromContentEntry = () ->
    ce = if empty()
        {
            adjs: [ viewing.lookupData?.adj ]
            nouns: [ viewing.lookupData?.noun ]
            topics: [ viewing.lookupData?.topic ]
        }
    else
        if (isPresSlide())
            viewing.presentation
        else
            viewing.contentEntries[viewing.contentEntryIndex]

    sel = d3.select(adjSelector).selectAll(".adj").data(ce.adjs ? [], (d) -> d)
    sel.exit().remove()
    sel.enter().append("span").classed("adj", true).text((d) -> d)

    sel = d3.select(nounSelector).selectAll(".noun").data(ce.nouns ? [], (d) -> d)
    sel.exit().remove()
    sel.enter().append("span").classed("noun", true).text((d) -> d)

    sel = d3.select(topicSelector).selectAll(".topic").data(ce.topics ? [], (d) -> d)
    sel.exit().remove()
    sel.enter().append("button").classed("topic", true)
        .on("click", (d) -> IBViewer.listEntriesFor(d))
        .text((d) -> d)

    $(titleSelector).text(ce.name ? "(Untitled)")
    $(scoreSelector).text(ce.score ? 0)
    $(commentCountSelector).text(ce.commentCount ? 0}

    # Also update the content links
    $(".content-url-link").attr("href", ce.url ? "#")

updateSlidesFromContentEntry = () ->
    if empty()
        # This is a bit of a bizarre cheat to ensure the content panes all fit the same data form
        sel = d3.select(slideSelector).selectAll(".content-pane").data([ { id: "no-content" }], (d) -> d.id)
        sel.exit().remove()
        sel.enter().append("div").classed("content-pane", true)
            .append("div").classed("no-content-notice", true)
            .text("We don't have content for that. Would you like to add some?")
    else
        idx = 0
        ceArr = if (viewing.presentation?)
            idx = viewing.contentEntryIndex + 1
            [viewing.presentation].concat(viewing.contentEntries)
        else
            idx = viewing.contentEntryIndex
            viewing.contentEntries

        sliceStart = idx - 1
        if sliceStart < 0
            sliceStart = 0

        sliceEnd = idx + 2
        if sliceEnd > ceArr.length
            sliceEnd = ceArr.length

        sliced = ceArr[sliceStart...sliceEnd]
        slicedIdx = idx - sliceStart # The index into the sliced array

        sel = d3.select(slideSelector).selectAll(".content-pane").data(sliced, (d) -> d.id)
        sel.exit().remove()
        cp = sel.enter().append("div").classed("content-pane", true).each(createContentPane)
        sel.each((d,i) ->
            if (i == slicedIdx)
                $(this).addClass(currentSlideClass)
                $(this).show()
            else
                $(this).hide()
                $(this).removeClass(currentSlideClass)
        )

# Sets up JQuery-UI sortable (drag and drop reordering) on the slide sorter.  This will be enabled and disabled in
# updateSlideSorter
prepSlideSorting = () ->
    sortHandler =
        ibss_move_start: null
        ibss_move_end: null

        start: (event, ui) ->
            @ibss_move_start = ui.item.index();

        update: (event, ui) ->
            @ibss_move_end = ui.item.index();
            IBViewer.performSlideMove(@ibss_move_start, @ibss_move_end);

    $(ssSlideListSelector).sortable(sortHandler)


updateSlideSorter = () ->
    # Disable sorting in the slide list
    $(ssSlideListSelector).sortable("disable")

    if viewing.presentation?
        $(slideSorterSelector).show()
        $(makePresentationSelector).hide()

        slideList = d3.select(ssSlideListSelector).selectAll(".slide")
            .data(viewing.contentEntries, (d) -> d.id)

        slideList.exit().remove()
        slideList.enter().append("div").classed("slide", true).append("div")
            .each((d,i) ->
                sel = d3.select(this)

                if viewing.presentation.permissions?.mayEdit
                    # Add a select checkbox
                    sel.append("input").attr("type", "checkbox")
                        .classed("slide-select", true)
                        .classed("require-edit-pres", true)
                        .attr("value", i) # Value is the index into the contentEntries array

                sel.append("div").classed("slide-number", true).text(i + 1)
                sel.append("div").classed("name", true)
                    .text((d) -> d.name ? "(unnamed slide)")
                sel.append("div").classed("kind", true)
                    .text((d) -> d.kind ? "(unnamed slide)")
                sel.on("click", () -> IBViewer.goToSlide(i))
            )

        # Mark which is the current slide
        slideList.each((d,i) ->
            if (currCe() == d)
                $(this).addClass("active")
            else
                $(this).removeClass("active")
        )

        # Allow re-ordering of slides, if permitted
        if (viewing.presentation.permissions?.mayEdit ? false)
            # Enable sorting in the slide list
            $(ssSlideListSelector).sortable("enable")

    else
        $(slideSorterSelector).hide()
        $(makePresentationSelector).show()

# Creates a div for some content
createContentPane = (d, i) ->
    selection = d3.select(this)

    if (d.url?)
        iframe = selection.append("div").classed("content-frame", true).append("iframe")
            .attr("frameborder", 0)
            .attr("src", d.url)
    else
        if (d == viewing.presentation)
            # Make the slide that describes the presentation
            pane = selection.append("div").classed("content-frame", true).append("div")
            pres = pane.append("div")
                    .classed("hero-unit", true)
            pres.append("h1").text((d) -> d.name ? "Untitled presentation")





updateNextPrevStart = () ->
    if empty()
        $(nextButtonSelector).attr("disabled", true)
        $(prevButtonSelector).attr("disabled", true)
        $(presStartButtonSelector).attr("disabled", true)
    else
        $(nextButtonSelector).attr("disabled", not hasNext())
        $(prevButtonSelector).attr("disabled", not hasPrev())
        $(presStartButtonSelector).attr("disabled", not viewing.presentation?)


updateEditControls = () ->
    if empty()
        $(editButtonSelector).attr("disabled", true)
    else
        if (not currCe().editUrl?)
            $(editButtonSelector).attr("disabled", true)


# Accepts a standard response from the server, and updates the 'viewing' data
#
handleCePresSeeAlsoResponse = (data) ->
    if (data.presentation?)
        viewing.presentation = data.presentation
        viewing.contentEntries = data.presentation.entries
        if (data.recommended?)
            viewing.contentEntryIndex = viewing.findCE(data.recommended)
        else
            viewing.contentEntryIndex = -1
            viewing.emptyPermissions = data.nothing?.permissions
    else
        viewing.presentation = null
        if (data.recommended?)
            viewing.contentEntries = [data.recommended]
            viewing.contentEntryIndex = 0
        else
            viewing.contentEntries = []
            viewing.contentEntryIndex = -1
            viewing.emptyPermissions = data.nothing?.permissions
    viewing.seeAlso = data.seeAlso
    updateView()

onDataSuccess = (data, status, jxhr) ->
    handleCePresSeeAlsoResponse(data)

onDataError = (jxhr, status, error) ->
    console.log("error")
    console.log(error)


lookupCE = (data) ->
    viewing.lookupData = data
    $.ajax({
        url: "content"
        data: data
        type: "GET"
        success: onDataSuccess
        error: onDataError
    })


# Make a presentation using the current content entry
makePresentation = () ->
    ce = currCe()
    if (currCe?)
        $.ajax({
            url: "makePresentation"
            type: "post"
            data: {
                including: ce.id
            }
            success: onDataSuccess
            error: onDataError
        })

window.IBViewer =

    viewing: () -> viewing
    
    # Handler for JSON response packets from the server
    handleStandardJsonResponse: handleCePresSeeAlsoResponse
    
    # The currently viewed content entry
    currCe: currCe

    lookupCE: lookupCE

    goToPageOne: () ->
        lookupCE({topic: "Page One"})

    goToPresStart: () ->
        if viewing.presentation?
            viewing.contentEntryIndex = -1
            updateView()

    goToSlide: (i) ->
        if viewing.presentation?
            viewing.contentEntryIndex = i
            updateView()

    nextSlide: () ->
        if hasNext()
            viewing.contentEntryIndex += 1
            updateView()

    prevSlide: () ->
        if hasPrev()
            viewing.contentEntryIndex -= 1
            updateView()


    init: (ib_init) ->
        lookupCE(ib_init)

    makePresentation: makePresentation

    # Shows the confirmation form for removing entries from the presentation
    showRemovePresEntriesForm: () ->
        inputList = $("#remove-from-pres-item-list")
        # Reset the form
        inputList.html("")

        # Add input elements for each ticked slide
        $(".slide-select:checked").each((i, el) ->
            index = $(el).val()
            inp = $("<input type='hidden' name='index' />").val(index)
            inputList.append(inp)
            inp = $("<input type='hidden' name='entryId' />").val(viewing?.contentEntries[index]?.id)
            inputList.append(inp)
        )

        $("#remove-from-pres-modal").modal("show")

    # Submits the form to remove entries from the presesntation
    submitRemovePresEntriesForm: () ->
        # TODO: Fix absolute URL
        submitUrl = "/presentation/" + viewing.presentation?.pid + "/removeEntries"
        IBAddForms.performAdd("#remove-from-pres-form", submitUrl)

    # Show the modal dialog for uploading a zip of images
    showAppendToPres: () ->
        $("#append-to-pres-modal").modal("show")

    # Submits the form to remove entries from the presesntation
    submitAppendToPresForm: () ->
        # TODO: Fix absolute URL
        submitUrl = "/presentation/" + viewing.presentation?.pid + "/appendEntry"
        IBAddForms.performAdd("#append-to-pres-form", submitUrl)

    # Performs an AJAX post and reacts to the results for up/down-voting
    voteAction: (url, e) ->
      jQuery.ajax({
        url: url
        type: "POST"
        dataType: "json"
        success: (data) -> 
          e.score = data.score
          e.permissions.mayVote = false
          updatePermissions()
          updateTagsFromContentEntry()
      })

    # Upvote    
    upVote: () -> 
        e = currCe()
        if e?
          url = "/entry/" + e.id + "/upVote"
          @voteAction(url, e)
          
    # Downvote    
    downVote: () -> 
        e = currCe()
        if e?
          url = "/entry/" + e.id + "/downVote"
          @voteAction(url, e)
        
    showCommentsModal: () ->
        # Look up the entry
        window.CommentsViewer.view(currCe()?.id)

        # Show the modal
        $("#comments-modal").modal("show")
	


    # Shows the like entry form
    showDislikeEntryForm: () ->
        # Clear any previous comment
        $("#dislike-comment").val("")

        # Show the modal
        $("#dislike-modal").modal("show")


    # Submits the like entry form in the background
    submitLikeEntryForm: () ->
        # TODO: Fix absolute URL
        submitUrl = "/entry/" + currCe()?.id + "/likeOrDislike"
        data = new FormData($("#like-form").get()[0])
        client = new XMLHttpRequest()
        client.open("POST", submitUrl, false)
        client.send(data)

        # Hide all modals
        $(".modal").modal('hide')

    # Submits the dislike entry form in the background
    submitDislikeEntryForm: () ->
        # TODO: Fix absolute URL
        submitUrl = "/entry/" + currCe()?.id + "/likeOrDislike"
        data = new FormData($("#dislike-form").get()[0])
        client = new XMLHttpRequest()
        client.open("POST", submitUrl, false)
        client.send(data)

        # Hide all modals
        $(".modal").modal('hide')

    # Asks the server to re-order slides
    performSlideMove: (from, to) ->
        $.ajax({
            url: "movePresEntry"
            type: "POST"
            data: {
                ceIdx: viewing.contentEntryIndex
                presentation: viewing.presentation?.pid
                from: from,
                to: to
            }
            success: handleCePresSeeAlsoResponse

        })


    chat: () ->
        textbox = $("#chat-content")
        msg = textbox.val()
        textbox.val("")
        anon = $("#chat-anon:checked").val() ? false
        $.ajax({
            url: "chat"
            type: "POST"
            data: {
                anonymous: anon
                content: msg
                viewingCe: currCe()?.id
            }
        })

    updateView: updateView

    empty: empty

    editContent: () ->
        currIFrame().attr("src", currCe().editUrl)

    currIFrame: currIFrame

    currPermissions: currPermissions

    listEntriesFor: (topic) ->
        $("#list-entries-modal").addClass("querying").modal("show")
        $("#list-entries-topic").text(topic)
        $("#list-entries-div").html("")
        $.ajax({
            url: "entriesForTopic"
            data: {
                topic: topic
            }
            success: (data) ->
                nouns = []
                for entry in data.entries
                    for n in entry.nouns
                        if (nouns.indexOf(n) < 0)
                            nouns.push(n)
                nouns.sort()

                $("#list-entries-modal").removeClass("querying")
                e = d3.select("#list-entries-div").selectAll(".topic").data(nouns)
                entry = e.enter().append("div").classed("noun-set", true)
                entry.append("h4").text((d) -> d)
                entry.each((d, i) ->
                    sel = d3.select(this)
                    entries = data.entries.filter((e) -> e.nouns.indexOf(d) >= 0)
                    console.log(entries)
                    console.log(sel)
                    entryDiv = sel.selectAll(".entry").data(entries).enter().append("button").classed("entry", true)
                        .on("click", (d) ->
                            IBViewer.lookupCE({entry: d.id})
                            $("#list-entries-modal").removeClass("querying").modal("hide")
                        )
                        .append("div")
                    entryDiv.append("div").classed("title", true).text((d) -> d.name ? "(untitled)")
                    entryDiv.append("div").classed("note", true).text((d) -> d.note)
                    tags = entryDiv.append("div").classed("tags", true).selectAll(".tag").data((d) -> d.adjectives.concat(d.nouns))
                    tags.enter().append("span").classed("tag", true).text((d) -> d)
                    entryDiv.append("div").classed("refval", true).text((d) ->
                        if (d.kind == "URL")
                            d.refval
                        else
                            ""
                    )
                    entryDiv.append("div").classed("kind", true).text((d) -> d.kind)
                    entryDiv.append("div").classed("site", true).text((d) -> d.site)
                )
        })


window.IBPanes =

    toggleLeftPanel: () =>
        $("#left-panel").toggleClass("closed")
        $("#left-panel-control").toggleClass("closed")

    toggleRightPanel: () =>
        $("#main-content").toggleClass("right-open")
        $("#right-edge").toggleClass("right-open")
        $("#right-panel-holder").toggleClass("closed")


$(document).ready(() ->
    # Initialise the slide sorter
    prepSlideSorting()

    window.IBViewer.init(ib_init)
)

resizeContentFrames = () ->
    if((navigator.userAgent.match(/iPhone/i)) || (navigator.userAgent.match(/iPod/i)) || (navigator.userAgent.match(/iPad/i)))
       # iPad / iPhone / iPhone
       # These won't let you scroll an iframe, but auto-expand the iframe's height. The surrounding div can then supply the scrolling
       $(".current-slide .content-frame").css("overflow", "scroll").css("-webkit-overflow-scrolling", "touch").css("height", "100%")
    else
        h = $(window).height() - $("#main-content").offset().top - $("#ib-bottom-panel-holder").height() - 5
        try
            $(".current-slide iframe").height(h)
        catch ex
            console.log(ex)
        $("#right-panel").height(h)
        $("#middle-left").height(h)
        $("#slide-sorter").height(h)


$(window).resize(resizeContentFrames)

$(window).load(() ->
    console.log("Window has loaded.")
    resizeContentFrames()
)


# This returns IBViewer as a require.js module
#
define(() -> window.IBViewer)

