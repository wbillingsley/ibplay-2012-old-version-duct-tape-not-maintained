/*
 * Require.js set of modules for the content viewer.
 * (At the moment we're just including everything!)
 */
require([
    "markdown/ibMarkdown",
    "qna/qnalist"
], function(IBMarkdown, IBQnaList) {
	console.log("library has loaded")
		
	IBQnaList.updateQuestionsList()
	
	var converter1 = IBMarkdown.getSanitizingConverter()
    var editor1 = new Markdown.Editor(converter1);
	editor1.run()	
})