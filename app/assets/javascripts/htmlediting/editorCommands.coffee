repositionEditor = () ->
    h = $(".editControls").css("height")
    $("#editor").css("margin-top", h)

theEditor = null

hookEditor = (sel) ->
    theEditor = new Docstrap.Editor(sel)
    window.theEditor = theEditor

savedRange = null

window.EditWikiPage =

    # Show the insert web link form
    showWebLinkForm: () ->
        $("#docstrap-weblink-modal").modal("show")

    # Show the insert web link form
    showEntryLinkForm: () ->
        $("#docstrap-entrylink-modal").modal("show")

    # Show the insert web link form
    showTopicLookupForm: () ->
        $("#docstrap-topiclookup-modal").modal("show")

    # Show the insert youtube form
    showYouTubeForm: () ->
        savedRange = window.getSelection().getRangeAt(0)
        $("#youtube-id").val("")
        $("#docstrap-youtube-modal").modal("show")

    insertYouTube: () ->
        sel = window.getSelection()
        sel.removeAllRanges()
        sel.addRange(savedRange)
        val = $("#youtube-id").val()
        # TODO: Detect if the full YouTube URL has been pasted in and extract the ID
        videoId = val
        templ = '<div class="fillable-template youtube-med" data-ib-template="youtube" data-ib-value="THEKEY"><div class="placeholder">YouTube</div></div>'
        theEditor.insertHTML(templ.replace("THEKEY", videoId))
        # Hide all modals
        $(".modal").modal('hide')

    # Show the insert web link form
    showUploadImageForm: () ->
        $("#docstrap-upload-image-modal").modal("show")

    # Upload an image and insert it into the page
    uploadImage: () ->
        selector = "#docstrap-upload-image-form"
        url = "storeImage"
        data = new FormData($(selector).get()[0])
        client = new XMLHttpRequest()
        client.open("POST", url, false)
        client.send(data)
        # TODO handle errors
        mediaFileId = client.responseText
        templ = '<img src="/media/THEKEY" />'
        theEditor.insertHTML(templ.replace("THEKEY", mediaFileId))
        # Hide all modals
        $(".modal").modal('hide')

    # Show the insert web image form
    showWebImageForm: () ->
        $("#docstrap-web-image-modal").modal("show")

    # Insert an image from the web
    insertWebImage: () ->
        url = $("#docstrap-web-image-url").val()
        templ = '<img src="THEKEY" />'
        theEditor.insertHTML(templ.replace("THEKEY", url))
        # Hide all modals
        $(".modal").modal('hide')

    # Show the media browser
    showImageBrowser: () ->



    # Inserts an anchor that will be picked up by the arrow-key prev/next functionality
    insertNextAnchor: () ->
        num = "" + Math.random()
        tag = "tag" + num.substring(2)
        html = "<span id='" + tag + "' class='ib-prevnext'></span>"
        console.log(html)
        theEditor.insertHTML(html);

    insertTextNodeAfterSel: () ->
        theEditor.insertTextNodeAfterSel("&nbsp;")

    getThemeable: () ->
        theEditor.selectionAncestorMatching(".themeable").first()

    # Sets the background theme
    setThemeableBackground: (bgClass) ->
        @setThemeableMutexClass(bgClass, "^themeBg")

    # Sets the border theme
    setThemeableBorder: (borderClass) ->
        @setThemeableMutexClass(borderClass, "^themeBorder")

    # Sets the border theme
    setThemeableMargin: (marginClass) ->
        @setThemeableMutexClass(marginClass, "^themeMargin")

    # Sets the border theme
    setThemeableFontSet: (fontClass) ->
        @setThemeableMutexClass(fontClass, "^themeFontSet")

    # Sets the theme switch that can, for instance, change whether fonts are light or dark
    setThemeableInnerTheme: (innerClass) ->
        @setThemeableMutexClass(innerClass, "^themeInner")

    # Sets the theme switch that can, for instance, change whether fonts are light or dark
    setThemeableMutexClass: (className, mutexRegex) ->
        themeable = @getThemeable()
        classList = themeable[0].classList
        for clazz in classList
            if clazz? and clazz.search(mutexRegex) >= 0
                classList.remove(clazz)
        classList.add(className)


    # toggles a class on or off on a themeable
    toggleThemeableClass: (toggleClass) ->
        themeable = @getThemeable()
        classList = themeable[0].classList
        classList.toggle(toggleClass)




$(window).resize(() -> repositionEditor())
$(document).ready(() ->
	repositionEditor()
	$("#editTools").tab()
	hookEditor("#editor")
)


    
# This returns EditWikiPage as a require.js module
#
define(["./docstrap"], (Docstrap) -> window.EditWikiPage)
