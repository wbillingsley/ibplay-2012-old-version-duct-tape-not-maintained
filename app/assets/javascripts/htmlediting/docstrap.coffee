
class Editor
    constructor: (@selector) ->

    htmlModalSelector: "#docstrap-edit-html-modal"
    htmlTextareaSelector: "#docstrap-edit-html-textarea"

    #----- The basics -----#

    justifyLeft: () ->
        document.execCommand('justifyLeft', false, "")

    justifyCenter: () ->
        document.execCommand('justifyCenter', false, "")

    justifyRight: () ->
        document.execCommand('justifyRight', false, "")

    bold: () ->
        document.execCommand('bold', false, "")

    italic: () ->
        document.execCommand('italic', false, "")

    underline: () ->
        document.execCommand('underline', false, "")

    strikeThrough: () ->
        document.execCommand('strikeThrough', false, "")

    subscript: () ->
        document.execCommand('subscript', false, "")

    superscript: () ->
        document.execCommand('superscript', false, "")

    removeFormat: () ->
        document.execCommand('removeFormat', false, "")

    indent: () ->
        document.execCommand('indent', false, "")

    outdent: () ->
        document.execCommand('outdent', false, "")

    insertOrderedList: () ->
        document.execCommand('insertOrderedList', false, "")

    insertUnorderedList: () ->
        document.execCommand('insertUnorderedList', false, "")


    #---- show source ----#

    updateTextAreaSource: () ->
        val = $(@selector).html()
        $(@htmlTextareaSelector).val(val)

    updateContentEditableSource: () ->
        val = $(@htmlTextareaSelector).val()
        $(@selector).html(val)

    toggleMarkup: () ->
        el = $(@selector)
        if(el.hasClass("showMarkup"))
            el.removeClass("showMarkup")
        else
            el.addClass("showMarkup")

    #---- Formatting ----#

    formatBlock: (blockEl) ->
        document.execCommand('formatBlock', false, blockEl)

    insertHTML: (html) ->
        document.execCommand('insertHTML', false, html)

    insertMyTemplate: (el) ->
        html = $(el).find(".template").html().toString().trim()
        console.log(html)
        #this.pasteHtmlAtCaret(html)
        v = document.execCommand('insertHTML', false, html)
        console.log(v)


    pasteHtmlAtCaret: (html) ->
        if (window.getSelection)
            # IE9 and non-IE
            sel = window.getSelection();
            if (sel.getRangeAt && sel.rangeCount)
                range = sel.getRangeAt(0);
                range.deleteContents();

                # Range.createContextualFragment() would be useful here but is
                # non-standard and not supported in all browsers (IE9, for one)
                el = document.createElement("div");
                el.innerHTML = html;
                frag = document.createDocumentFragment()
                while ( (node = el.firstChild) )
                    lastNode = frag.appendChild(node);

                range.insertNode(frag);

                #Preserve the selection
                if (lastNode)
                    range = range.cloneRange();
                    range.setStartAfter(lastNode);
                    range.collapse(true);
                    sel.removeAllRanges();
                    sel.addRange(range);
        else if (document.selection && document.selection.type != "Control")
            #IE < 9
            document.selection.createRange().pasteHTML(html);

    # Get the ancestor of the selection that matches the jQuery filter
    selectionAncestorMatching: (filter) ->
        sel = window.getSelection()
        range = sel.getRangeAt(0)
        n = $(range.commonAncestorContainer)
        n.closest(filter)

    # Inserts a text node after the node containing the current selection.
    # This is to solve the problem that it's impossible to put the caret
    # in between <b>foo</b><i>bar</i> otherwise.
    insertTextNodeAfterSel: (text) ->
        sel = window.getSelection()
        range = sel.getRangeAt(0)

        # If we're not in an element, get the element parent
        if sc.nodeType != 1
            sc = sc.parentElement

        # Now get the parent to pad.
        p = sc.parentElement

        t = document.createTextElement(text)
        p.insertBefore(t, sc.nextSibling)

        range.setStartAfter(t)

Docstrap =
    Editor: Editor

window.Docstrap = Docstrap


    
# This returns Docstrap as a require.js module
#
define([], () -> window.Docstrap)