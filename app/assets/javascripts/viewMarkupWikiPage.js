require([
   "helper/eventRoom",
   "helper/chatStream",
   "helper/wikiSubstitutions",
   "helper/pollResults",
   "helper/textPollResults",
   "helper/entryComments",
   "markdown/ibMarkdown"
], function(IBEventRoom, IBChatStream, IBWikiSubstitutions, IBPollResults, IBTextPollResults, IBEntryComments, IBMarkdown) {
	console.log("library has loaded")
	
	var converter1 = IBMarkdown.getSanitizingConverter()

	var source = $("#md-src")
	var dest = $("body")

	var last = ""

	function convert() {
	  var s = source.val()
	  if (last != s) {
	    dest.html(converter1.makeHtml(s))
	    last = s
	  }
	}

	convert()
	IBWikiSubstitutions.run()
	
})