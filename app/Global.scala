import com.theintelligentbook.ibmodel.mongo.DAO
import com.theintelligentbook.ibmodel.Sanitizer
import com.wbillingsley.handy._
import play.api._

object Global extends GlobalSettings {

  val testDBname = "ib1"


  override def onStart(app: Application) {
    RefById.lookUpMethod = new RefById.LookUp {      
      def lookup[T](r:RefById[T, _]) = DAO.resolve(r)      
    }
    RefManyById.lookUpMethod = new RefManyById.LookUp {      
      def lookup[T](r:RefManyById[T, _]) = DAO.resolveMany(r)      
    }
    
    DAO.database = Global.testDBname
    DAO.initConn();

    /*
    * Set up AntiSamy HTML sanitizer
    */
    Sanitizer.setPolicyFromResource("/antiSamyPolicy.xml")

    /*
     * Set the default content for newly created books and pages
     */
    val source1 = io.Source.fromInputStream(getClass.getResourceAsStream("/defaultWikiPageContent.html"))
    controllers.ContentController.defaultWikiPageContent = source1.mkString
    source1.close()

    val source2 = io.Source.fromInputStream(getClass.getResourceAsStream("/defaultPageOneContent.html"))
    controllers.BookController.defaultPageOneContent = source2.mkString
    source2.close()
    
    val source3 = io.Source.fromInputStream(getClass.getResourceAsStream("/defaultMarkdownContent.md"))
    controllers.ContentController.defaultMarkupWikiPageContent = source3.mkString
    source2.close()
    

    preloadClasses
  }

  /*
  * In development mode, Play and sbt can sometimes end up being deadlocked by Salat
  * (contending to use the ClassLoader). To avoid this, we preload the classes Salat
  * will need.
  */
  def preloadClasses = {
    val classes = Seq(
      classOf[com.theintelligentbook.ibmodel.mongo.Book],
      classOf[com.theintelligentbook.ibmodel.mongo.Reader],
      classOf[com.theintelligentbook.ibmodel.mongo.ContentEntry],
      classOf[com.theintelligentbook.ibmodel.mongo.BookInvitation],
      classOf[com.theintelligentbook.ibmodel.mongo.ALogE],
      classOf[com.theintelligentbook.ibmodel.mongo.ChatComment],
      classOf[com.theintelligentbook.ibmodel.mongo.MediaFile],
      classOf[com.theintelligentbook.ibmodel.mongo.Poll],
      classOf[com.theintelligentbook.ibmodel.mongo.PollResponse],
      classOf[com.theintelligentbook.ibmodel.mongo.Presentation],
      classOf[com.theintelligentbook.ibmodel.mongo.WikiPage],
      classOf[com.theintelligentbook.ibmodel.mongo.WikiPageHistory],
      classOf[com.theintelligentbook.ibmodel.mongo.Registration],
      classOf[model.EventRoom]
    )

    for (clazz <- classes) {
      val c2 = this.getClass.getClassLoader.loadClass(clazz.getName)
      println("Preloaded " + c2.getName())
    }

  }
  
  
  override def onHandlerNotFound(request: play.api.mvc.RequestHeader) = {
      play.api.mvc.Results.NotFound(views.html.xErrorNotFound("Not found"));
  }  

}