package controllers

import play.api._
import play.api.libs.openid.OpenID
import play.api.data.Form
import play.api.data.Forms._
import libs.concurrent.{Thrown, Redeemed}
import play.api.mvc._
import play.api.mvc.Results.{BadRequest, Redirect, Ok, Async}
import play.api.libs.concurrent.Execution.Implicits._
import model.Identity
import com.theintelligentbook.ibmodel.ReaderModel

/**
 * Handles OpenIDLogin$ authentication flows
 */
object OpenIDLogin {

  def loginPost = Action { implicit request =>

    Form(single(
      "openid" -> nonEmptyText
    )).bindFromRequest.fold(
    error => {
      Logger.info("bad request " + error.toString)
      BadRequest(error.toString)
    },
    {
      case (openid) => Async {
        val futUrl = OpenID.redirectURL(openid, routes.OpenIDLogin.openIDCallback.absoluteURL())
        futUrl.map(Redirect(_))         
      } // TODO: Handle errors in OpenId
    })
  }

  def openIDCallback = Action { implicit request =>
    AsyncResult(
      OpenID.verifiedId.map { info =>
          val r = ReaderModel.findOrCreateByIdent("openId", "url", info.id)
          UserController.loggedIn(r)(request)
      } // TODO: Handle errors in OpenId
    )
  }

}
