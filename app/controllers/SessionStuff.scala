package controllers

import com.wbillingsley.handy._

import play.api.mvc._
import org.bson.types.ObjectId
import com.theintelligentbook.ibmodel.mongo.{DAO, Reader}

object SessionStuff {

  def withLoggedInReader(session:Session, rid:ObjectId):Session = {
    session + ("userid" -> rid.toString)
  }

  def withSessionKey(session:Session, key:String):Session = {
    session + ("sessionkey" -> key)
  }

  def sessionKey(session:Session) = {
    session.get("sessionkey")
  }

  def newSessionKey:String = (new ObjectId).toString

  def withLoggedInReader(session:Session, r:Ref[Reader]):Session = {
    r.getId match {
      case Some(rid) => withLoggedInReader(session, rid)
      case _ => session - "userid"
    }
  }

  def loggedInReader[AC](request:Request[AC]):Ref[Reader] = {
    loggedInReader(request.session)
  }

  def loggedInReader(session:Session):Ref[Reader] = {
    val optId = session.get("userid").map(new ObjectId(_))
    val r = Ref.fromOptionId(classOf[Reader], optId)
    r.fetch
  }

}
