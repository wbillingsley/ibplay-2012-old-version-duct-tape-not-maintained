package controllers

import play.api._
import play.api.mvc._
import play.api.mvc.Results._
import com.theintelligentbook.ibmodel._
import RefConversions._
import com.theintelligentbook.ibmodel.mongo._
import model.{IBJson, Identity}
import play.api.data._
import play.api.data.Forms._
import com.wbillingsley.handy._
import Ref._
import org.bson.types.ObjectId
import model.Util._


object BookController {
  

  /** The content of a freshly created book. Updated on startup. */
  var defaultPageOneContent = "(to be loaded)";
  
  def coverImageUrl(book:Book):String = {
    println(book.coverImageURL)
    val s = book.coverImageURL.getOrElse("")
    if (s.trim.isEmpty) {
      routes.Assets.at("images/book_cover1.png").toString
    } else s    
  }

  /**
   * HTML Form handler for creating a book; redirects to the book's cover page
   * @return redirects to the book's cover page
   */
  def createBook = WithEm {
    Action { implicit request =>

      val form = Form(
        Forms.tuple(
          "book.title" -> text,
          "book.shortDesc" -> text
        )
      )
      val (title, shortDesc) = form.bindFromRequest().get
      
      for (
          r <- optionally(SessionStuff.loggedInReader(request));
          appr = Approval(r);
          b <- BookModel.createBook(appr, title, shortDesc, defaultPageOneContent)
      ) yield {        
        views.html.viewBook(b, appr) 
      }
    }
  }

  /**
   * Shows the front page (details and admin links) of the book
   * @param bid the book ID
   * @return HTML
   */
  def bookFront(bid:String) = WithEm {
    Action { implicit request =>
      for (
          b <- RefById(classOf[Book], new ObjectId(bid));
          r <- optionally(SessionStuff.loggedInReader(request))
      ) yield {        
        views.html.viewBook(b, Approval(r)) 
      }
    }
  }

  /**
   * Shows the edit page for the book's details
   * @param bid the book ID
   * @return HTML
   */
  def editBook(bid:String) = WithEm { Action { implicit request =>      
      for (
          r <- optionally(SessionStuff.loggedInReader(request));
          b <- RefById(classOf[Book], bid);
          appr <- Approval(r) ask SecurityModel.EditBook(b.itself)          
      ) yield views.html.editBook(b.itself)
  }}

  /**
   * Shows the edit page for the book's details
   * @param bid the book ID
   * @return HTML
   */
  def manageInvitesView(bid:String) = WithEm {
    Action { implicit request =>
      for (
          r <- optionally(SessionStuff.loggedInReader(request));
          b <- RefById(classOf[Book], bid);
          appr <- Approval(r) ask SecurityModel.Invite(b.itself)          
      ) yield views.html.viewBookInvites(b.itself)      
    }
  }
  
  /**
   * The view of a book that automatically lists various kinds of content
   */
  def viewBookContent(bid:String) = WithEm { Action { implicit request => 
      for (
          r <- optionally(SessionStuff.loggedInReader(request));
          b <- RefById(classOf[Book], bid);
          appr = Approval(r);
          a <- appr ask SecurityModel.Read(b.itself)          
      ) yield views.html.viewBookContent(b.itself, appr)      
  }}

  /**
   * The view of a book that automatically lists various kinds of content
   */
  def viewBookIndex(bid:String) = WithEm { Action { implicit request => 
      for (
          r <- optionally(SessionStuff.loggedInReader(request));
          b <- RefById(classOf[Book], bid);
          appr = Approval(r);
          a <- appr ask SecurityModel.Read(b.itself)          
      ) yield views.html.viewBookIndex(b, appr)      
  }}

  /**
   * The view of a book that automatically lists various kinds of content
   */
  def viewEventsChart(bid:String) = WithEm { Action { implicit request => 
      for (
          r <- optionally(SessionStuff.loggedInReader(request));
          b <- RefById(classOf[Book], bid);
          appr = Approval(r);
          a <- appr ask SecurityModel.ViewStats(b.itself)          
      ) yield views.html.viewBookEventsChart(b.itself, appr)  
  }}
  
  /**
   * The view of a book that automatically lists various kinds of content
   */
  def viewChatChart(bid:String) = WithEm { Action { implicit request => 
      for (
          r <- optionally(SessionStuff.loggedInReader(request));
          b <- RefById(classOf[Book], bid);
          appr = Approval(r);
          a <- appr ask SecurityModel.ViewStats(b.itself)          
      ) yield views.html.viewBookChatChart(b.itself, appr)  
  }}
  

  def handleCreateInvite(bid:String) = WithEm { Action { implicit request =>

    val params = request.body.asFormUrlEncoded.get

    val b = RefById(classOf[Book], bid)
    val r = SessionStuff.loggedInReader(request)

    val code = fetchStr(params, "code")
    val roles = params.getOrElse("role", Seq.empty[String]).map(BookRole.valueOf(_)).toSet
    val limitedNumber = params.get("limitedNumber").flatMap(_.headOption) match {
      case Some("true") => true
      case Some("on") => true
      case _ => false
    }
    val number = params.get("number").flatMap(_.headOption).map(_.toInt)

    for (
      result <- BookModel.createInvite(b, r, code, roles, limitedNumber, number.getOrElse(1))   
    ) yield Redirect(routes.BookController.manageInvitesView(bid))
  }}

  /**
   * Form handler for the edit book form
   * @param bid the book ID
   * @return redirects to the book's cover page
   */
  def handleEditBook(bid:String) = WithEm { Action { implicit request =>

    val b = RefById(classOf[Book], bid)
    val r = SessionStuff.loggedInReader(request)
    val tok:Approval[Reader] = r

    val formData = request.body.asMultipartFormData.get
    val mapping = formData.asFormUrlEncoded

    // If a cover image file was set, store it and remember the URL
    val imageFile = formData.file("coverImageFile")
    val uploadedUrl = imageFile flatMap { picture =>
      import java.io.FileInputStream
      val filename = picture.filename
      val contentType = picture.contentType

      val is = new FileInputStream(picture.ref.file)
      val mf = try {
        MediaModel.storeImage(
          book=b, permTok=tok,
          mimeType=contentType, name=Some(filename), shortDescription=Some("Uploaded cover image"),
          public=true, inputStream = is
        )
      } finally {
        is.close()
      }
      mf.toOption.map(f => routes.MediaFileController.getImage(f.id.toString).absoluteURL())
    }

    var listNouns = Seq.empty[String]
    var listNounTitles = Seq.empty[String]
    val result = b flatMap { book =>
      
      /*
       * Default all booleans to false, because that's how checkboxes in forms work
       * (Set to true if checked, absent from the form data if unchecked)
       */
      book.listed = false
      
      for ((key, optVal) <- mapping; v <- optVal) {
        key match {
          case "title" => book.title = Some(v)
          case "shortName" => book.shortName = Some(v)
          case "shortDescription" => book.shortDescription = Some(v)
          case "longDescription" => book.longDescription = Some(v)
          case "signupPolicy" => book.signupPolicy = BookSignupPolicy.valueOf(v)
          case "chatPolicy" => book.chatPolicy = BookChatPolicy.valueOf(v)
          case "coverImageURL" => book.coverImageURL = Some(v)
          case "listnoun" => { listNouns = listNouns :+ v }
          case "listnountitle" => { listNounTitles = listNounTitles :+ v }
          case "listed" => { book.listed = parseBool(v) }
        }
      }
      
      // Handle booleans as the way checkboxes works means we have to set these false by default
      book.listed = fetchBool(mapping, "listed").getOrElse(false)
            
      val zipped = listNounTitles.zip(listNouns).map{case (title, tag) => TitleAndTag(title=title, tag=tag)}
      book.listNouns = zipped
      
      // Overwrite the cover image with the uploaded image URL if there was one
      uploadedUrl.foreach(url => book.coverImageURL = Some(url))

      // Try to save the changes
      BookModel.saveBook(book.itself, tok)
    }

    for (r <- result) yield Redirect(routes.BookController.bookFront(bid))
  }}

  /**
   * Form handler for using a book invitation code
   * @param bid
   * @return
   */
  def handleUseInvite(bid:String) = WithEm { Action { implicit request =>
    val form = Form(
      Forms.single(
        "code" -> optional(text)
      )
    )

    val code = form.bindFromRequest().get
    
    refResultToResult(
	    for (
	        book <- RefById(classOf[Book], bid);
	        r = SessionStuff.loggedInReader(request);
	        result <- code match {
		      case Some(c) => SecurityModel.useInviteByBookAndCode(r, book.itself, c)
		      case _ => RefFailed(new IllegalStateException("You are not logged in, so shouldn't be able to submit an invite code"))
		    }
	    ) yield {
	    	Redirect(routes.BookController.bookFront(bid))
	    }
    )
  }}


}
