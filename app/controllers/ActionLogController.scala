package controllers


import play.api._
import play.api.mvc._
import play.api.mvc.Results._

import com.theintelligentbook.ibmodel._
import RefConversions._
import com.theintelligentbook.ibmodel.mongo._
import model.{IBJson, Identity}

import play.api.data._
import play.api.data.Forms._
import com.wbillingsley.handy._
import org.bson.types.ObjectId


/**
 * Created with IntelliJ IDEA.
 * User: wbillingsley
 * Date: 24/07/12
 * Time: 12:36 AM
 * To change this template use File | Settings | File Templates.
 */

object ActionLogController {

  /**
   * Gets a trimmed Option[String] value from a map of parameters
   * @param map
   * @param key
   * @return
   */
  private def fetchStr(map:Map[String,Seq[String]], key:String) = {
    val v = map.getOrElse(key, Seq.empty[String]).headOption
    v.flatMap{str =>
      val trim = str.trim()
      if (trim.isEmpty()) { None } else { Some(trim) }
    }
  }

  /**
   * Gets a Set value from a map of parameters
   * @param map
   * @param key
   * @return
   */
  private def fetchOptSet(map:Map[String,Seq[String]], key:String) = {
    map.get(key).map(_.toSet)
  }


  def logView = WithEm { Action { implicit request =>

    val params = request.body.asFormUrlEncoded.get

    val ceid = fetchStr(params, "entry")
    val topics = fetchOptSet(params, "topics[]")

    val ce = Ref.fromOptionId(classOf[ContentEntry], ceid).fetch
    val b = ce.flatMap(_.book)
    val r = SessionStuff.loggedInReader(request)
    val optS = SessionStuff.sessionKey(request.session)
    val s = optS.getOrElse(SessionStuff.newSessionKey)
    
    ActionLogModel.logAction(ce, b, r, Some(s), topics=topics)
    if (optS.isEmpty) {
      Ok("").withSession(SessionStuff.withSessionKey(request.session, s))
    } else {
      Ok("")
    }
  }}

  /**
   * Re-aggregates events for the book. This is temporary -- when we know what
   * formats we need, we'll update dynamically as events come in
   * @return
   */
  def updateAggregatedEvents(bid:String) = WithEm { Action { implicit request =>    
      val b = RefById(classOf[Book], bid)
      val r = SessionStuff.loggedInReader(request)

      ActionLogModel.updateAggregatedEvents(b, r)
      Redirect(routes.BookController.viewEventsChart(bid))
  }}
  
  /**
   * Fetches aggregates of events per hour
   */
  def aggregatedEvents(bid:String) = WithEm { Action { implicit request =>    
      val b = RefById(classOf[Book], bid)
      val r = SessionStuff.loggedInReader(request)

      val s = ActionLogModel.aggregatedEvents(b, r).fetch.toSeq
      
      if (s.isEmpty) {
        Ok("{ \"aggEvents\": [] }")
      } else {
    	  Ok("{ \"aggEvents\": [" +
          s.reduce(_ + ',' + _)
          + "] }")  
      }
      
  }}
  
  def updateAggregatedChats(bid:String) = WithEm { Action { implicit request =>    
      val b = RefById(classOf[Book], bid)
      val r = SessionStuff.loggedInReader(request)

      ChatModel.updateAggregatedChats(b, r)
      Redirect(routes.BookController.viewChatChart(bid))
  }}
  
  def aggregatedChats(bid:String) = WithEm { Action { implicit request =>    
      val b = RefById(classOf[Book], bid)
      val r = SessionStuff.loggedInReader(request)

      val s = ChatModel.aggregatedChats(b, r).fetch.toSeq
      
      if (s.isEmpty) {
        Ok("{ \"aggEvents\": [] }")
      } else {
    	  Ok("{ \"aggEvents\": [" +
          s.reduce(_ + ',' + _)
          + "] }")  
      }
      
  }}
}
