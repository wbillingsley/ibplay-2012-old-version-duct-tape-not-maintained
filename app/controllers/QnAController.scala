package controllers

import play.api.mvc.{ResponseHeader, ChunkedResult, Action}
import play.api.mvc.Results._
import com.wbillingsley.handy._
import Ref._
import com.theintelligentbook.ibmodel._
import com.theintelligentbook.ibmodel.mongo._
import model.Util._
import com.theintelligentbook.ibmodel.mongo.QnAQuestionDAO
import model.IBJson
import play.api.libs.json._


case class QnAPerms(vote:Boolean, answer:Boolean, comment:Boolean)


object QnAController {
  
  /**
   * Show the HTML view listing questions for the book
   */
  def viewListQuestions(bid:String, skip:Option[Int] = None) = WithEm { Action { implicit request => 
      for (
          book <- RefById(classOf[Book], bid);
          reader <- optionally(SessionStuff.loggedInReader(request));
          tok = Approval(reader);
          approved <- tok ask SecurityModel.Read(book.itself);
          questions = QnAQuestionDAO.findNewest(book.itself, skip=skip).fetch.toSeq
      ) yield {
        views.html.viewNewestQnA(book, tok, questions)
      }      
    
  }}
  
  /**
   * Views a Q&A Question
   */
  def viewQuestion(qid:String) = WithEm { Action { implicit request => 
      for (
          q <- RefById(classOf[QnAQuestion], qid);
          reader <- optionally(SessionStuff.loggedInReader(request));
          tok = Approval(reader);
          b <- q.book;
          approved <- tok ask SecurityModel.Read(q.book)
      ) yield {
        QnAQuestionDAO.incrementViewed(q.itself)
        views.html.viewQnAQuestion(b, tok, q)
      }      
    
  }}
  
  /**
   * JSON API for retrieving a list of comments
   */
  def qnaQuestions(bookId:String, mode:String, skip:Int = 0) = WithEm { Action { implicit request => 
    val resp = for (
      reader <- optionally(SessionStuff.loggedInReader(request));    
      tok = Approval(reader);
      book = RefById(classOf[Book], bookId);
      approved <- tok ask SecurityModel.Read(book);
      questions = QnAQuestionDAO.findNewest(book, Some(skip));
      json <- IBJson.manyQnaQuestionToJson(questions, tok)
    ) yield {      
      Ok(Json.obj("questions" -> json.toSeq))
    }
    resp
  }}

  /**
   * JSON API for a single question
   */
  def questionJson(qId:String) = WithEm { Action { implicit request => 
    val resp = for (
      reader <- optionally(SessionStuff.loggedInReader(request));    
      tok = Approval(reader);
      q <- RefById(classOf[QnAQuestion], qId);
      approved <- tok ask SecurityModel.Read(q.book);      
      json <- IBJson.qnaQuestionToFullJson(q.itself, tok)
    ) yield {      
      Ok(json)
    }
    resp    
  }}
  
  
  /**
   * Handle submission of the form to add a question to the book
   */
  def handleNewQuestion(bid:String) = WithEm { Action { implicit request =>

      val mapping = request.body.asMultipartFormData.get.asFormUrlEncoded
      val b = RefById(classOf[Book], bid)
      
      val resp = for (
        book <- b;
        reader <- optionally(SessionStuff.loggedInReader(request));
        tok = Approval(reader);
        approved <- tok ask SecurityModel.Read(book.itself);
        session = SessionStuff.sessionKey(request.session);
        title <- fetchStr(mapping, "title");
        body <- fetchStr(mapping, "body")        
      ) yield {
        val q = QnAQuestionDAO.newQuestion(book.itself, reader, session, title, body)
        println("And q is " + q)
        QnAQuestionDAO.save(q)
        Redirect(routes.QnAController.viewQuestion(q.id.toString))        
      }      
      resp
  }}

  /**
   * Handle submission of the form to add an answer
   */
  def handleAnswerQuestion(qid:String) = WithEm { Action { implicit request =>
      val mapping = request.body.asMultipartFormData.get.asFormUrlEncoded
      val q = RefById(classOf[QnAQuestion], qid)
      
      val resp = for (
        question <- q;
        reader <- optionally(SessionStuff.loggedInReader(request));
        tok = Approval(reader);
        approved <- tok ask SecurityModel.Read(question.book);
        session = SessionStuff.sessionKey(request.session);
        body <- fetchStr(mapping, "body")        
      ) yield {
        val q = QnAQuestionDAO.addAnswer(question.itself, reader, session, body)
        Redirect(routes.QnAController.viewQuestion(qid))        
      }      
      resp
  }}
  
  /**
   * Handle submission of the form to add an answer
   */
  def handleAddQuestionComment(qid:String) = WithEm { Action { implicit request =>
      val mapping = request.body.asMultipartFormData.get.asFormUrlEncoded
      val q = RefById(classOf[QnAQuestion], qid)
      
      val resp = for (
        question <- q;
        reader <- optionally(SessionStuff.loggedInReader(request));
        tok = Approval(reader);
        approved <- tok ask SecurityModel.Read(question.book);
        body <- fetchStr(mapping, "body")        
      ) yield {
        val q = QnAQuestionDAO.addQComment(question.itself, reader, body)
        Redirect(routes.QnAController.viewQuestion(qid))        
      }      
      resp
  }}  
  
  /**
   * Handle submission of the form to add an answer
   */
  def handleAddAnswerComment(qid:String) = WithEm { Action { implicit request =>
      val mapping = request.body.asMultipartFormData.get.asFormUrlEncoded
      val q = RefById(classOf[QnAQuestion], qid)
      
      val resp = for (
        question <- q;
        reader <- optionally(SessionStuff.loggedInReader(request));
        tok = Approval(reader);
        approved <- tok ask SecurityModel.Read(question.book);
        answer = Ref.fromOptionId(classOf[QnAAnswer], fetchStr(mapping, "answer"));
        body <- fetchStr(mapping, "body")        
      ) yield {
        val q = QnAQuestionDAO.addAnsComment(question.itself, answer, reader, body)
        Redirect(routes.QnAController.viewQuestion(qid))        
      }      
      resp
  }}    
  
}