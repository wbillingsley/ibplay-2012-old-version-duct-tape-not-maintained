package controllers

import play.api._
import libs.iteratee.Enumerator
import mvc._
import play.api.mvc.Results._

import com.theintelligentbook.ibmodel._
import RefConversions._
import com.theintelligentbook.ibmodel.mongo._
import model.{IBJson, Identity}

import RefConversions._

import play.api.data._
import play.api.data.Forms._
import com.wbillingsley.handy._
import org.bson.types.ObjectId
import scala.collection.mutable
import com.wbillingsley.handy.RefById
import com.wbillingsley.handy.RefItself
import scala.Some
import com.wbillingsley.handy.RefFailed


/**
 * Created with IntelliJ IDEA.
 * User: wbillingsley
 * Date: 19/06/12
 * Time: 10:23 PM
 * To change this template use File | Settings | File Templates.
 */

object MediaFileController {


  def storeImage(bookId:String) = WithEm { Action(BodyParsers.parse.multipartFormData) { implicit request =>

    val b = RefById(classOf[Book], new ObjectId(bookId)).fetch
    val r = SessionStuff.loggedInReader(request)

    val dataParts = request.body.dataParts
    val shortDesc = dataParts.get("shortDescription").flatMap(_.headOption)

    request.body.file("picture").map { picture =>
      import java.io.FileInputStream
      val filename = picture.filename
      val contentType = picture.contentType

      val is = new FileInputStream(picture.ref.file)
      val mf = try {
        MediaModel.storeImage(
          book=b, permTok=r,
          mimeType=contentType, name=Some(filename), shortDescription=shortDesc,
          inputStream = is
        )
      } finally {
        is.close()
      }

      mf match {
        case RefItself(m) => Created(m.id.toString)
        case RefNone => NotModified
        case RefFailed(exc) => InternalServerError(exc.getMessage)
      }
    }.getOrElse {
      NotModified
    }
  }}

  def imageUploadForm(id:String) = WithEm(
    Action { implicit request =>
      Ok(views.html.uploadImage())
    }
  )

  def getImage(mediaId:String) = WithEm { Action { implicit request =>
    val m = RefById(classOf[MediaFile], new ObjectId(mediaId)).fetch
    val r = SessionStuff.loggedInReader(request)

    MediaModel.getImage(m, r) match {
      case RefItself(m) => {

        MediaModel.getStream(m) match {
          case Some(is) => {
            Ok.stream(Enumerator.fromStream(is)).withHeaders(
              "Content-Type" -> m.mimeType.getOrElse(""),
              "Expires" -> "Wed Jul 14 11:26:47 GMT 2100" // TODO: Move this into a constant
            )
          }
          case None => NoContent
        }

      }
      case RefNone => NotFound
      case RefFailed(exc) => InternalServerError(exc.getMessage)
    }

  }}

}