package controllers

import play.api._
import play.api.mvc._
import play.api.mvc.Results._

import com.theintelligentbook.ibmodel._
import RefConversions._
import com.theintelligentbook.ibmodel.mongo._
import model.{IBJson, Identity}

import model.Util._
import com.wbillingsley.handy._
import org.bson.types.ObjectId


/**
 * Created with IntelliJ IDEA.
 * User: wbillingsley
 * Date: 23/07/12
 * Time: 11:17 PM
 * To change this template use File | Settings | File Templates.
 */

object ReaderController {

  def avatarUrl(reader:Reader):String = {
    routes.Assets.at("images/avatar.png").toString
  }
  
  
  /**
   * Shows the front page (details and admin links) of the book
   * @return HTML
   */
  def viewSelf = WithEm {
    Action { implicit request =>
      val r = SessionStuff.loggedInReader(request)      
      r.map{views.html.viewSelf(_)} orIfNone RefFailed(Refused("You are not logged in"))
    }
  }

  /**
   * Shows the edit page for the book's details
   * @return HTML
   */
  def editSelf = WithEm {
    Action { implicit request =>
      val r = SessionStuff.loggedInReader(request)
      r.map(views.html.editSelf(_)) orIfNone RefFailed(Refused("You are not logged in"))
    }
  }


  def handleEditSelf = WithEm { Action { implicit request =>

    val mapping = request.body.asFormUrlEncoded.get
    val r = SessionStuff.loggedInReader(request)
    (for (reader <- r) yield {
        reader.nickname = fetchStr(mapping, "nickname")
        reader.fullname = fetchStr(mapping, "fullname")
        reader.email = fetchStr(mapping, "email")
        ReaderDAO.save(reader)
        val rr = Redirect(routes.ReaderController.viewSelf())
        rr
    }) orIfNone RefFailed(Refused("You are not logged in"))
  }}


}
