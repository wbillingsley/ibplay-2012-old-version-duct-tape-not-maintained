package controllers

import play.api._
import play.api.mvc._
import com.theintelligentbook.ibmodel.mongo._

import com.wbillingsley.handy._

import play.api.Logger

object Application extends Controller {

  val logger = Logger(this.getClass)
  
  def login = Action {
    Ok(views.html.login())
  }

  def index = WithEm(
    Action { implicit request =>

      val r = SessionStuff.loggedInReader(request)
      val tok:Approval[Reader] = new Approval(r)
      
      Ok(views.html.index(tok))
    }
  )

  def about = Action { implicit request =>
      Ok(views.html.welcome())
  }
  
  
  import model.Util._
  
  /**
   * Test page for 404 errors
   */
  def notFound = Action { implicit request =>     
    val r:Ref[play.api.templates.Html] = RefNone
  	r
  }
  
  /**
   * Test page for Forbidden errors
   */
  def forbidden = Action { implicit request =>     
    val r:Ref[play.api.templates.Html] = RefFailed(Refused("This is a test page that always refuses permission"))
  	r
  }

  /**
   * Test page for errors
   */
  def error = Action { implicit request =>     
    val r:Ref[play.api.templates.Html] = RefFailed(new IllegalStateException("This is a test page that always gives this error"))
  	r
  }

}