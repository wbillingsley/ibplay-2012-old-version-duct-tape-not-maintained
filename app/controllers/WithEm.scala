package controllers

import play.api.mvc.{Result, Request, Action}
import com.theintelligentbook.ibmodel.mongo.DAO


case class WithEm[A](val action:Action[A]) extends Action[A] {

  def apply(request: Request[A]): Result = {
    DAO.withTransaction(action(request))
  }

  lazy val parser = action.parser
}
