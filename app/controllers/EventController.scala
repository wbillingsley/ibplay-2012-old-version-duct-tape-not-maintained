package controllers

import play.api._
import mvc.{ResponseHeader, Action, SimpleResult, ChunkedResult, WebSocket}
import http.{HeaderNames, Status}
import libs.json._
import libs.iteratee.{Enumerator, Iteratee, Enumeratee}
import play.api.libs.concurrent.Execution.Implicits._
import mvc.{Action, SimpleResult, Controller}
import play.api.mvc.Results._

import com.theintelligentbook.ibmodel._
import com.theintelligentbook.ibmodel.mongo._
import model.{EventRoom, ChatEvents, PollEvents, TextPollEvents, IBJson, Identity, Util}
import Util._

import play.api.data._
import play.api.data.Forms._
import com.wbillingsley.handy._
import Ref._
import org.bson.types.ObjectId
import scala.collection.mutable

import play.api.libs.json._
import com.wbillingsley.handy.RefById
import templates.Html


object EventController extends Controller {

    
  def processSubscriptions(listenerName:String, tok:Approval[Reader], s:String) {
    val j = Json.parse(s)
    val typ:Option[String] = (j \ "type").asOpt[String]
    
    println("Subscription: " + s)
    
    typ match {
      case Some("chatStream") => {
        val bid:Option[String] = (j \ "id").asOpt[String]
        for (approved <- tok ask SecurityModel.Read(Ref.fromOptionId(classOf[Book], bid)); b <- bid) {
          EventRoom.default ! EventRoom.Subscribe(listenerName, ChatEvents.ChatStream(b))
        }
      }
      case Some("pollResults") => {
        val pid:Option[String] = (j \ "id").asOpt[String]
        for (approved <- tok ask SecurityModel.ViewPollResults(Ref.fromOptionId(classOf[Poll], pid)); p <- pid) {
          EventRoom.default ! EventRoom.Subscribe(listenerName, PollEvents.PollStream(p))
        }
      }
      case Some("textPollResults") => {
        val pid:Option[String] = (j \ "id").asOpt[String]
        for (approved <- tok ask SecurityModel.ViewTextPollResults(Ref.fromOptionId(classOf[TextPoll], pid)); p <- pid) {
          EventRoom.default ! EventRoom.Subscribe(listenerName, TextPollEvents.TPollStream(p))
        }        
      }
      case _ => {
        
      }
    }
  }

  def subscribe(bookId:String) = Action { implicit request =>
    
    import play.api.data._
    import Forms._
    
    val subForm = Form(tuple("listenerName" -> text, "subscription" -> text)).bindFromRequest
    val (listenerName, subscription) = subForm.get
    val who = SessionStuff.loggedInReader(request)
    processSubscriptions(listenerName, Approval(who), subscription)
    Ok("")    
  }
  
  
  def websocketEvents(bookId:String, subscriptions:Option[String] = None) = WebSocket.async[JsValue] { implicit request =>
  
    import scala.language.reflectiveCalls
    
    // We should already have a session key, but if not, let's generate a throwaway one
    val s = SessionStuff.sessionKey(request.session).getOrElse((new ObjectId()).toString) 
    
    val jsIteratee = Iteratee.foreach[JsValue] { j => }
    
    val refR = (for (
      book <- RefById(classOf[Book], new ObjectId(bookId));
      r <- optionally(SessionStuff.loggedInReader(request.session));
      tok = Approval(r);
      approved <- tok ask SecurityModel.Read(book.itself) 
    ) yield {
      val name = (new ObjectId()).toString      
      val f = EventRoom.join(name, r, s, book.id.toString)
      val rf = new RefFuture(f)  
      
      // Perform any required subscriptions
      for (s <- subscriptions) processSubscriptions(name, tok, s)
      
      rf
    }).flatten
	
    val p = scala.concurrent.promise[(Iteratee[JsValue, Unit], Enumerator[JsValue])]
    refR.onComplete(
      onSuccess = { e =>
        p.success(jsIteratee, e)
      },
      onNone = {
        p.failure(new IllegalArgumentException("Not found"))
      },
      onFail = { f =>
       p.failure(_) 
      }
    )
    p.future
    
  }      
  
  def serverSentEvents(bookId:String, subscriptions:Option[String] = None) = Action { implicit request =>

    import scala.language.reflectiveCalls
    
    val toEventSource = Enumeratee.map[JsValue]{ msg =>
      val d = "data: "+ msg.toString +"\n\n"
      //val m = "" + d.length + "\n" + d
      d
    }
    
    // We should already have a session key, but if not, let's generate a throwaway one
    val s = SessionStuff.sessionKey(request.session).getOrElse((new ObjectId()).toString) 
    
    refResultToResult(
	    for (
	      book <- RefById(classOf[Book], new ObjectId(bookId));
	      r <- optionally(SessionStuff.loggedInReader(request));
	      tok = Approval(r);
	      approved <- tok ask SecurityModel.Read(book.itself) 
	    ) yield {
	      val name = (new ObjectId()).toString
	      val promise = EventRoom.join(name, r, s, book.id.toString)
	      Async {
	        promise.map(enumerator => {
	          /*
	           We pad the enumerator to send some initial data so that iOS will act upon the Connection: Close header
	           to ensure it does not pipeline other requests behind this one.  See HTTP Pipelining, Head Of Line blocking
	            */
	          val paddedEnumerator = Enumerator[JsValue](Json.toJson(Map("type" -> Json.toJson("ignore")))).andThen(enumerator)
	          val eventSource = paddedEnumerator &> toEventSource	          	         
	
	          // Apply the iteratee
	          val result = ChunkedResult[String](
	            header = ResponseHeader(play.api.http.Status.OK, Map(
	              HeaderNames.CONNECTION -> "close",
	              HeaderNames.CONTENT_LENGTH -> "-1",
	              HeaderNames.CONTENT_TYPE -> "text/event-stream"
	            )),
	            chunks = {iteratee:Iteratee[String, Unit] => eventSource.apply(iteratee) });
	          
	          // Perform any required subscriptions
	          for (s <- subscriptions) processSubscriptions(name, tok, s)
	          
	          result
	        })
	      }
	    }
	)
  }  
  
  def pollForEvents(bookId:String, subscriptions:Option[String] = None) = Action { implicit request =>

    import scala.language.reflectiveCalls  
    
    // We should already have a session key, but if not, let's generate a throwaway one
    val s = SessionStuff.sessionKey(request.session).getOrElse((new ObjectId()).toString) 
    
    var sep = ""
    val toStringSource = Enumeratee.map[JsValue]{ msg => val s = sep + msg.toString; sep = ","; s }
    
    refResultToResult(
	    for (
	      book <- RefById(classOf[Book], new ObjectId(bookId));
	      r <- optionally(SessionStuff.loggedInReader(request));
	      tok = Approval(r);
	      approved <- tok ask SecurityModel.Read(book.itself) 
	    ) yield {
	      val name = (new ObjectId()).toString
	      val promise = EventRoom.join(name, r, s, book.id.toString)
	      Async {
	        promise.map(enumerator => {
	         
	          import scala.concurrent.duration._
	          import scala.concurrent._
	          import play.api.libs.concurrent.Execution.Implicits
	          
	          val to = Enumerator.generateM[JsValue] { 
	          	play.api.libs.concurrent.Promise.timeout[Option[JsValue]](message=Some(Json.obj("type" -> "timeout"):JsValue), duration=20.seconds) 
	          }
	          val limited = (enumerator >- to) &> Enumeratee.breakE[JsValue](p => (p \ "type").asOpt[String] == Some("timeout"))
	          
	          val strEvents = limited &> toStringSource
	          
	          
	          /*
	           * We pad the enumerator to send some initial data so that iOS will act upon the Connection: Close header
	           * to ensure it does not pipeline other requests behind this one.  See HTTP Pipelining, Head Of Line blocking
	           * 
	           * We then limit the enumerator to just take the first result, so that the request completes. 
	           */
	          val eventSource = Enumerator[String]("{\"events\": [").andThen(strEvents).andThen(Enumerator("]}"))	          
	          
	          
	          /*// Apply the iteratee
	          val result = ChunkedResult[String](
	            header = ResponseHeader(Status.OK, Map(
	              HeaderNames.CONNECTION -> "close",
	              HeaderNames.CONTENT_LENGTH -> "-1",
	              HeaderNames.CONTENT_TYPE -> "application/json"
	            )),
	            chunks = {iteratee:Iteratee[String, Unit] => eventSource2.apply(iteratee) });
	          */
	          // Perform any required subscriptions
	          for (s <- subscriptions) processSubscriptions(name, tok, s)
	          
	          //result
	          Ok.stream(eventSource >>> Enumerator.eof).withHeaders(HeaderNames.CONNECTION -> "close").withSession(SessionStuff.withSessionKey(request.session, s))
	        })
	      }
	    }
	)
  }    
  
}