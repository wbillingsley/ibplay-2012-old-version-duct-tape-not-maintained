package controllers

import play.api.mvc.{Action, Controller}
import org.bson.types.ObjectId
import play.api.libs.json.Json
import play.api.mvc.Results._
import model.Util._
import com.wbillingsley.handy._
import Ref._
import com.theintelligentbook.ibmodel.mongo.{MarkupWikiPage, WikiPage, Book}
import com.theintelligentbook.ibmodel._
import play.api.data._
import Forms._
import scala.Some

object WikiController extends Controller {

  def read(pid:String) = WithEm {
    Action { implicit request =>

      val pageRef = WikiModel.read(RefById(classOf[WikiPage], pid), Approval(SessionStuff.loggedInReader(request)))
      for (p <- pageRef) yield {
        Ok(views.html.viewWikiPage(p.itself))
      }

    }
  }

  /**
   * Edit view
   * @param pid
   * @return
   */
  def edit(pid:String) = WithEm {
    Action { implicit request =>

      val tok = Approval(SessionStuff.loggedInReader(request))
      val pageRef = WikiModel.read(RefById(classOf[WikiPage], pid), tok)
      val result = for (
          p <- pageRef;
          approval <- tok ask SecurityModel.EditWikiPage(p.itself)
      ) yield {
        Ok(views.html.editWikiPage(p.itself))
      }
      result
    }
  }

  def editPost(pid:String) = WithEm { Action { implicit request =>

    val p = RefById(classOf[WikiPage], pid)
    val r = SessionStuff.loggedInReader(request)
    def form = Form(
      Forms.tuple(
        "content" -> text,
        "comment" -> optional(text)
      )
    ).bindFromRequest

    form.fold(
      formWithErrors => {
        InternalServerError(form.errorsAsJson)
      },
      value => {
        val (content, comment) = value
        val result = WikiModel.setPageContent(p, Approval(r), content=content, comment=comment)
        for (p <- result) yield {
          Redirect(routes.WikiController.read(pid))
        }
      }
    )
  }}

  /**
   * Upload an image into a wiki page.  Forwards to the method on MediaFileController
   * @param pid
   */
  def storeImage(pid:String) = {
    val bookId = RefById(classOf[WikiPage], pid).flatMap(_.book).getId.getOrElse("").toString
    MediaFileController.storeImage(bookId)
  }

  
  /**
   * Shows the viewing page for a MarkupWikiPage (ie, Markdown)
   */
  def viewMarkupWikiPage(pid:String) = WithEm {
    Action { implicit request =>
      
      (for (
        page <- RefById(classOf[MarkupWikiPage], pid);
        reader <- optionally(SessionStuff.loggedInReader(request));
        appr = Approval(reader);
        approved <- appr ask SecurityModel.ReadEntry(page.ce)
      ) yield {
        views.html.viewMarkupWikiPage(page)
      })
    }
  }

  /**
   * Shows the editing page for a MarkupWikiPage (ie, Markdown)
   */
  def viewEditMarkupWikiPage(pid:String) = WithEm {
    Action { implicit request =>

      (for (
        page <- RefById(classOf[MarkupWikiPage], pid);
        reader <- optionally(SessionStuff.loggedInReader(request));
        appr = Approval(reader);
        approved <- appr ask SecurityModel.EditContent(page.ce)
      ) yield {
        views.html.editMarkupWikiPage(page)
      })      
      
    }
  }
  
  /**
   * Handles the form submission of a markup wiki page
   */
  def editMarkupWikiPagePost(pid:String) = WithEm { Action { implicit request =>

    val p = RefById(classOf[MarkupWikiPage], pid)
    val r = SessionStuff.loggedInReader(request)
    def form = Form(
      Forms.tuple(
        "content" -> text,
        "comment" -> optional(text)
      )
    ).bindFromRequest

    form.fold(
      formWithErrors => {
        InternalServerError(form.errorsAsJson)
      },
      value => {
        val (content, comment) = value
        val result = WikiModel.setMarkupPageContent(p, Approval(r), content=content, comment=comment)
        val resp = (for (p <- result) yield {
          Redirect(routes.WikiController.viewMarkupWikiPage(pid))
        })
        resp
      }
    )
  }}  

}
