package controllers

import play.api._
import mvc.{ResponseHeader, Action, SimpleResult, ChunkedResult}
import http.{HeaderNames, Status}
import libs.Comet
import libs.iteratee.{Enumerator, Iteratee, Enumeratee}
import play.api.libs.concurrent.Execution.Implicits._
import mvc.{Action, SimpleResult}
import play.api.mvc.Results._

import com.theintelligentbook.ibmodel._
import com.theintelligentbook.ibmodel.mongo._
import model.{EventRoom, ChatEvents, IBJson, Identity, Util}
import Util._

import play.api.data._
import play.api.data.Forms._
import com.wbillingsley.handy._
import Ref._
import org.bson.types.ObjectId
import scala.collection.mutable

import play.api.libs.json._
import com.wbillingsley.handy.RefById
import templates.Html


object ChatController {


  /**
   * Adds a chat comment to the stream.  
   */
  def addChatComment(bookId: String) = WithEm { Action { implicit request =>

    val form = Form(
      Forms.tuple(
        "content" -> text,
        "anonymous" -> boolean,
        "viewingCe" -> optional(text)
      )
    )
    val (content, anonymous, viewingCe) = form.bindFromRequest().get

    val b = RefById(classOf[Book], bookId)
    val r = SessionStuff.loggedInReader(request)
    val s = request.session.get("sessionKey").getOrElse((new ObjectId()).toString)

    val viewCe = Ref.fromOptionId(classOf[ContentEntry], viewingCe)
    
    if (content.trim.isEmpty) {
      NoContent.itself
    } else {
      val refResult = for (
        book <- b;
        reader <- optionally(r);
        topics <- optionally(viewCe.map(_.topics.toSeq));
        cc <- ChatModel.addChatComment(book.itself, Approval(reader), anonymous, Some(s), Some(content.trim), topics.getOrElse(Seq.empty))
      ) yield {
        EventRoom.notifyEventRoom(new ChatEvents.ChatMessage(
            anonymous=anonymous, book=book, reader=reader, msg=Some(content), topics=topics.getOrElse(Seq.empty)
        ))
        Ok("").withSession(request.session + ("sessionKey" -> s))
      }
      refResult
    }
  }}

  def listenForEvents(bookId:String) = Action { implicit request =>

    // We should already have a session key, but if not, let's generate a throwaway one
    val s = SessionStuff.sessionKey(request.session).getOrElse((new ObjectId()).toString) 
    
    refResultToResult(
	    for (
	      book <- RefById(classOf[Book], new ObjectId(bookId));
	      r <- optionally(SessionStuff.loggedInReader(request));
	      approved <- Approval(r) ask SecurityModel.Read(book.itself) 
	    ) yield {
	      val name = (new ObjectId()).toString
	      val promise = EventRoom.join(name, r, s, book.id.toString, ChatEvents.ChatStream(book.id.toString))
	      Async {
	        promise.map(enumerator => {
	          val eventSource = enumerator &> Comet(callback = "parent.IBEvents.receive")
	          Ok.stream(eventSource)
	        })
	      }      
	    }
	)    
  }

  def serverSentEvents(bookId:String) = Action { implicit request =>

    import scala.language.reflectiveCalls
    
    val toEventSource = Enumeratee.map[JsValue]{ msg =>
      val d = "data: "+ msg.toString +"\n\n"
      //val m = "" + d.length + "\n" + d
      d
    }
    
    // We should already have a session key, but if not, let's generate a throwaway one
    val s = SessionStuff.sessionKey(request.session).getOrElse((new ObjectId()).toString) 
    
    refResultToResult(
	    for (
	      book <- RefById(classOf[Book], new ObjectId(bookId));
	      r <- optionally(SessionStuff.loggedInReader(request));
	      approved <- Approval(r) ask SecurityModel.Read(book.itself) 
	    ) yield {
	      val name = (new ObjectId()).toString
	      val promise = EventRoom.join(name, r, s, book.id.toString, ChatEvents.ChatStream(book.id.toString))
	      Async {
	        promise.map(enumerator => {
	          /*
	           We pad the enumerator to send some initial data so that iOS will act upon the Connection: Close header
	           to ensure it does not pipeline other requests behind this one.  See HTTP Pipelining, Head Of Line blocking
	            */
	          val paddedEnumerator = Enumerator[JsValue](Json.toJson(Map("type" -> Json.toJson("ignore")))).andThen(enumerator)
	          val eventSource = paddedEnumerator &> toEventSource
	
	          /*
	          SimpleResult(
	            header = ResponseHeader(Status.OK, Map(
	              HeaderNames.CONNECTION -> "Close",
	              HeaderNames.CONTENT_LENGTH -> "-1",
	              HeaderNames.CONTENT_TYPE -> "text/event-stream"
	            )),
	            body = eventSource)
	            */
	
	          ChunkedResult[String](
	            header = ResponseHeader(Status.OK, Map(
	              HeaderNames.CONNECTION -> "close",
	              HeaderNames.CONTENT_LENGTH -> "-1",
	              HeaderNames.CONTENT_TYPE -> "text/event-stream"
	            )),
	            chunks = {iteratee:Iteratee[String, Unit] => eventSource.apply(iteratee) });
	        })
	      }
	    }
	)
  }

  def getRecentChatComments(bookId:String, afterId:Option[String], beforeId:Option[String]) = WithEm { Action { implicit request =>
    
    val t1 = System.currentTimeMillis()
    for (
        book <- RefById(classOf[Book], bookId);
        tok = Approval(SessionStuff.loggedInReader(request))
    ) yield {
	    val before = Ref.fromOptionId(classOf[ChatComment], beforeId)
	    val after = Ref.fromOptionId(classOf[ChatComment], afterId)
	    val comments = ChatModel.getRecentChatComments(book.itself, after, before, Some(50), tok)
      
	    import scala.language.reflectiveCalls
	    
	    var sep = ""
	    val enumeratee = Enumeratee.mapM[ChatComment]{ c => 
	      (for (json <- IBJson.chatCommentToJson(c.itself)) yield {
	        val r = sep + json.toString 
	        sep = ", "
	        r
	      }).toFuture 
	    } compose Enumeratee.filter(!_.isEmpty) compose Enumeratee.map(_.get)

	    val enumerator = Enumerator("{ \"comments\" : [") andThen 
	      (comments.enumerator through enumeratee) andThen 
	      Enumerator("] }") andThen 
	      Enumerator.eof[String]
	    
	    val t2 = System.currentTimeMillis()
	    println("" + (t2 - t1) + "ms")
	    
	    SimpleResult(
	        header = ResponseHeader(200, Map("Content-Type" -> "application/json")),
	        body = enumerator
	    )
	    
    }
  }}

  /**
   * Chat room page
   */
  def viewBookChat(bookId:String) = WithEm { Action { implicit request =>
    val tok = Approval(SessionStuff.loggedInReader(request))
    for (
      book <- RefById(classOf[Book], new ObjectId(bookId));
      approved <- tok ask SecurityModel.Read(book.itself) 
    ) yield {
      views.html.viewBookChat(book.itself, tok)
    }
  }}


}
