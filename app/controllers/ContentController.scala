package controllers

import play.api._
import play.api.mvc._
import play.api.mvc.Results._

import com.theintelligentbook.ibmodel._
import RefConversions._
import com.theintelligentbook.ibmodel.mongo._
import model.{IBJson, Identity}

import play.api.data._
import play.api.data.Forms._
import com.wbillingsley.handy._
import Ref._
import org.bson.types.ObjectId
import scala.collection.mutable

import model.Util._

import play.api.libs.json._
import java.util.zip.ZipInputStream


object ContentController extends Controller {

  /** The content of a freshly created wiki page. Updated on startup. */
  var defaultWikiPageContent = "(to be loaded)";

  var defaultMarkupWikiPageContent = "(to be loaded)";
  
  /**
   * Renders the content viewing page
   * @param bid
   * @param entry
   * @param adj
   * @param noun
   * @param topic
   * @param site
   * @return
   */
  def lookupView(bid:String,
        entry:Option[String] = None,
        adj:Option[String] = None, noun:Option[String] = None, topic:Option[String] = None,
        site:Option[String] = None
  ) = WithEm {
    Action { implicit request =>

      val b = RefById(classOf[Book], new ObjectId(bid)).fetch
      b match {
        case RefItself(book) => {
          val qs = request.queryString

          val m = mutable.Map.empty[String, String]
          entry.map { a => m.put("entry", a) }
          adj.map { a => m.put("adj", a) }
          noun.map { a => m.put("noun", a) }
          topic.map { a => m.put("topic", a) }
          site.map { a => m.put("site", a) }
          val js = Json.toJson(m.toMap)

          Ok(views.html.viewContent(b, js))
        }
        case RefFailed(exc) => InternalServerError(exc.getMessage)
        case RefNone => NotFound
      }

    }
  }


  /**
   * JSON API form handler for looking up a content entry
   * @param bid
   * @param entry
   * @param adj
   * @param noun
   * @param topic
   * @param site
   * @return
   */
  def contentQuery(bid:String,
                 entry:Option[String],
                 adj:Option[String], noun:Option[String], topic:Option[String],
                 site:Option[String]
                  ) = WithEm {
    Action { implicit request =>

      val b = RefById(classOf[Book], new ObjectId(bid)).fetch

      val tok:Approval[Reader] = SessionStuff.loggedInReader(request)
      val rec = entry match {
        case Some(eid) =>
          RefById(classOf[ContentEntry], eid).fetch
        case None => {
          val filters = mutable.Map.empty[String, String]
          adj.foreach {a => filters.put("adjective", a)}
          noun.foreach {a => filters.put("noun", a)}
          site.foreach {a => filters.put("site", a)}
          ContentModel.recommendCE(b, tok, topic, filters.toMap)
        }
      }

      val (ce, pres) = ContentModel.presAndCE(rec)
      val seeAlso = ContentModel.getAlternatives(ce, tok)

      ceAndPresResponse(tok, b, ce.fetch, pres.fetch, seeAlso)
    }
  }

  /**
   * JSON API handler for retrieving a list of content entries for a topic
   * @param bid
   * @param topic
   * @return
   */
  def entriesForTopic(bid:String, topic:String) = WithEm { Action { implicit request =>
    val b = RefById(classOf[Book], bid)
    val tok:Approval[Reader] = SessionStuff.loggedInReader(request)
    
    import Ref._    
    
    val entries = ContentModel.getAllEntriesForTopic(b, tok, topic)
    val jsonEntries = entries.flatMap(e => IBJson.entryToJson(e.itself))
    
    val seq = jsonEntries.fold(Seq.empty[JsObject])(_ :+ _)
    seq.map(s => Ok(Json.obj("entries" -> s)))
  }}

  /**
   * Form handler for creating a presentation around a content entry
   * @param bookId
   * @return
   */
  def makePresentation(bookId:String) = WithEm { Action { implicit request =>
    val b = RefById(classOf[Book], new ObjectId(bookId)).fetch
    val tok:Approval[Reader] = SessionStuff.loggedInReader(request).fetch

    val form = Form(
      Forms.single(
        "including" -> text
      )
    )
    val includingCeId = form.bindFromRequest().get
    val incl = RefById(classOf[ContentEntry], new ObjectId(includingCeId)).fetch
    val p = PresentationModel.makePresentation(b, tok, incl)
    ceAndPresResponse(tok, b, incl, p)
  }}


  /**
   * Updates the tags, name, note, protect, inTrash, showFirst of a content entry.
   * @param entryId
   * @return
   */
  def editDetails(entryId:String) =  WithEm { Action { implicit request =>
    val ce = RefById(classOf[ContentEntry], entryId).fetch
    val tok:Approval[Reader] = SessionStuff.loggedInReader(request).fetch
    val mapping = request.body.asMultipartFormData.get.asFormUrlEncoded
    val presKey = fetchStr(mapping, "presentation")
    val pres = Ref.fromOptionId(classOf[Presentation], presKey)

    val result = ce.flatMap { entry =>
      entry.inTrash = fetchBool(mapping, "ce_inTrash").getOrElse(false)
      entry.protect = fetchBool(mapping, "ce_protect").getOrElse(false)
      entry.showFirst = fetchBool(mapping, "ce_showFirst").getOrElse(false)
      entry.name = fetchStr(mapping, "ce_name")
      entry.shortDescription = fetchStr(mapping, "ce_note")
      entry.nouns = fetchSet(mapping, "ce_noun")
      entry.adjectives = fetchSet(mapping, "ce_adj")
      entry.topics = fetchSet(mapping, "ce_topic")
      ContentModel.saveContentEntry(entry, tok)
    }
    ceAndPresResponse(tok, result.flatMap(_.book), result, pres)
  }}

  /**
   * REST API for adding a wiki page
   * @param bookId
   * @return
   */
  def addWikiPageEntry(bookId:String) = WithEm { Action { implicit request =>
    val b = RefById(classOf[Book], bookId)
    val tok:Approval[Reader] = SessionStuff.loggedInReader(request)

    val mapping = request.body.asMultipartFormData.get.asFormUrlEncoded

    val presKey = fetchStr(mapping, "presentation")
    val pres = Ref.fromOptionId(classOf[Presentation], presKey)
    val addToPres = fetchBool(mapping, "ce_addToPres").getOrElse(false)

    val ceRef = WikiModel.newPageEntry(
      bookRef=b, tok=tok, content=defaultWikiPageContent,
      name=fetchStr(mapping, "ce_name"),
      shortDescription=fetchStr(mapping, "ce_note"),
      adjectives=fetchSet(mapping, "ce_adj"),
      nouns=fetchSet(mapping, "ce_noun"),
      topics=fetchSet(mapping, "ce_topic"),
      highlightTopic=fetchStr(mapping, "ce_highlightTopic"),
      showFirst=fetchBool(mapping, "ce_showFirst").getOrElse(false),
      protect=fetchBool(mapping, "ce_protect").getOrElse(false)
    )
    
    if (addToPres) {
      for (ce <- ceRef) {
        val rm = new RefManyById(classOf[ContentEntry], Seq(ce.id))
        PresentationModel.insertEntriesIntoPresentation(pres, rm, -1, tok)
      }
    }
    
    val r = (for (ce <- ceRef) yield {
      request match {
        case Accepts.Html() => Redirect(routes.ContentController.lookupView(bookId, entry=Some(ce.id.toString))).itself
        case Accepts.Json() => ceAndPresResponse(tok, b, ceRef, pres)
        case _ => NotAcceptable.itself
      }    
    }).flatten    
    r
  }}
  
  /**
   * REST API for adding a markup (Markdown, etc) wiki page
   * @param bookId
   * @return
   */
  def addMarkupWikiPageEntry(bookId:String) = WithEm { Action { implicit request =>
    
    val mapping = request.body.asMultipartFormData.get.asFormUrlEncoded

    val presKey = fetchStr(mapping, "presentation")
    val pres = Ref.fromOptionId(classOf[Presentation], presKey)
    val addToPres = fetchBool(mapping, "ce_addToPres").getOrElse(false)
    
    val resp = (for (
        book <- RefById(classOf[Book], bookId);
        reader <- optionally(SessionStuff.loggedInReader(request));
        tok = Approval(reader);
        ce <- WikiModel.newMarkupPageEntry(
	      book=book.itself, tok=tok, content=defaultMarkupWikiPageContent,
	      name=fetchStr(mapping, "ce_name"),
	      shortDescription=fetchStr(mapping, "ce_note"),
	      adjectives=fetchSet(mapping, "ce_adj"),
	      nouns=fetchSet(mapping, "ce_noun"),
	      topics=fetchSet(mapping, "ce_topic"),
	      highlightTopic=fetchStr(mapping, "ce_highlightTopic"),
	      showFirst=fetchBool(mapping, "ce_showFirst").getOrElse(false),
	      protect=fetchBool(mapping, "ce_protect").getOrElse(false)
	    )
    ) yield {
      if (addToPres) {
        val rm = new RefManyById(classOf[ContentEntry], Seq(ce.id))
        PresentationModel.insertEntriesIntoPresentation(pres, rm, -1, tok)
      }      
      
	  request match {
	    case Accepts.Html() => Redirect(routes.ContentController.lookupView(bookId, entry=Some(ce.id.toString))).itself
	    case Accepts.Json() => ceAndPresResponse(tok, book.itself, ce.itself, pres)
	    case _ => NotAcceptable.itself
	  }
    }).flatten
    
    resp
  }}  



  /** Appends an existing content entry at to a presentation */
  def appendEntryToPres(presId:String) = WithEm { Action { implicit request =>
    val p = RefById(classOf[Presentation], presId)
    val tok:Approval[Reader] = SessionStuff.loggedInReader(request)

    val mapping = request.body.asMultipartFormData.get.asFormUrlEncoded
    val entry = Ref.fromOptionId(classOf[ContentEntry], fetchStr(mapping, "entryId"))
    val rm = new RefManyById(classOf[ContentEntry], entry.getId.toSeq)
    
    val result = PresentationModel.insertEntriesIntoPresentation(p, rm, -1, tok)
    val response = result.flatMap { r => 
      ceAndPresResponse(tok, r.book, rm.first, r.itself)
    }
    response
  }}

  /**
   * REST API for uploading a ZIP of image files
   * @param bookId
   * @return
   */
  def uploadZipAsEntries(bookId:String) = WithEm { Action { implicit request =>
    val b = RefById(classOf[Book], bookId)
    val tok:Approval[Reader] = SessionStuff.loggedInReader(request)

    val fd = request.body.asMultipartFormData.get
    val mapping = fd.asFormUrlEncoded

    val presKey = fetchStr(mapping, "presentation")
    val pres = Ref.fromOptionId(classOf[Presentation], presKey)
    val addToPres = fetchBool(mapping, "ce_addToPres").getOrElse(false)


    // First, upload the zip
    fd.file("file") match {
      case Some(zipFile) => {
        import java.io.FileInputStream

        val is = new FileInputStream(zipFile.ref.file)
        val zis = new ZipInputStream(is)
        try {
          val entries = MediaModel.newContentEntriesFromZip(
            book=b, tok=tok, zipInputStream=zis,
            adjectives=fetchSet(mapping, "ce_adj"),
            nouns=fetchSet(mapping, "ce_noun"),
            topics=fetchSet(mapping, "ce_topic"),
            highlightTopic=fetchStr(mapping, "ce_highlightTopic"),
            showFirst=fetchBool(mapping, "ce_showFirst").getOrElse(false),
            protect=fetchBool(mapping, "ce_protect").getOrElse(false)
          )
          
          val ids = entries.fetch.toSeq.map(_.id)
          val rm = new RefManyById(classOf[ContentEntry], ids)

          if (addToPres) {            
            val result = PresentationModel.insertEntriesIntoPresentation(pres, rm, -1, tok)
            // If successful, we want the first added content entry, within the set presentation
            ceAndPresResponse(tok, b, entries.first, result)
          } else {
            val result = PresentationModel.newPresentationEntry(
                book=b, approval=tok, entries=rm,
                name=fetchStr(mapping, "ce_name"),
                shortDescription=fetchStr(mapping, "ce_note"),
                adjectives=fetchSet(mapping, "ce_adj"),
                nouns=fetchSet(mapping, "ce_noun"),
                topics=fetchSet(mapping, "ce_topic"),
                highlightTopic=fetchStr(mapping, "ce_highlightTopic"),
                showFirst=fetchBool(mapping, "ce_showFirst").getOrElse(false),
                protect=fetchBool(mapping, "ce_protect").getOrElse(false)
              )
            ceAndPresResponse(tok, b, entries.first, result)
          }
        } finally {
          is.close()
        }
      }
      case None => NotModified
    }
  }}


  /**
   * Moves slides around in a presentation
   * @param bid Book ID
   */
  def movePresEntry(bid:String) = WithEm { Action { implicit request =>

    val params = request.body.asFormUrlEncoded.get

    val b = RefById(classOf[Book], bid)

    val from = fetchInt(params, "from").getOrElse(0)
    val to = fetchInt(params, "to").getOrElse(0)
    val ceIdx = fetchInt(params, "ceIdx").getOrElse(0)
    val p = Ref.fromOptionId(classOf[Presentation], fetchStr(params, "presentation"))

    val tok:Approval[Reader] = SessionStuff.loggedInReader(request)

    val result = PresentationModel.moveEntriesInPresentation(p, from, from + 1, to, tok).fetch

    ceAndPresResponse(tok, b, RefNone, result)

  }}

  /**
   * Removes entries from a presentation, and optionally trashes them
   * @param pid
   * @return
   */
  def removeEntriesFromPres(pid:String) = WithEm { Action { implicit request =>

    val pres = RefById(classOf[Presentation], pid).fetch
    val tok:Approval[Reader] = SessionStuff.loggedInReader(request)

    val fd = request.body.asMultipartFormData.get
    val mapping = fd.asFormUrlEncoded
    val indices = mapping.get("index").getOrElse(Seq.empty[String]).map(_.toInt)
    val entryIds = mapping.get("entryId").getOrElse(Seq.empty[String])
    val trash = fetchBool(mapping, "trash").getOrElse(false)

    var hasBeenModified = false
    for (
      p <- pres;
      oldIds = p.entries.getIds;
      (index, id) <- indices.zip(entryIds)
    ) {
      if (oldIds(index).toString != id.toString) {
        hasBeenModified = true
      }
    }
    
    if (hasBeenModified) {
      Results.BadRequest("{ \"error\": \"Presentation has been modified. Reload and try again.\" }");
    } else {

      if (trash) {
        for (
            p <- pres; 
            entries = p.entries.fetch.toSeq;
            index <- indices;
            e = entries(index)
        ) {
          e.inTrash = true
          ContentModel.saveContentEntry(e, tok)
        }
      }

      val result = PresentationModel.removeEntriesFromPresentation(pres, indices, tok).fetch;

      ceAndPresResponse(tok, result.flatMap(_.book), RefNone, result)
    }
  }}

  def upVote(eid:String) = WithEm { Action { implicit request =>   
    val ce = RefById(classOf[ContentEntry], eid)
    val tok:Approval[Reader] = SessionStuff.loggedInReader(request)
    for (score <- ContentModel.upVote(ce, tok)) yield {
      Ok(Json.obj("score" -> score))
    }
  }}
  
  def downVote(eid:String) = WithEm { Action { implicit request =>   
    val ce = RefById(classOf[ContentEntry], eid)
    val tok:Approval[Reader] = SessionStuff.loggedInReader(request)
    for (score <- ContentModel.downVote(ce, tok)) yield {
      Ok(Json.obj("score" -> score))
    }
  }}


}
