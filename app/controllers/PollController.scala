package controllers

import play.api.mvc.{ResponseHeader, ChunkedResult, Action, Controller}
import play.api.mvc.Results._
import org.bson.types.ObjectId
import com.wbillingsley.handy._
import Ref._
import com.theintelligentbook.ibmodel._
import model.{EventRoom, PollEvents, TextPollEvents, IBJson}
import model.Util._

import mongo._
import play.api.data._
import play.api.libs.Comet
import play.api.libs.iteratee.{Iteratee, Enumerator, Enumeratee}
import play.api.libs.concurrent.Execution.Implicits._
import play.api.libs.json.{JsValue, Json}
import play.api.http.{HeaderNames, Status}



/**
 * Created with IntelliJ IDEA.
 * User: wbillingsley
 * Date: 22/07/12
 * Time: 12:14 PM
 * To change this template use File | Settings | File Templates.
 */

object PollController extends Controller {

  def view(pid:String) = WithEm {
    Action { implicit request =>

      val skOpt = SessionStuff.sessionKey(request.session)
      val resp = (for (
          poll <- RefById(classOf[Poll], pid);
          reader <- optionally(SessionStuff.loggedInReader(request));
          appr = Approval(reader);
          approved <- appr ask SecurityModel.Read(poll.book)
      ) yield {
        val pr = PollResponseDAO.findVote(poll.itself, reader, skOpt)
        
        request match {
          case Accepts.Html() => Ok(views.html.viewPoll(poll.itself, pr, appr)).itself
          case Accepts.Json() => for (j <- IBJson.pollToJson(poll.itself, appr)) yield Ok(j)
          case _ => NotAcceptable.itself
        }            
      }).flatten
      resp
    }
  }

  /**
   * Edit view
   * @param pid
   * @return
   */
  def edit(pid:String) = WithEm {
    Action { implicit request =>
      for (
          poll <- RefById(classOf[Poll], pid);
          reader <- optionally(SessionStuff.loggedInReader(request));
          approved <- Approval(reader) ask SecurityModel.Read(poll.book)
      ) yield {
        views.html.editPoll(poll)
      }      
    }
  }

  /**
   * Gets a Boolean value from a map of parameters
   * @param map
   * @param key
   * @return
   */
  private def fetchBool(map:Map[String,Seq[String]], key:String) = {
    map.getOrElse(key, Seq.empty[String]).headOption.map { v =>
      val str = v.toLowerCase
      str == "on" || str == "true" || str == "1"
    }
  }
  
  /**
   * REST API for adding a wiki page
   * @param bookId
   * @return
   */
  def addPollEntry(bookId:String) = WithEm { Action { implicit request =>
    val b = RefById(classOf[Book], new ObjectId(bookId))
    val tok:Approval[Reader] = Approval(SessionStuff.loggedInReader(request))

    val mapping = request.body.asMultipartFormData.get.asFormUrlEncoded

    val presKey = fetchStr(mapping, "presentation")
    val pres = Ref.fromOptionId(classOf[Presentation], presKey)
    val addToPres = fetchBool(mapping, "ce_addToPres").getOrElse(false)

    val ceRef = PollModel.newPollEntry(
      b, tok,
      name=fetchStr(mapping, "ce_name"),
      shortDescription=fetchStr(mapping, "ce_note"),
      adjectives=fetchSet(mapping, "ce_adj"),
      nouns=fetchSet(mapping, "ce_noun"),
      topics=fetchSet(mapping, "ce_topic"),
      highlightTopic=fetchStr(mapping, "ce_highlightTopic"),
      showFirst=fetchBool(mapping, "ce_showFirst").getOrElse(false),
      protect=fetchBool(mapping, "ce_protect").getOrElse(false)
    )
    if (addToPres) {
      for (ce <- ceRef) {
        val rm = new RefManyById(classOf[ContentEntry], Seq(ce.id))
        PresentationModel.insertEntriesIntoPresentation(pres, rm, -1, tok)
      }
    }
    
    val r = (for (ce <- ceRef) yield {
      request match {
        case Accepts.Html() => Redirect(routes.ContentController.lookupView(bookId, entry=Some(ce.id.toString))).itself
        case Accepts.Json() => ceAndPresResponse(tok, b, ceRef, pres)
        case _ => NotAcceptable.itself
      }    
    }).flatten    
    r
    
  }}  


  def handleEditPoll(pid:String) = WithEm { Action { implicit request =>
    refResultToResult(
	    (for (
	        poll <- RefById(classOf[Poll], pid);
	        tok = Approval(SessionStuff.loggedInReader(request))        
	    ) yield {
	      val params = request.body.asFormUrlEncoded.get
	      
	      poll.question = params.get("question").flatMap(_.headOption)
	      for (v <- params.get("answerVisibility").flatMap(_.headOption)) {
	        poll.answerVisibility = PollResultsVisibility.valueOf(v)
	      }
	      for (v <- params.get("resultsVisibility").flatMap(_.headOption)) {
	        poll.resultsVisibility = PollResultsVisibility.valueOf(v)
	      }
	      for (v <- params.get("mode").flatMap(_.headOption)) {
	        poll.mode = PollMode.valueOf(v)
	      }
	      poll.mayMoveVote = fetchBool(params, "mayMoveVote").getOrElse(false)
	      poll.options = params.get("option").getOrElse(Seq.empty[String])
	      poll.optFeedback = params.get("optFeedback").getOrElse(Seq.empty[String])
	      poll.optScore = params.get("optScore").getOrElse(Seq.empty[String]).map(_.toInt)
	
	      for (saved <- PollModel.savePoll(poll, tok)) yield {
	        Redirect(routes.PollController.view(saved.id.toString))      
	      }
	    })
    )
  }}


  /**
   * Called when a vote arrives by POST
   */
  def vote(pid:String) = WithEm { Action { implicit request =>

    refResultToResult (for (
        poll <- RefById(classOf[Poll], pid);
        tok = Approval(SessionStuff.loggedInReader(request));        
        mapping = request.body.asMultipartFormData.get.asFormUrlEncoded;
        skOpt = SessionStuff.sessionKey(request.session);
        sk = skOpt.getOrElse(SessionStuff.newSessionKey);
        answer = fetchSetInts(mapping, "answer");
        pr <- PollModel.respondToPoll(poll.itself, tok, Some(sk), answer) 
    ) yield {
        EventRoom.default ! PollEvents.Vote(pr)

        skOpt match {
          case Some(k) => Redirect(routes.PollController.view(pid))
          case None => Redirect(routes.PollController.view(pid)).withSession(SessionStuff.withSessionKey(request.session, sk))
        }
    })
    
  }}

  def listenForEvents(pollId:String) = Action { implicit request =>
    refResultToResult(
	    for (
	        poll <- RefById(classOf[Poll], pollId);
	        reader <- optionally(SessionStuff.loggedInReader(request));
	        session = SessionStuff.sessionKey(request.session).getOrElse(SessionStuff.newSessionKey);
	        approved <- Approval(reader) ask SecurityModel.Read(poll.book)        
	    ) yield {
	      val name = (new ObjectId()).toString
	      val promise = EventRoom.join(name, reader, session, poll._book.toString, PollEvents.PollStream(poll.id.toString))
	      Async {
	        promise.map(enumerator => {
	          Ok.stream(enumerator &> Comet(callback = "parent.IBPollResults.receive"))
	        })
	      }      
	    }
    )
  }

  def serverSentEvents(pollId:String) = Action { implicit request =>

    import scala.language.reflectiveCalls
    
    val toEventSource = Enumeratee.map[JsValue]{ msg =>
      val d = "data: "+ msg.toString +"\n\n"
      d
    }

    refResultToResult(
        for (
	        poll <- RefById(classOf[Poll], pollId);
	        reader <- optionally(SessionStuff.loggedInReader(request));
	        session = SessionStuff.sessionKey(request.session).getOrElse(SessionStuff.newSessionKey);
	        approved <- Approval(reader) ask SecurityModel.Read(poll.book)        
	    ) yield {    
	      val name = (new ObjectId()).toString
	      val promise = EventRoom.join(name, reader, session, poll._book.toString, PollEvents.PollStream(poll.id.toString))
	      Async {
	        promise.map(enumerator => {
	          /*
	           We pad the enumerator to send some initial data so that iOS will act upon the Connection: Close header
	           to ensure it does not pipeline other requests behind this one.  See HTTP Pipelining, Head Of Line blocking
	            */
	          val paddedEnumerator = Enumerator[JsValue](Json.toJson(Map("type" -> Json.toJson("ignore")))).andThen(enumerator)
	          val eventSource = paddedEnumerator &> toEventSource
	
	          ChunkedResult[String](
	            header = ResponseHeader(play.api.http.Status.OK, Map(
	              HeaderNames.CONNECTION -> "close",
	              HeaderNames.CONTENT_LENGTH -> "-1",
	              HeaderNames.CONTENT_TYPE -> "text/event-stream"
	            )),
	            chunks = {iteratee:Iteratee[String, Unit] => eventSource.apply(iteratee) });
	        })
	      }
        }
    )
  }

  
  def pushToChat(pollId:String) = WithEm { Action { implicit request => 
    refResultToResult (for (
        p <- RefById(classOf[Poll], pollId);
        a <- Approval(SessionStuff.loggedInReader(request)) ask SecurityModel.EditPoll(p.itself)
    ) yield {
      EventRoom.notifyEventRoom(PollEvents.PushPollToChat(p))
      Ok("")      
    })
  }}
  
  def viewTextPoll(pid:String) = WithEm {
    Action { implicit request =>

      val skOpt = SessionStuff.sessionKey(request.session)
      val resp = (for (
          poll <- RefById(classOf[TextPoll], pid);
          reader <- optionally(SessionStuff.loggedInReader(request));
          appr = Approval(reader);
          approved <- appr ask SecurityModel.Read(poll.book)
      ) yield {
        request match {
          case Accepts.Html() => Ok(views.html.viewTextPoll(poll, appr)).itself
          case Accepts.Json() => for (j <- IBJson.textPollToJson(poll.itself, appr)) yield Ok(j)
          case _ => NotAcceptable.itself
        }            
      }).flatten
      resp
    }
  }  
  
  /**
   * Edit view
   * @param pid
   * @return
   */
  def editTextPoll(pid:String) = WithEm {
    Action { implicit request =>
      for (
          poll <- RefById(classOf[TextPoll], pid);
          reader <- optionally(SessionStuff.loggedInReader(request));
          appr = Approval(reader);          
          approved <- appr ask SecurityModel.EditContent(poll.ce)
      ) yield {
        views.html.editTextPoll(poll, appr)
      }      
    }
  }  
  
  /**
   * REST API for adding a text poll page
   * @param bookId
   * @return
   */
  def addTextPollEntry(bookId:String) = WithEm { Action { implicit request =>
    val b = RefById(classOf[Book], new ObjectId(bookId))
    val tok:Approval[Reader] = Approval(SessionStuff.loggedInReader(request))

    val mapping = request.body.asMultipartFormData.get.asFormUrlEncoded

    val presKey = fetchStr(mapping, "presentation")
    val pres = Ref.fromOptionId(classOf[Presentation], presKey)
    val addToPres = fetchBool(mapping, "ce_addToPres").getOrElse(false)

    val ceRef = PollModel.newTextPollEntry(
      b, tok,
      name=fetchStr(mapping, "ce_name"),
      shortDescription=fetchStr(mapping, "ce_note"),
      adjectives=fetchSet(mapping, "ce_adj"),
      nouns=fetchSet(mapping, "ce_noun"),
      topics=fetchSet(mapping, "ce_topic"),
      highlightTopic=fetchStr(mapping, "ce_highlightTopic"),
      showFirst=fetchBool(mapping, "ce_showFirst").getOrElse(false),
      protect=fetchBool(mapping, "ce_protect").getOrElse(false), 
      text = "Would you like to edit this to change the question?"
    )
    if (addToPres) {
      for (ce <- ceRef) {
        val rm = new RefManyById(classOf[ContentEntry], Seq(ce.id))
        PresentationModel.insertEntriesIntoPresentation(pres, rm, -1, tok)
      }
    }
    
    val r = (for (ce <- ceRef) yield {
      request match {
        case Accepts.Html() => Redirect(routes.ContentController.lookupView(bookId, entry=Some(ce.id.toString))).itself
        case Accepts.Json() => ceAndPresResponse(tok, b, ceRef, pres)
        case _ => NotAcceptable.itself
      }    
    }).flatten    
    r
    
  }}  
  
  /**
   * Form handler for editing text polls
   */
  def handleEditTextPoll(pid:String) = WithEm { Action { implicit request =>
    refResultToResult(
	    (for (
	        poll <- RefById(classOf[TextPoll], pid);
	        tok = Approval(SessionStuff.loggedInReader(request))        
	    ) yield {
	      val mapping = request.body.asMultipartFormData.get.asFormUrlEncoded
	      poll.question = fetchStr(mapping, "question")
	
	      for (saved <- PollModel.saveTextPoll(poll, tok)) yield {
	        Redirect(routes.PollController.viewTextPoll(saved.id.toString))      
	      }
	    })
    )
  }}  
  
  
  def pushTextPollToChat(pollId:String) = WithEm { Action { implicit request => 
    refResultToResult (for (
        p <- RefById(classOf[TextPoll], pollId);
        a <- Approval(SessionStuff.loggedInReader(request)) ask SecurityModel.EditContent(p.ce)
    ) yield {
      EventRoom.notifyEventRoom(TextPollEvents.PushToChat(p))
      Ok("")      
    })
  }}
    
  
  
  val wordMatch = "[a-zA-Z]+".r
    //
    //
  
  /** TODO: Replace this with a list of stop words that is configurable per poll. */
  val stopWords = Seq("an", "and", "are", "as", "at", "be", "by", "for", "from", "has", "he", 
      "in", "it", "its", "of", "on", "that", "the", "to", "was", "were", "will", "with")
  

  def addTextPollAnswer(tpId:String) = WithEm { Action { implicit request => 
    val r = refResultToResult (for (
        poll <- RefById(classOf[TextPoll], tpId);
        tok = Approval(SessionStuff.loggedInReader(request));        
        mapping = request.body.asMultipartFormData.get.asFormUrlEncoded;
        skOpt = SessionStuff.sessionKey(request.session);
        sk = skOpt.getOrElse(SessionStuff.newSessionKey);
        answer <- Ref(fetchStr(mapping, "answer"));
        words = (for (w <- wordMatch.findAllIn(answer) if w.length > 1) yield w.toLowerCase).toSet;
        filteredWords = words.filter(w => !stopWords.contains(w));
        pr <- PollModel.respondToTextPoll(poll.itself, tok, Some(sk), answer, filteredWords) 
    ) yield {
        
        EventRoom.default ! TextPollEvents.Vote(poll.itself, filteredWords)

        skOpt match {
          case Some(k) => Redirect(routes.PollController.viewTextPoll(tpId))
          case None => Redirect(routes.PollController.view(tpId)).withSession(SessionStuff.withSessionKey(request.session, sk))
        }
    }) 
    r
  }}
  
  /**
   * Looks up answers to a text poll that contain a particular word
   */
  def queryTextPollAnswers(tpId:String, words:Option[String]) =  WithEm { Action { implicit request => 
    val r = refResultToResult (for (
        poll <- RefById(classOf[TextPoll], tpId);
        tok = Approval(SessionStuff.loggedInReader(request));        
        approved <- tok ask SecurityModel.Read(poll.book) 
    ) yield {
      val results = poll.answers.filter(p => words.forall(p.words.contains(_)))        
        Ok(Json.obj("answers" -> results.map{a => Json.obj(
          "answer" -> a.text,
          "date" -> a.created
        )}))
    }) 
    r      
  }}
  

}