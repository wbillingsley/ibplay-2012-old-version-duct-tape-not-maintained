package controllers

import play.api._
import play.api.mvc._
import play.api.mvc.Results._

import com.wbillingsley.handy._
import Ref._

import com.theintelligentbook.ibmodel._
import mongo._
import RefConversions._
import play.api.data._

import model.Util._
import model.IBJson

/**
 * Controller for content that's external to the book
 */
object ExternalsController extends Controller {

  /** Show the form to edit the URL */
  def editWebsite(wid:String) = WithEm { Action { implicit request =>
    val wRef = RefById(classOf[Website], wid)
    val tok:Approval[Reader] = SessionStuff.loggedInReader(request)

    for (a <- tok ask SecurityModel.EditContent(wRef.flatMap(_.ce))) yield {
      views.html.editWebsite(wRef, tok)
    }
  }}

  /** Show the form to edit the video id */
  def editYouTube(yid:String) = WithEm { Action { implicit request =>
    val yRef = RefById(classOf[YouTube], yid)
    val tok:Approval[Reader] = SessionStuff.loggedInReader(request)

    for (a <- tok ask SecurityModel.EditContent(yRef.flatMap(_.ce))) yield {
        Ok(views.html.editYouTube(yRef, tok))
    }
  }}

  /** Show the form to edit the video id */
  def editGoogleSlides(yid:String) = WithEm { Action { implicit request =>
    val yRef = RefById(classOf[GoogleSlides], yid)
    val tok:Approval[Reader] = SessionStuff.loggedInReader(request)

    val r =for (gp <- yRef; a <- tok ask SecurityModel.EditContent(gp.ce)) yield {
        Ok(views.html.editGoogleSlides(gp, tok))
    }
    r
  }}
  

  /**
   * REST API for adding a web page
   * @param bookId
   * @return
   */
  def addWebsiteEntry(bookId:String) = WithEm { Action { implicit request =>
    val b = RefById(classOf[Book], bookId).fetch
    val tok:Approval[Reader] = SessionStuff.loggedInReader(request)

    val mapping = request.body.asMultipartFormData.get.asFormUrlEncoded

    val presKey = fetchStr(mapping, "presentation")
    val pres = Ref.fromOptionId(classOf[Presentation], presKey)
    val addToPres = fetchBool(mapping, "ce_addToPres").getOrElse(false)

    val ceRef = ExternalsModel.newWebsiteEntry(
      bookRef=b, tok=tok, url=fetchStr(mapping, "ce_refval").getOrElse("http://www.example.com"),
      name=fetchStr(mapping, "ce_name"),
      shortDescription=fetchStr(mapping, "ce_note"),
      adjectives=fetchSet(mapping, "ce_adj"),
      nouns=fetchSet(mapping, "ce_noun"),
      topics=fetchSet(mapping, "ce_topic"),
      highlightTopic=fetchStr(mapping, "ce_highlightTopic"),
      showFirst=fetchBool(mapping, "ce_showFirst").getOrElse(false),
      protect=fetchBool(mapping, "ce_protect").getOrElse(false)
    )
    if (addToPres) {
      for (ce <- ceRef) {
        val rm = new RefManyById(classOf[ContentEntry], Seq(ce.id))
        PresentationModel.insertEntriesIntoPresentation(pres, rm, -1, tok)
      }
    }

    val r = (for (ce <- ceRef) yield {
      request match {
        case Accepts.Html() => Redirect(routes.ContentController.lookupView(bookId, entry=Some(ce.id.toString))).itself
        case Accepts.Json() => ceAndPresResponse(tok, b, ceRef, pres)
        case _ => NotAcceptable.itself
      }    
    }).flatten    
    r    
    
  }}

  /**
   * REST API for adding a web page
   * @param bookId
   * @return
   */
  def addYouTubeEntry(bookId:String) = WithEm { Action { implicit request =>
    val b = RefById(classOf[Book], bookId).fetch
    val tok:Approval[Reader] = SessionStuff.loggedInReader(request)

    val mapping = request.body.asMultipartFormData.get.asFormUrlEncoded

    val presKey = fetchStr(mapping, "presentation")
    val pres = Ref.fromOptionId(classOf[Presentation], presKey)
    val addToPres = fetchBool(mapping, "ce_addToPres").getOrElse(false)

    val ceRef = ExternalsModel.newYouTubeEntry(
      bookRef=b, tok=tok, youtubeId=fetchStr(mapping, "ce_refval").getOrElse("ahTScxRjSWM"),
      name=fetchStr(mapping, "ce_name"),
      shortDescription=fetchStr(mapping, "ce_note"),
      adjectives=fetchSet(mapping, "ce_adj"),
      nouns=fetchSet(mapping, "ce_noun"),
      topics=fetchSet(mapping, "ce_topic"),
      highlightTopic=fetchStr(mapping, "ce_highlightTopic"),
      showFirst=fetchBool(mapping, "ce_showFirst").getOrElse(false),
      protect=fetchBool(mapping, "ce_protect").getOrElse(false)
    )
    if (addToPres) {
      for (ce <- ceRef) {
        val rm = new RefManyById(classOf[ContentEntry], Seq(ce.id))
        PresentationModel.insertEntriesIntoPresentation(pres, rm, -1, tok)
      }
    }

    val r = (for (ce <- ceRef) yield {
      request match {
        case Accepts.Html() => Redirect(routes.ContentController.lookupView(bookId, entry=Some(ce.id.toString))).itself
        case Accepts.Json() => ceAndPresResponse(tok, b, ceRef, pres)
        case _ => NotAcceptable.itself
      }    
    }).flatten    
    r 
  }}
  
  /**
   * REST API for adding a web page
   * @param bookId
   * @return
   */
  def addGoogleSlidesEntry(bookId:String) = WithEm { Action { implicit request =>
    val b = RefById(classOf[Book], bookId).fetch
    val tok:Approval[Reader] = SessionStuff.loggedInReader(request)

    val mapping = request.body.asMultipartFormData.get.asFormUrlEncoded

    val presKey = fetchStr(mapping, "presentation")
    val pres = Ref.fromOptionId(classOf[Presentation], presKey)
    val addToPres = fetchBool(mapping, "ce_addToPres").getOrElse(false)
    
    val embedCode = fetchStr(mapping, "ce_embedCode")
    val presId = for (str <- embedCode) yield {
      // Finds the URL in the embed code
      val codeMatcher = "presentation/d/([^ \"]+)/embed".r
      
      codeMatcher.findFirstMatchIn(str) match {
        case Some(regex) => regex.group(1)
        case None => str
      }      
    }

    val ceRef = ExternalsModel.newGoogleSlidesEntry(
      bookRef=b, tok=tok, 
      embedCode=fetchStr(mapping, "ce_embedCode"),
      presId=presId,
      name=fetchStr(mapping, "ce_name"),
      shortDescription=fetchStr(mapping, "ce_note"),
      adjectives=fetchSet(mapping, "ce_adj"),
      nouns=fetchSet(mapping, "ce_noun"),
      topics=fetchSet(mapping, "ce_topic"),
      highlightTopic=fetchStr(mapping, "ce_highlightTopic"),
      showFirst=fetchBool(mapping, "ce_showFirst").getOrElse(false),
      protect=fetchBool(mapping, "ce_protect").getOrElse(false)
    )
    if (addToPres) {
      for (ce <- ceRef) {
        val rm = new RefManyById(classOf[ContentEntry], Seq(ce.id))
        PresentationModel.insertEntriesIntoPresentation(pres, rm, -1, tok)
      }
    }

    val r = (for (ce <- ceRef) yield {
      request match {
        case Accepts.Html() => Redirect(routes.ContentController.lookupView(bookId, entry=Some(ce.id.toString))).itself
        case Accepts.Json() => ceAndPresResponse(tok, b, ceRef, pres)
        case _ => NotAcceptable.itself
      }    
    }).flatten    
    r 
  }}  


 def handleEditWebsite(wid:String) = WithEm { Action { implicit request =>
   val rr = for (
     w <- RefById(classOf[Website], wid)   
   ) yield {
     val tok = Approval(SessionStuff.loggedInReader(request))
     val params = request.body.asFormUrlEncoded.get
     
     w.url = params.get("url").flatMap(_.headOption)
     val result = ExternalsModel.saveWebsite(w, tok)

     for (
         r <- result;
         url <- Ref(r.url)         
     ) yield Redirect(url)
     
   }
   refResultToResult(rr.flatten(Ref.OneToOne)) 
 }}
 
 def handleEditGoogleSlides(wid:String) = WithEm { Action { implicit request =>
   val rr = for (
     gp <- RefById(classOf[GoogleSlides], wid)   
   ) yield {
     val tok = Approval(SessionStuff.loggedInReader(request))
     val mapping = request.body.asMultipartFormData.get.asFormUrlEncoded
     
     val embedCode = fetchStr(mapping, "ce_embedCode")
     val presId = for (str <- embedCode) yield {
       // Finds the URL in the embed code
       val codeMatcher = "presentation/d/([^ \"]+)/embed".r
      
       codeMatcher.findFirstMatchIn(str) match {
         case Some(regex) => regex.group(1)
         case None => str
       }      
     }    
     
     gp.embedCode = embedCode
     gp.presId = presId
     val result = ExternalsModel.saveGooglePres(gp, tok)

     for (
         r <- result;
         c <- r.ce;
         url <- IBJson.viewUrl(c)        
     ) yield Redirect(url)
     
   }
   refResultToResult(rr.flatten(Ref.OneToOne)) 
 }} 
 

}
