package controllers

import com.wbillingsley.handy._
import Ref._
import com.theintelligentbook.ibmodel._
import com.theintelligentbook.ibmodel.RefConversions._
import com.theintelligentbook.ibmodel.mongo._
import model._
import model.Util._
import play.api.mvc.Results._
import play.api.mvc.{Action, Controller}
import play.api.libs.json.Json

object CommentsController extends Controller {
  
  
  /**
   * Records a like or dislike comment against a content entry
   * @param eid
   * @return
   */
  def addComment(eid:String) = WithEm { Action { implicit request =>

    val ceRef = RefById(classOf[ContentEntry], eid)
    val tok = Approval(SessionStuff.loggedInReader(request))
    val skOpt = SessionStuff.sessionKey(request.session)
    val sk = skOpt.getOrElse(SessionStuff.newSessionKey)

    val fd = request.body.asMultipartFormData.get
    val mapping = fd.asFormUrlEncoded

    val comment = fetchStr(mapping, "comment")
    
    val r = for (
      ce <- ceRef;
      appr <- tok ask SecurityModel.Comment(ce.itself)    
    ) yield {
      comment match {
        case Some(c) => {
          ContentModel.comment(ce.itself, tok, Some(sk), c)
          request match {
            case Accepts.Html() => Ok(views.html.viewEntryComments(ceRef, tok, -1))
            case Accepts.Json() => Ok(Json.obj("result" -> "ok"))
            case _ => NotAcceptable
          }          
        }
        case _ => Forbidden("You did not provide any text in that comment")
      }
    }
    r
  }}
  
  /**
   * Renders the content viewing page
   * @param bid
   * @param entry
   * @param adj
   * @param noun
   * @param topic
   * @param site
   * @return
   */
  def viewForEntry(eid:String, skip:Option[Int]) = WithEm { Action { implicit request =>
    import Ref._
    
    for (
        e <- RefById(classOf[ContentEntry], eid);
        r <- optionally(SessionStuff.loggedInReader(request));
        appr = Approval(r);
        approved <- appr ask SecurityModel.Read(e.book)
    ) yield {
      views.html.viewEntryComments(e.itself, appr, skip.getOrElse(0))
    }
  }}
  
  
  /**
   * Returns a JSON structure representing the set of comments
   */
  def jsonForEntry(eid:String, skip:Option[Int]) = WithEm { Action { implicit request =>    
    import Ref._
    
    val e = RefById(classOf[ContentEntry], eid)
    val r = SessionStuff.loggedInReader(request)
    val resp = for (
        ce <- e; 
        comments = ContentModel.getComments(ce.itself, Approval(r), skip.getOrElse(0));
        jsonComments <- IBJson.manyEntryCommentToJson(ce.comments.toRefMany)
    ) yield {
      Ok(Json.obj(
        "count" -> ce.commentCount,
        "comments" -> jsonComments.toSeq
      ))
    }    
    resp
  }}
}