package controllers

import play.api._
import play.api.mvc._
import play.api.mvc.Results

import com.theintelligentbook.ibmodel._
import com.theintelligentbook.ibmodel.mongo._
import model.{IBJson, Identity}
import com.wbillingsley.handy.{Ref, RefNone, RefItself}


object UserController {

  def loggedIn(r:Ref[Reader], redirectTo:Option[Call] = None) = { implicit request:Request[AnyContent] =>
    val redir =redirectTo.getOrElse(routes.Application.index())
    Results.Redirect(redir).withSession(SessionStuff.withLoggedInReader(request.session, r))
  }

}
